<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table     = 'offers';
    protected $fillable  = ['name','slug','code','nbrPosition','description','category','experience','status','location','dateEnd','price'];

}
