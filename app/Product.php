<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table     = 'products';
    protected $fillable = ['name','slug','price','minQty','description','image','image_url','category_id','onSale','sale_amount','saleType','salePercentage','store_id','more_info','stock_location','livraison','prixLivraison'];

    public function is_liked_by_auth_user(){
        return $this->likes->where('user_id', Auth::id())->isNotEmpty();
    }
    public function is_wished_by_auth_user(){
        return $this->wishlists->where('user_id', Auth::id())->isNotEmpty();
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function wishlists(){

        return $this->hasMany('App\Wishlist');
    }

    public function productImages(){

        return $this->hasMany('App\ProductImages');
    }

    public function likes(){
        return $this->hasMany('App\Like');
    }

    public function wishlist(){
        return $this->hasMany(Wishlist::class);
    }
}
