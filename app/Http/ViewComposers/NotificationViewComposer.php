<?php

namespace App\Http\ViewComposers;

use App\Demande;
use App\Product;
use App\Purchased;
use Illuminate\Contracts\View\View;

class NotificationViewComposer
{

//    protected $notifications;
//
//    public function __construct(UserRepository $users)
//    {
//        // Dependencies automatically resolved by service container...
//        $this->users = $users;
//    }

    public function compose(View $view)
    {


        $allShopNotifications    = Purchased::all();
        $shopNotifications       = Purchased::orderBy('view')->orderBy('created_at','desc')->paginate(10);
        $shopNotificationsLus    = Purchased::where('view','Yes')->orderBy('created_at','desc')->paginate(10);
        $newShopNotifications    = Purchased::where('view','No')->get();

        $allNotifications        = Demande::all();
        $notifications           = Demande::orderBy('view')->orderBy('created_at','desc')->paginate(10);
        $newNotifications        = Demande::where('view','No')->get();
        $view->with('notifications',        $notifications)
             ->with('newNotifications',     $newNotifications)
             ->with('allNotifications',      $allNotifications)
             ->with('allShopNotifications',  $allShopNotifications)
             ->with('shopNotifications',     $shopNotifications)
             ->with('shopNotificationsLus',  $shopNotificationsLus)
             ->with('newShopNotifications',  $newShopNotifications);

    }
}