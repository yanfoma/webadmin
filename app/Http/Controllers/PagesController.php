<?php

namespace App\Http\Controllers;

use App\Category;
use Auth;
use App\Checkouts;
use App\Like;
use App\Mail\thankYouPurchased;
use App\Purchased;
use App\Product;
use App\Settings;
use App\Store;
use App\Tracking;
use App\Wishlist;
use Session;
use Mail;
use App\Cart;
use App\SlideShow;
use App\Mail\contactMessage;
use Illuminate\Http\Request;

use Newsletter;

class PagesController extends Controller
{
    public function __construct()
    {

        $this->middleware(['auth']);
    }

    public function welcome(){
        $stores = Store::orderBy('created_at','desc')->get();
        return view('welcome',compact('stores'));
    }

    public function purchased(){
        $store_id  = Auth::user()->store_id;
        $purcahsed = Purchased::where('store_id',$store_id)->orderBy('created_at','desc')->get();
        return view('yanfoshop_admin.purchased.index',compact('purcahsed'));
    }

    public function ecobankConfirm(){
        $store_id  = Auth::user()->store_id;
        $purcahsed = Purchased::where('store_id',$store_id)->paginate(10);
        return view('yanfoshop_admin.purchased.confirm',compact('purcahsed'));
    }

    public function ecobankConfirmed($id){
        $store_id  = Auth::user()->store_id;
        $purcahsed = Purchased::where('id',$id)->where('store_id',$store_id)->first();
        $purcahsed->payed = 2;
        $purcahsed->save();

        //send email to the guy
        $name  = $purcahsed->user->name;
        $to    = $purcahsed->user->email;
        $email = "info@yanfoma.tech";

        Tracking::create([
            'purchasedId'      => $purcahsed->purchasedId,
            'step'             => "En cours de préparation pour l'envoie",
            'date'             => now(),
            'note'             => "Votre Commande à été validée et est en cours préparation pour l'envoie"
        ]);
        $data = [
            'to'               => $to,
            'name'             => $name,
            'purchasedId'      => $purcahsed->purchasedId,
            'payedDate'        => $purcahsed->payedDate,
            'email'            => $email,
            'total'            => $purcahsed->total,
        ];


        Mail::send('emails.purchasedConfirmed',$data,function($mail) use ($to,$email,$name) {
            $mail->from($email,'YanfoShop');
            $mail->to($to,$name);
            $mail->subject('Confirmation De votre Versement Ecobank' );
        },true);

        Session::flash('success','Transaction validée!!!');

        return redirect()->route('purchased.ecobankConfirm');
    }

    public function favorited(){
        $store_id  = Auth::user()->store_id;
        $favorited       = Wishlist::select('product_id','created_at')->where('store_id',$store_id)->groupBy('product_id')->get();
        $favorited_count = Wishlist::where('store_id',$store_id)->count();
        return view('yanfoshop_admin.favorited.index',compact('favorited','favorited_count'));
    }

    public function liked(){
        $store_id  = Auth::user()->store_id;
        $liked       = Like::select('product_id','created_at')->where('store_id',$store_id)->groupBy('product_id')->get();
        $liked_count = Like::where('store_id',$store_id)->count();
        return view('yanfoshop_admin.liked.index',compact('liked','liked_count'));
    }


    public function shop()
    {
        $store_id  = Auth::user()->store_id;
        if(request('query')){

            //die(request('query'));
            //$products = Product::where('name','like','%'.request('query').'%')->orderBy('name')->paginate(6);
            //dd($products);
        }else{
            $products  = Product::where('store_id',$store_id)->OrderBy('id','desc')->paginate(3);
        }

        $bondeals = Product::where('store_id',$store_id)->OrderBy('price','asc')->get()->take(3);
        $latests  = Product::where('store_id',$store_id)->OrderBy('id','desc')->get()->take(3);
        $slides   = SlideShow::where('store_id',$store_id)->latest()->first();
        return view('frontEnd.shop',compact('products','bondeals','latests','slides'));
    }

    public function singleProduct($slug)
    {
        $store_id  = Auth::user()->store_id;
        $product   = Product::where('slug', $slug)->where('store_id',$store_id)->firstOrFail();

        $bondeals = Product::where('store_id',$store_id)->OrderBy('price','asc')->get()->take(3);
        $latests  = Product::where('store_id',$store_id)->OrderBy('id','desc')->get()->take(3);
        return view('frontEnd.shopSingle',compact('product','bondeals','latests'));
    }

    public function cart(){

        return view('frontEnd.cart');
    }

    public function addToCart(){
        $store_id  = Auth::user()->store_id;
        $product = Product::where('id',request()->product_id)->where('store_id',$store_id)->first();

        if (\Auth::check()) {
            $cartItem      = Cart::add([
                'id'       => $product->id,
                'name'     => $product->name,
                'qty'      => request()->quantity,
                'price'    => $product->price,
                'store_id' => $store_id
            ]);

            Cart::associate($cartItem->rowId,'App\Product');
            return redirect()->route('cart.index');

        } else {
            return redirect(app()->getLocale().'/login/'.$product->id);
        }
    }

    public function cartDelete($id)
    {
        Cart::remove($id);
        return redirect()->back();
    }

    public function checkout()
    {
        if(Cart::content()->count() == 0)
        {
            Session::flash('info','Votre panier est vide. Veuillez ajouter des produits!!!');
            return redirect()->back();
        }
        return view('frontEnd.checkout');
    }

     public function order(Request $request)
    {

        if(Cart::content()->count() == 0)
        {
            Session::flash('info','Votre panier est vide. Veuillez ajouter des produits!!!');
            return redirect()->route('cart.index');
        }

        $store_id  = Auth::user()->store_id;

        $this->validate($request,[
            'name'      => 'required',
            'country'   => 'required',
            'city'      => 'required',
            'email'     => 'required|email',
            'phone'     => 'required',
            'address'   => 'required'
        ]);

        $name           = $request->name;
        $country        = $request->country;
        $city           = $request->city;
        $email          = $request->email;
        $phone          = $request->phone;
        $address        = $request->address;
        $to             = 'fabioued@yahoo.fr';
//        $resume       = $request->resume;
//        $code         = $request->code;
//        $id           = $request->id;

        $data = [
            'to'        =>$to,
            'name'      =>$name,
            'country'   =>$country,
            'city'      =>$city,
            'email'     =>$email,
            'phone'     =>$phone,
            'address'   =>$address,
        ];

        $purchaser = Purchased::create([
            'name'          => $name,
            'pays'          => $country,
            'ville'         => $city,
            'email'         => $email,
            'phone'         => $phone,
            'addresse'      => $address,
            'nbrProduits'   => Cart::content()->count(),
            'total'         => Cart::total(),
            'view'          => 'No',
            'store_id'      => $store_id,
        ]);

         foreach(Cart::content() as $product){

            Checkouts::create([
                'purchased_id'   =>$purchaser->id,
                'purchased_name' =>$purchaser->name,
                'name'           =>$product->name,
                'image'          =>$product->model->image,
                'qty'            =>$product->qty,
                'prix'           =>$product->price,
                'total'          =>$product->total(),
                'store_id'      => $store_id,
            ]);
         }

        Mail::send('emails.purchased',$data,function($mail) use ($to,$email) {
            $mail->from($email);
            $mail->to($to);
            $mail->subject('New Purchase' );
        },true);

        Mail::to($email)->send(new thankYouPurchased($name));

         Cart::destroy();

        Session::flash('success',trans('app.thanksPurchased'));

        return redirect()->route('thanks');
    }


    public function thanks(){
        return view('frontEnd.thanks');
    }


    public function redirect(){

        return redirect('login');
    }

    public function subscribe(){
        $email = request('email');
        $name  = request('name');

        Newsletter::subscribe($email,['LNAME'=>$name]);


        Session::flash('success',trans('app.succesRegistered'));

        return redirect()->back();
    }


    public function email(Request $request){

        //dd($request->all());

        $this->validate($request,[
            'name'      => 'required',
            'email'     => 'required|email',
            'subject'   => 'required',
            'message'   => 'required'
        ]);

        $email      = $request->email;
        $name       = $request->name;
        $message    = $request->message;
        $subject    = $request->subject;
        $phone      = $request->phone;
        $send       = 'fabioued@yahoo.fr';

        Mail::to($send)->send(new contactMessage($name,$message,$phone,$email,$subject));

        Session::flash('success',trans('app.thanksContact'));

        return redirect()->back();
    }

    public function search(){

        $languuage = app()->getLocale();

        $product = Product::where('name','like','%'.request('query').'%')->get();

        return view('frontEnd/results')->with('product_query',$product)
                ->with('title','Search results for:'.request('query'))
                ->with('settings', Settings::first());
    }


}


