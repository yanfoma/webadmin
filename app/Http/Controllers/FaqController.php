<?php

namespace App\Http\Controllers;

use App\Faq;
use Session;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index(){
        $faqs = Faq::orderBy('created_at','Desc')->get();
        return view('yanfoshop_admin.faq.index',compact('faqs'));
    }

    public function create()
    {
        return view('yanfoshop_admin.faq.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'question' => 'required',
            'response' => 'required'
        ]);
        $faq                   =   new Faq();
        $faq->question         =   $request->question;
        $faq->response         =   $request->response;
        $faq->save();
        Session::flash('success', 'FAQ Created !!!');
        return redirect()->route('faq.index');
    }
    public function edit($id)
    {
        $faq = Faq::find($id);
        return view('yanfoshop_admin.faq.edit',compact('faq'));

    }
    public function update(Request $request, $id)
    {
        $faq           = Faq::find($id);
        $faq->question = $request->question;
        $faq->response = $request->response;
        $faq->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('faq.index');
    }

    public function destroy($id)
    {
        Faq::destroy($id);
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('faq.index');
    }
}
