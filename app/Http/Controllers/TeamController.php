<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Session;
use File;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::where('admin','>','0')->get();
        return view('yanfoshop_admin.team.index',compact('users'));
    }

    public function create()
    {
        return view('yanfoshop_admin.team.create');
    }

    public function profile()
    {
        return view('yanfoshop_admin.team.profile');
    }

    public function edit($id)
    {
        $member = Team::find($id);
        return view('yanfoshop_admin.team.edit')->with('member', $member);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required',
            'admin' => 'required',
            'email' => 'required|email'
        ]);
        $user = User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'admin'     => $request->admin,
            'password'  => bcrypt('WebAdmin@2019'),
            'adminType' => 'yanfoshop',
        ]);
        $profile = Profile::create([
            'user_id' => $user->id,
            'avatar'  => env('AVATAR_LINK'),
        ]);
        Session::flash('success', trans('app.userCreated'));
        return redirect()->route('team.index');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required',
            'email'         => 'required',
            'langue'        => 'required',
        ]);

        $member = User::find($id);
        $member->name           = $request->name;
        $member->position       = $request->position;
        $member->bio            = $request->bio;
        $member->langue         = $request->langue;

        $member->save();

        Session::flash('success',trans('app.memberUpdated'));

        return redirect()->route('team.index');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        Session::flash('success', trans('app.userDeleted'));
        return redirect()->route('team.index');
    }
}
