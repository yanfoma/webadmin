<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Session;

class UsersController extends Controller
{

    public function __construct()
    {

        $this->middleware(['auth']);
    }

    public function index()
    {
        $store_id       = Auth::user()->store_id;
        $users = User::where('store_id',$store_id)->paginate(100);
        return view('yanfoshop_admin.users.index',compact('users'));
    }

    public function create()
    {
        return view('yanfoshop_admin.users.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt('password'),
        ]);
        $profile = Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatar/avatar.png'
        ]);
        Session::flash('success', trans('app.userCreated'));
        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('yanfoshop_admin.users.edit')->with('user', $user);
    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->profile->delete();
        unlink($user->profile->avatar);
        $user->delete();
        Session::flash('success', trans('app.userDeleted'));
        return redirect()->route('users.index');
    }

    public function admin($id)
    {
        $user = User::find($id);
        $user->admin = 1;
        $user->save();
        Session::flash('success', trans('app.userIsAdmin'));
        return redirect()->route('users.index');
    }

    public function not_admin($id)
    {
        $user = User::find($id);
        $user->admin = 0;
        $user->save();
        Session::flash('success', trans('app.userIsNotAdmin'));
        return redirect()->route('users.index');

    }
}
