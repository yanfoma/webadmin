<?php

namespace App\Http\Controllers;

use App\Category;

use App\Post;

use App\PostTranslation;
use App\Tag;

use Session;

use File;

use Auth;

use Illuminate\Http\Response;

use Illuminate\View\View;

use Illuminate\Http\Request;
use Alert;
class PostsController extends Controller
{
    public function index()
    {
        return view('yanfoshop_admin/posts/index')->with('posts', PostTranslation::orderBy('created_at','desc')->paginate(12));
    }

    public function create()
    {
        //
        $categories = Category::all();
        $tags       = Tag::all();
        $posts      = PostTranslation::all();

        if($categories->count() == 0 || $tags->count() == 0){

            Session::flash('info',trans('app.noCategory'));

            return redirect()->back();
        }
        return view('yanfoshop_admin/posts/create')->with('categories', $categories)
                                         ->with('tags',$tags)
                                         ->with('posts',$posts);
    }

    public function store(Request $request)
    {
            $this->validate($request,[
            'title'         => 'required',
            'featuredImage' => 'required|image',
            'language'      => 'required',
            'body'          => 'required',
            'category_id'   => 'required',
            'tags'          => 'required',
        ]);

        $featuredImage          = $request->featuredImage;

        $featuredImage_new_name = time().$featuredImage->getClientOriginalName();

        $featuredImage->move('uploads/', $featuredImage_new_name);

        $langue     = $request->language;

        $locale     = substr($langue,0,2);

        app()->setLocale($locale);

        $post = Post::create([

            'user_id'       => Auth::id(),
            'title'         => $request->title,
            'slug'          => str_slug($request->title),
            'content'       => $request->body,
            'featuredImage' => 'uploads/' .$featuredImage_new_name,
            'language'      => $request->language,
            'category_id'   => $request->category_id,
            'blog'          => 'post',
            'translated'    => '1',
        ]);

        $post->tags()->attach($request->tags);

        Session::flash('success',trans('app.postCreated'));

        return redirect()->route('post.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$locale)
    {
        //
        $tag = Tag::all();

        $categories =  Category::all();

        $locale = $locale;

        $post = PostTranslation::where('post_id',$id)->where('locale',$locale)->first();

        $postTag = Post::find($id);

        //dd($postTag->tags);
        return view('yanfoshop_admin.posts.edit')->with('post',$post)
                                       ->with('postTag',$postTag)
                                       ->with('categories',$categories)
                                       ->with('tags',$tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function translate($id)
    {
        $post = PostTranslation::find($id);
        return view('yanfoshop_admin.posts.translate')->with('post',$post)
                                            ->with('category', Category::find($id))
                                            ->with('tags', Tag::all());
    }

    public function update(Request $request, $id,$locale)
    {
        //

        $this->validate($request, [
            'title'         => 'required',
            'body'          => 'required',
            'category_id'   => 'required',
            'language'      => 'required',
            'blog'          => 'required'
        ]);

        //$post = Post::find($id);

        $post = PostTranslation::where('post_id',$id)->where('locale',$locale)->first();

        //dd($post->title);

        /*

        $translated      = $postTranslation->translated;

        */

        $featuredImage   = $post->featuredImage;

        //dd($featuredImage);

        if($request->hasFile('featuredImage')){

            $path = parse_url($post->featuredImage);
            File::delete(public_path($path['path']));

            $featuredImage          = $request->featuredImage;

            $featuredImage_new_name = time().$featuredImage->getClientOriginalName();

            $featuredImage->move('uploads/', $featuredImage_new_name);

            $post->featuredImage = 'uploads/'.$featuredImage_new_name;
        }


        $post->title         = $request->title;
        $post->slug          = str_slug($request->title);
        $post->featuredImage = $featuredImage;
        $post->content       = $request->body;
        $post->language      = $request->language;
        $post->category_id   = $request->category_id;
        $post->blog          = $request->blog;

        $post->save();

       // $post->tags()->sync($request->tags);

        Session::flash('success',trans('app.postUpdated'));

        return redirect()->route('post.index');
    }


    public function storeTranslate(Request $request, $id)
    {
        $this->validate($request,[
            'title'      => 'required',
            'body'       => 'required',
        ]);

        $postToTranslate = PostTranslation::find($id);

        //$currentPost    = Post::find($id)->first();

        $title          = $request->title;
        $slug           = str_slug($request->title);
        $image          = $postToTranslate->featuredImage;
        $langue         = $postToTranslate->language;
        $category_id    = $postToTranslate->category_id;
        $blog           = $postToTranslate->blog;
        $user_id        = $postToTranslate->user_id;
        $post_id        = $postToTranslate->post_id;
        //$tags           = $currentPost->tags;
        $content        = $request->body;
        $translated     = 1;

        if($langue =='english'){

            $langue = 'french';
        }else{
            $langue = 'english';
        }
        $locale     = substr($langue,0,2);

        app()->setLocale($locale);

        //dd($post_id,$request->title,$locale);

        $post = PostTranslation::create([

            'title'               => $title,
            'featuredImage'       => $image,
            'slug'                => $slug,
            'content'             => $content,
            'language'            => $langue,
            'locale'              => $locale,
            'translated'          => $translated,
            'category_id'         => $category_id,
            'blog'                => $blog,
            'user_id'             => $user_id,
            'post_id'             => $post_id,
        ]);

        $postToTranslate->translated = $translated;
        $postToTranslate->save();
        //$post->tags()->sync($request->tags);
        Session::flash('success',trans('app.public'));
        //dd($request->all());

        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //Alert::success('Do you want to delete this record','Confirmation');
        //Alert::error('Something went wrong', 'Oops!');

        $posts = PostTranslation::where('post_id',$id)->get();
        //dd($post);

        foreach ($posts as $post){

            $post->delete();
        }

        //Alert::message('Robots are working!')->persistent("Close this");

        Session::flash('success',trans('app.postTrashed'));

        return redirect()->route('post.index');
    }

    public function trashed(){

        $posts = PostTranslation::onlyTrashed()->get();

        return view('yanfoshop_admin.posts.trashed')->with('posts',$posts);


    }

    public function kill($id){

        $posts = PostTranslation::withTrashed()->where('post_id', $id)->get();
        $postImage = PostTranslation::withTrashed()->where('post_id', $id)->first();

        $path = parse_url($postImage->featuredImage);

        File::delete(public_path($path['path']));

        foreach ($posts as $post){

            $post->forceDelete();

        }





        Session::flash('success', trans('app.postDeleted'));

        return redirect()->back();

    }

    public function restore ($id){
        //

        $posts = PostTranslation::withTrashed()->where('post_id', $id)->get();

        foreach ($posts as $post){

            $post->restore();

        }

        Session::flash('success', trans('app.postRestored'));

        return redirect()->route('post.index');
    }
}
