<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function index()
    {
        return view('yanfoshop_admin.coupons.index')->with('coupons', Coupon::all());
    }
    public function create()
    {
        return view('yanfoshop_admin.coupons.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'category' => 'required'
        ]);
        //dd($request->image);
        $category               =   new Category;
        //$image_url              =   $this->uploadFile($request);
        $category->name         =   $request->category;
        $category->image_url    =   'null';
        $category->save();
        Session::flash('success', trans('app.CategoryCreated'));
        return redirect()->route('coupons.index');
    }

    public function edit($id)
    {
        $coupon = Coupon::find($id);
        return view('yanfoshop_admin.coupons.edit')->with('coupons', $coupon);

    }
    public function update(Request $request, $id)
    {
        $coupon = Coupon::find($id);
        $coupon->name = $request->category;
//        if ($request->hasFile('image')) {
//
//            Cloudder::delete($category->image_url);
//            $category->image_url    = $request->file('image')->getClientOriginalName();
//            $image_url              = $this->uploadFile($request);
//            $category->image_url    = $image_url;
//        }
        $category->image_url    =   'null';
        $category->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('coupons.index');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        //$publicId = $category->image_url;
        $category->delete();
        //Cloudder::delete($publicId);

//        foreach ($category->posts as $post){
//            $post->forceDelete();
//        }
//        $category->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('coupons.index');
    }
}
