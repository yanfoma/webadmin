<?php

namespace App\Http\Controllers;

use App\Category;
use App\ParentCategory;
use App\Subcategory;
use Auth;
use Illuminate\Http\Request;
use Session;

class ParentCategoryController extends Controller
{

    public function index()
    {
        $store_id        = Auth::user()->store_id;
        $categories      = ParentCategory::where('store_id',$store_id)->get();
        return view('yanfoshop_admin.categories.parentCategories.index',compact('categories'));
    }


    public function create()
    {
        return view('yanfoshop_admin.categories.parentCategories.create');
    }

    public function show($id)
    {
        $store_id        = Auth::user()->store_id;
        $parentCategory  = ParentCategory::where('id',$id)->where('store_id',$store_id)->first();
        $subCategories   = $parentCategory->Subcategories;
        return view('yanfoshop_admin.categories.parentCategories.show',compact('subCategories','parentCategory'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'category' => 'required'
        ]);
        $category               =   new ParentCategory;
        $category->name         =   $request->category;
        $category->store_id     =   Auth::user()->store_id;
        $category->image        =   'null';
        $category->save();
        Session::flash('success', trans('app.CategoryCreated'));
        return redirect()->route('parentCategory.index');
    }


    public function edit($id) {
        $store_id       = Auth::user()->store_id;
        $category       = ParentCategory::where('id',$id)->where('store_id',$store_id)->first();
        return view('yanfoshop_admin.categories.parentCategories.edit',compact('category'));

    }
    public function update(Request $request, $id)
    {
        $store_id       = Auth::user()->store_id;
        $category       = ParentCategory::where('id',$id)->where('store_id',$store_id)->first();
        $category->name = $request->category;
//        if ($request->hasFile('image')) {
//
//            Cloudder::delete($category->image_url);
//            $category->image_url    = $request->file('image')->getClientOriginalName();
//            $image_url              = $this->uploadFile($request);
//            $category->image_url    = $image_url;
//        }
        $category->image        =   'null';
        $category->store_id     =   $store_id;
        $category->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('parentCategory.index');
    }

    public function destroy($id)
    {
        $store_id       = Auth::user()->store_id;
        $category       = ParentCategory::where('id',$id)->where('store_id',$store_id)->first();
        $category->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('parentCategory.index');
    }

}
