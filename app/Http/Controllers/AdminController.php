<?php

namespace App\Http\Controllers;

use App\Checkouts;
use App\Community;
use App\Demande;
use App\Introduction;
use App\Mail\customerMails;
use App\Offer;
use App\Purchased;
use App\Testimony;
use App\UpcomingEvents;
use App\User;
use Illuminate\Http\Request;
use Mail;
use Session;
use File;
use App\SlideShow;
use Analytics;
use Spatie\Analytics\Period;

class AdminController extends Controller
{
    public function index()
    {

        return view('yanfoshop_admin.home.index');
    }


    public function upload(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $image = $request->image;

        $image_new_name = time() . $image->getClientOriginalName();

        $image->move('uploads/slideshows/', $image_new_name);

        SlideShow::create([
            'title' => $request->title,
            'image' => 'uploads/slideshows/' . $image_new_name,
        ]);

        Session::flash('success', trans('app.imageAdded'));
        return redirect()->route('slideshow.index');
    }

    public function notifications()
    {
        $allNotifications = Demande::all();
        $notifications = Demande::orderBy('view')->orderBy('created_at', 'desc')->simplePaginate();
        $newNotifications = Demande::where('view', 'No')->get();
        return view('yanfoshop_admin.notifications', compact('notifications', 'allNotifications'));
    }

    public function shopNotifications()
    {
        $allNotifications = Purchased::all();
        $notifications = Purchased::orderBy('view')->orderBy('created_at', 'desc')->simplePaginate();
        $newNotifications = Purchased::where('view', 'No')->get();
        return view('yanfoshop_admin.shopNotifications', compact('notifications', 'allNotifications'));
    }

    public function singleNotification($id)
    {
        $notification = Purchased::findorFail($id);
        $offer = Offer::where('code', $notification->code)->first();
        $notification->view = 'Yes';
        $notification->save();
        return view('yanfoshop_admin.singleNotification', compact('notification', 'offer'));
    }

    public function singleShopNotification($id)
    {
        $notification = Purchased::where('id',$id)->first();
        $checkouts = Checkouts::where('purchased_id', $id)->get();
        $notification->view = 'Yes';
        $notification->save();
        return view('yanfoshop_admin.singleShopNotification', compact('notification', 'checkouts'));
    }

    public function deleteBulkNotifications(Request $request)
    {

        //dd($request->all());

        $notifications = Demande::findOrFail($request->bulkNotifications);
        foreach ($notifications as $notification) {
            $notification->delete();
        }
        Session::flash('success', trans('app.deleted'));
        return redirect()->back();
    }

    public function deleteShopBulkNotifications(Request $request)
    {

        //dd($request->all());

        $notifications = Purchased::findOrFail($request->bulkNotifications);
        foreach ($notifications as $notification) {
            $notification->delete();
        }
        Session::flash('success', trans('app.deleted'));
        return redirect()->back();
    }

    public function mailCenter()
    {
        $users = User::where('admin', 0)->get();
        return view('admin.mails-center.mails-center-index', ['users' => $users]);
    }

    public function sendMailToCustomers(Request $request)
    {
        $moreusers = ($request->get('moreusers'));
        $object = $request->get('subject');
        $msbody = $request->get('message');

        Mail::bcc($moreusers)->send(new CustomerMails($object, $msbody));


        return redirect()->back();
    }

}
