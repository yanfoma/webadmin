<?php

namespace App\Http\Controllers;

use App\Product;
use App\Sale;
use Auth;
use Illuminate\Http\Request;
use Session;

class SalesController extends Controller
{
    public function index()
    {
        $store_id   = Auth::user()->store_id;
        $products   = Product::where('store_id',$store_id)->orderBy('created_at','desc')->get();
        $sales      = Sale::where('store_id',$store_id)->get();
        return view('yanfoshop_admin.sales.index',compact('sales','products'));
    }
    public function create()
    {
        $store_id   = Auth::user()->store_id;
        $products = Product::where('store_id',$store_id)->orderBy('created_at','desc')->get();
        return view('yanfoshop_admin.sales.create',compact('products'));
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'product_id'     => 'required',
            'sale_amount'    => 'required'
        ]);
        $store_id   = Auth::user()->store_id;
        $product_id                 = $request->product_id;
        $product                    = Product::where('id',$product_id)->where('store_id',$store_id)->first();
        $product->onSale            = true;
        $product->saleType          = 1;
        $product->sale_amount       = $request->sale_amount;
        $product->save();
        if(Sale::where('product_id',$product_id)->where('store_id',$store_id)->get()->isEmpty()){
            $sale                 = new Sale();
            $sale->product_id     = $product_id;
            $sale->store_id       = $store_id;
            $sale->save();
        }
        Session::flash('success', 'Sale created Succesfully !!!');
        return redirect()->route('sales.index');
    }

    public function storePercentage(Request $request)
    {
        $this->validate($request,[
            'sale_percentage'    => 'required'
        ]);

        $store_id   = Auth::user()->store_id;

        foreach (Product::where('store_id',$store_id)->get()  as $product){
            $product->onSale            = true;
            $product->saleType          = 2;
            $product->salePercentage    = $request->sale_percentage;
            $product->sale_amount       = $product->price - ($product->price * $request->sale_percentage ) / 100;
            $product->save();
            if(Sale::where('product_id',$product->id)->where('store_id',$store_id)->get()->isEmpty()){
                $sale                 = new Sale();
                $sale->product_id     = $product->id;
                $sale->store_id       = $store_id;
                $sale->save();
            }
        }
        Session::flash('success', 'Sale created Succesfully !!!');
        return redirect()->route('sales.index');
    }

    public function edit($id)
    {
        $store_id = Auth::user()->store_id;
        $sale     = Sale::where('id',$id)->where('store_id',$store_id)->first();
        $products = Product::where('store_id',$store_id)->orderBy('created_at','desc')->get();
        return view('yanfoshop_admin.sales.edit',compact('sale','products'));

    }
    public function update(Request $request, $id)
    {
        $store_id = Auth::user()->store_id;
        $product_id             = $request->product_id;
        $product                = Product::where('id',$product_id)->where('store_id',$store_id)->first();;
        $product->sale_amount   = $request->sale_amount;
        $product->saleType      = 1;
        $product->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('sales.index');
    }
    public function destroy($id)
    {
        $store_id                   = Auth::user()->store_id;
        $sale                       = Sale::where('id',$id)->where('store_id',$store_id)->first();
        $product                    = Product::find($sale->product_id);
        $product->onSale            = false;
        $product->sale_amount       = null;
        $product->salePercentage    = null;
        $product->saleType          = null;
        $product->save();
        $sale->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('sales.index');
    }

    public function deleteAll()
    {
        foreach (Product::where('store_id',Auth::user()->store_id)->get()  as $product)
        {
            $product->onSale            = false;
            $product->saleType          = null;
            $product->sale_amount       = null;
            $product->salePercentage    = null;
            $product->save();
        }
        foreach (Sale::where('store_id',Auth::user()->store_id)->get()  as $sale)
        {
            $sale->delete();
        }

        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('sales.index');
    }

}
