<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;
use Session;
use Auth;
use File;

/**
 * Class ProductsController
 * @package App\Http\Controllers
 */
class ProductsController extends Controller
{

    public function index()
    {

        $products   = Product::where('store_id',Auth::user()->store_id)->OrderBy('id', 'desc')->paginate(30);
        $categories = Category::where('store_id',Auth::user()->store_id)->get();
        return view('yanfoshop_admin.products.index', compact('products','categories'));
    }

    public function create()
    {
        $categories = Category::where('store_id',Auth::user()->store_id)->get();
        return view('yanfoshop_admin.products.create',compact('categories'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'name'          => "required",
            'price'         => "required",
            'minQty'        => "required",
            'description'   => "required",
            'category_id'   => "required",
            'image'         => "required|mimes:jpeg,bmp,jpg,png|between:1, 2048",
        ]);





        $store_id = Auth::user()->store_id;

        $image_url = $this->uploadFile($request);

        $more_info = '';
        
        if($request->has('more_info')){
            $more_info = $request->more_info;
        }

        if(!empty($request->prixLivraison)){
            $prixLivraison = $request->prixLivraison;
            $livraison     = "Payante";
        }else{
            $livraison     = "Gratuite";
            $prixLivraison = 0;
        }

        Product::create([
            'name'           => $request->name,
            'slug'           => str_slug($request->name),
            'price'          => $request->price,
            'minQty'         => $request->minQty,
            'description'    => $request->description,
            'more_info'      => $more_info,
            'image_url'      => $image_url,
            'category_id'    => $request->category_id,
            'store_id'       => $store_id,
            'stock_location' => $request->stock_location,
            'image'          =>$request->file('image')->getClientOriginalName(),
            'prixLivraison'  => $prixLivraison,
            'livraison'      => $livraison
        ]);

        Session::flash('success', 'Produit Ajouté avec Succès!');

        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $store_id       = Auth::user()->store_id;
        $product        = Product::where('id',$id)->where('store_id',$store_id)->first();
        $product->image = $product->image_url;
        $categories     = Category::where('store_id',$store_id)->get();
        return view('yanfoshop_admin.products.edit', compact('product','categories'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'          => "required",
            'price'         => "required",
            'minQty'        => "required",
            'category_id'   => 'required',
            'description'   => "required",
        ]);

        $store_id       = Auth::user()->store_id;

        $product = Product::where('id',$id)->where('store_id',$store_id)->first();
        $image_url ='';

        if ($request->hasFile('image')) {
            Cloudder::delete($product->image);
            $product->image      = $request->file('image')->getClientOriginalName();
            $image_url           = $this->uploadFile($request);
            $product->image_url  = $image_url;
        }

        if(!empty($request->prixLivraison)){
            $prixLivraison = $request->prixLivraison;
            $livraison     = "Payante";
        }else{
            $livraison     = "Gratuite";
            $prixLivraison = 0;
        }

        $product->name           = $request->name;
        $product->slug           = str_slug($request->name);
        $product->price          = $request->price;
        $product->minQty         = $request->minQty;
        $product->category_id    = $request->category_id;
        $product->description    = $request->description;
        $product->more_info      = $request->more_info;
        $product->stock_location = $request->stock_location;
        $product->livraison      = $livraison;
        $product->prixLivraison  = $prixLivraison;
        $product->save();
        Session::flash('success', 'Produit Modifié avec Succès!');
        return redirect()->route('products.index');
    }

    public function delete($id)
    {
        $store_id       = Auth::user()->store_id;
        $product        = Product::where('id',$id)->where('store_id',$store_id)->first();
        $publicId       = $product->image;
        $product->delete();
        Cloudder::delete($publicId);
        Session::flash('success', 'Produit Supprimé avec Succès!');
        return redirect()->route('products.index');
    }

    private function uploadFile(Request $request) {

        $image                = $request->file('image');
        $image_name           = $image->getRealPath();
        $publicId             = $image->getClientOriginalName();
        Cloudder::upload($image_name, $publicId);
        list($width, $height) = getimagesize($image_name);
        $image_url            = Cloudder::secureShow(Cloudder::getPublicId(), ["width" => $width, "height" => $height]);

        return $image_url;
    }
}
