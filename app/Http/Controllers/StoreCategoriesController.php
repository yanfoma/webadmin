<?php

namespace App\Http\Controllers;

use App\StoreCategorie;
use App\StoreCategories;
use Illuminate\Http\Request;
use Session;

class StoreCategoriesController extends Controller
{
    public function index()
    {
        $storeCategories = StoreCategorie::orderBy('created_at','desc')->get();
        return view('yanfoshop_admin.storeCategories.index',compact('storeCategories'));
    }
    public function create()
    {
        return view('yanfoshop_admin.storeCategories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'category' => 'required'
        ]);
        $storeCategory        =   StoreCategorie::create([
            'name' =>$request->category
        ]);
        Session::flash('success', trans('app.CategoryCreated'));
        return redirect()->route('storeCategories.index');
    }


    public function edit($id)
    {
        $storeCategory = StoreCategorie::find($id);
        return view('yanfoshop_admin.storeCategories.edit',compact('storeCategory'));

    }

    public function update(Request $request, $id)
    {
        $storeCategory       = StoreCategorie::find($id);
        $storeCategory->name = $request->category;
        $storeCategory->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('storeCategories.index');
    }

    public function destroy($id)
    {
        $storeCategory = StoreCategorie::find($id);
        $storeCategory->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('storeCategories.index');
    }

}
