<?php

namespace App\Http\Controllers;

use App\Interest;
use Session;
use Illuminate\Http\Request;

class InterestController extends Controller
{
    public function index(){
        $interests = Interest::orderBy('created_at','Desc')->get();
        return view('yanfoshop_admin.interests.index',compact('interests'));
    }

    public function create()
    {
        return view('yanfoshop_admin.interests.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'interest' => 'required'
        ]);
        $interest               =   new Interest();
        $interest->name         =   $request->interest;
        $interest->save();
        Session::flash('success', 'Interst Created !!!');
        return redirect()->route('interest.index');
    }
    public function edit($id)
    {
        $interest = Interest::find($id);
        return view('yanfoshop_admin.interests.edit',compact('interest'));

    }
    public function update(Request $request, $id)
    {
        $interest       = Interest::find($id);
        $interest->name = $request->interest;
        $interest->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('interest.index');
    }

    public function destroy($id)
    {
        Interest::destroy($id);
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('interest.index');
    }
}
