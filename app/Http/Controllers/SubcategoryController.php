<?php

namespace App\Http\Controllers;

use App\Category;
use App\ParentCategory;
use App\Subcategory;
use Auth;
use Illuminate\Http\Request;
use Session;

class SubcategoryController extends Controller
{
    public function index()
    {
        $store_id        = Auth::user()->store_id;
        $categories      = Category::where('store_id',$store_id)->get();
        return view('yanfoshop_admin.categories.index',compact('categories'));
    }

    public function create($id)
    {
        return view('yanfoshop_admin.categories.subcategories.create',compact('id'));
    }


    public function addCategories($subcat_id,$parent_id)
    {
        $store_id       = Auth::user()->store_id;
        $parentCategory = ParentCategory::where('store_id',$store_id)->where('id',$parent_id)->first();
        $subCategory    = Subcategory::where('store_id',$store_id)->where('id',$subcat_id)->where('parent_id',$parent_id)->first();
        $categories     = Category::where('store_id',$store_id)->get();
        return view('yanfoshop_admin.categories.subCategories.addCategories',compact('parentCategory','subCategory','categories'));
    }

    public function storeCategories(Request $request)
    {
        $this->validate($request,[
            'categories' => 'required'
        ]);
        $parentSub_id           = $request->parentSub_id;
        $parent_id              = $request->parent_id;
        $store_id               = Auth::user()->store_id;
        $categories             = $request->categories;

        foreach ($categories as $cat)
        {
            $category = Category::where('name','=',$cat)->where('store_id',$store_id)->first();
            $category->parent_id    = $parent_id;
            $category->parentSub_id = $parentSub_id;
            $category->save();
        }
        Session::flash('success', 'Ajouté avec Succès!');
        return redirect()->back();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'category' => 'required'
        ]);

        $id                     = $request->parent_id;
        $store_id               = Auth::user()->store_id;
        $parentCategory         = ParentCategory::where('id',$id)->where('store_id',$store_id)->first();
        $subcategory            = new Subcategory;
        $subcategory->name      = $request->category;
        $subcategory->parent_id = $id;
        $subcategory->store_id  = Auth::user()->store_id;
        $subcategory->image     = 'null';
        $subcategory->save();
        Session::flash('success', 'Ajouté avec Succès!');
        return redirect()->route('parentCategory.index');
    }

    public function edit($id,$parent_id) {
        $store_id       = Auth::user()->store_id;
        $subCategory    = Subcategory::where('id',$id)->where('parent_id',$parent_id)->where('store_id',$store_id)->first();
        return view('yanfoshop_admin.categories.subCategories.edit',compact('subCategory','parent_id'));

    }


    public function update(Request $request)
    {
        $id                 = $request->subCategory_id;
        $parent_id          = $request->parent_id;
        $store_id           = Auth::user()->store_id;
        $subCategory        = Subcategory::where('id',$id)->where('store_id',$store_id)->where('parent_id',$parent_id)->first();
        $subCategory->name  = $request->category;
//        if ($request->hasFile('image')) {
//
//            Cloudder::delete($category->image_url);
//            $category->image_url    = $request->file('image')->getClientOriginalName();
//            $image_url              = $this->uploadFile($request);
//            $category->image_url    = $image_url;
//        }
        $subCategory->image        =   'null';
        $subCategory->store_id     =   $store_id;
        $subCategory->parent_id    =   $parent_id;
        $subCategory->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('parentCategory.show',['id' => $parent_id]);
    }

    public function delCategories($id,$parent_id)
    {
        $store_id                  = Auth::user()->store_id;
        $category                  = Category::where('id',$id)->where('store_id',$store_id)->where('parentSub_id',$parent_id)->first();
        $category->parentSub_id    = 0;
        $category->parent_id       = 0;
        $category->save();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->back();
    }

    public function delete($id,$parent_id)
    {
        $store_id       = Auth::user()->store_id;
        $subCategory    = Subcategory::where('id',$id)->where('store_id',$store_id)->where('parent_id',$parent_id)->first();
        $subCategory->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('parentCategory.show',['id' => $parent_id]);
    }
}
