<?php

namespace App\Http\Controllers;

use App\Purchased;
use Auth;
use Mail;
use Session;
use App\Tracking;
use Illuminate\Http\Request;

class TrackingController extends Controller
{
    public function index(){
        $store_id     = Auth::user()->store_id;
        $purchased    = Purchased::where('store_id',$store_id)->with('trackings')->has('trackings')->get();
        return view('yanfoshop_admin.tracking.index',compact('purchased'));
    }

    public function create(){
        $store_id     = Auth::user()->store_id;
        $purchased = Purchased::where('store_id',$store_id)->get();
        return view('yanfoshop_admin.tracking.create',compact('purchased'));
    }

    public function store(Request $request){

        $this->validate($request,[
            'commandes'         => 'required',
            'date'              => 'required',
            'notificationMode'  => 'required',
            'step'              => 'required',
        ]);

        $store_id     = Auth::user()->store_id;
        foreach ($request->commandes as $commande)
        {
            Tracking::create([
                "purchasedId"   => $commande,
                "trackingCode"  => $request->trackingCode,
                "step"          => $request->step,
                "date"          => $request->date,
                "note"          => $request->note,
                "store_id"      => $store_id
            ]);

            $purchased          = Purchased::where('purchasedId',$commande)->where('store_id',$store_id)->first();

            $to                 = $purchased->email;
            $email              = "info@yanfoma.tech";
            $name               = $purchased->name;
            $purchasedId        = $purchased->purchasedId;
            $data               = [
                'to'            => $to,
                'name'          => $name,
                'purchasedId'   => $purchased->purchasedId,
                'payedDate'     => $purchased->payedDate,
                'email'         => $email,
                'total'         => $purchased->total,
                'step'          => $request->step,
                'trackingCode'  => $request->trackingCode,
                'note'          => $request->note,
                'date'          => $request->date,
            ];


            Mail::send('emails.tracking',$data,function($mail) use ($to,$email,$name,$purchasedId) {
                $mail->from($email,'YanfoShop');
                $mail->to($to,$name);
                $mail->subject('Suivi de votre Commande '.$purchasedId);
            },true);

//            switch($request->notificationMode){
//
//                case "mailSms" :
//                    sendTrackingSms();
//                    sendTrackingMail();
//
//                break;
//
//                case "sms" :
//                    sendTrackingSms();
//                break;
//
//                case "default":
//                    sendTrackingMail();
//                break;
//
//            }

            Session::flash('success','Success!!!');
            return redirect()->route('tracking.index');
        }
    }

    public function sendTrackingMail(){
        return 0;
    }

    public function sendTrackingSms(){
        return 0;
    }
}
