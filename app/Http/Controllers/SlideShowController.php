<?php

namespace App\Http\Controllers;

use App\SlideShow;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;
use Session;
use Auth;
use File;

class SlideShowController extends Controller
{
    //
    public function index()
    {
        $store_id   = Auth::user()->store_id;
        $images     = SlideShow::where('store_id',$store_id)->orderBy('order','asc')->get();
        return view('yanfoshop_admin.slideshow.index')->with('slides',$images);
    }

    public function updateOrder(Request $request)
    {
        $store_id   = Auth::user()->store_id;
        $images = SlideShow::where('store_id',$store_id)->get();
        foreach ($images as $slide) {
            //$slide->timestamps = false; // To disable update_at field updation
            $id = $slide->id;
            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $slide->update(['order' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $store_id       = Auth::user()->store_id;

        $image_url = $this->uploadFile($request);
        SlideShow::create([
            'title'         => $request->title,
            'image'         => $image_url,
            'store_id'      => $store_id
        ]);

        Session::flash('success',trans('app.imageAdded'));
        return redirect()->route('slideshow.index');
    }

    public function destroy($id)
    {
        $store_id   = Auth::user()->store_id;
        $slide      = SlideShow::where('id',$id)->where('store_id',$store_id)->first();
        $publicId   = $slide->image;
        $slide->delete();
        Cloudder::delete($publicId);
        Session::flash('success',trans('app.imageDeleted'));
        return redirect()->route('slideshow.index');
    }

    private function uploadFile(Request $request) {

        $image = $request->file('image');
        $image_name = $image->getRealPath();
        $publicId = $image->getClientOriginalName();
        Cloudder::upload($image_name, $publicId);
        list($width, $height) = getimagesize($image_name);
        $image_url = Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height" => $height]);

        return $image_url;
    }
}
