<?php

namespace App\Http\Controllers;

use App\Store;
use App\StoreCategorie;
use App\User;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;
use Session;
use File;

class StoreController extends Controller
{

    public function index()
    {
        return view('yanfoshop_admin.stores.index');
    }

    public function create()
    {
        $storeCategories = StoreCategorie::orderBy('created_at','desc')->get();
        return view('yanfoshop_admin.stores.create',compact('storeCategories'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name'          => "required",
            'category_id'   => "required",
            'email'         => "required|email",
            'phone'         => "required",
            'logo'          => "required|mimes:jpeg,bmp,jpg,png|between:1, 2048",
            'image'         => "required|mimes:jpeg,bmp,jpg,png|between:1, 2048",
            'about'         => "required",
        ]);

//        dd($request->all());

        $image_url = $this->uploadFile($request,'image');
        $logo_url  = $this->uploadFile($request,'logo');
        $store = Store::create([
            'name'          => $request->name,
            'slug'          => str_slug($request->name),
            'category_id'   => $request->category_id,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'addresse'      => $request->addresse,
            'facebook'      => $request->facebook,
            'messenger'     => $request->messenger,
            'whatsappp'     => $request->whatsappp,
            'linkedIn'      => $request->linkedIn,
            'about'         => $request->about,
            'image'         => $image_url,
            'logo'          => $logo_url,
        ]);

        $store_id           =$store->id;
        $createdStore       = Store::find($store_id);
        $createdStore->store_id =$store_id;
        $createdStore->save();
        //create the admin of that store and give a password

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'admin' => 4,
            'password' => bcrypt('YanfoShop@Store@2019'),
            'verified' => 'no',
            'admintype' => 'yanfoshop',
            'store_id' => $store->id,
        ]);

        Session::flash('success', 'Store Ajouté avec Succès!');

        return redirect()->route('welcome');
    }

    public function show(Store $store)
    {
        //
    }
    public function edit(Store $store)
    {
        //
    }
    public function update(Request $request, Store $store)
    {
        //
    }

    public function destroy($id)
    {
        $store          = Store::findOrFail($id);
        $publicId       = $store->image;
        $publicIdLogo   = $store->logo;
        $store->delete();
        Cloudder::delete($publicId);
        Cloudder::delete($publicIdLogo);
        Session::flash('success', 'Store Supprimé avec Succès!');
        return redirect()->route('store.index');
    }


    private function uploadFile(Request $request,$file) {

        $image                = $request->file($file);
        $image_name           = $image->getRealPath();
        $publicId             = $image->getClientOriginalName();
        Cloudder::upload($image_name, $publicId);
        list($width, $height) = getimagesize($image_name);
        $image_url            = Cloudder::secureShow(Cloudder::getPublicId(), ["width" => $width, "height" => $height]);

        return $image_url;
    }
}
