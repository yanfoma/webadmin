<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Store;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo;

    //$locale =Config::get('app.locale');
    /*if($locale=="en"){

        protected $redirectTo = '/dasboard';
    }*/

    protected function authenticated(Request $request)
    {
        $user = $request->user();

        if ($user->admintype == 'yanfoshop') {
            if($user->admin == 1 && $user->store_id == 1){
                return redirect()->route('welcome');
            }else{
                $store  = Store::find($user->store_id);
                return redirect()->route('dashboard');
            }
        }

        if ($user->admintype == 'cosmic') {
            return redirect()->route('cosmic');
        }

        return redirect()->route('login');
    }

    public function username()
    {
        if(!is_null(Input::get('email'))) {
            return 'email';
        };
        return 'wa_number';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
