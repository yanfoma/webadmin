<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use JD\Cloudder\Facades\Cloudder;
use File;
use Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store_id        = Auth::user()->store_id;
        $categories      = Category::where('store_id',$store_id)->get();
        return view('yanfoshop_admin.categories.index',compact('categories'));
    }
    public function create()
    {
        return view('yanfoshop_admin.categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'category' => 'required'
        ]);
        $category               =   new Category;
        $category->name         =   $request->category;
        $category->store_id     =   Auth::user()->store_id;
        $category->image_url    =   'null';
        $category->save();
        Session::flash('success', trans('app.CategoryCreated'));
        return redirect()->route('category.index');
    }

    public function edit($id) {
        $store_id       = Auth::user()->store_id;
        $category       = Category::where('id',$id)->where('store_id',$store_id)->first();
        return view('yanfoshop_admin.categories.edit',compact('category'));

    }
    public function update(Request $request, $id)
    {
        $store_id       = Auth::user()->store_id;
        $category       = Category::where('id',$id)->where('store_id',$store_id)->first();
        $category->name = $request->category;
//        if ($request->hasFile('image')) {
//
//            Cloudder::delete($category->image_url);
//            $category->image_url    = $request->file('image')->getClientOriginalName();
//            $image_url              = $this->uploadFile($request);
//            $category->image_url    = $image_url;
//        }
        $category->image_url    =   'null';
        $category->store_id     =   $store_id;
        $category->save();
        Session::flash('success', 'Modifié avec Succès!');
        return redirect()->route('category.index');
    }

    public function destroy($id)
    {
        $store_id       = Auth::user()->store_id;
        $category       = Category::where('id',$id)->where('store_id',$store_id)->first();
        //$publicId = $category->image_url;
        $category->delete();
        //Cloudder::delete($publicId);

//        foreach ($category->posts as $post){
//            $post->forceDelete();
//        }
//        $category->delete();
        Session::flash('success', 'Supprimé avec Succès!');
        return redirect()->route('category.index');
    }

    private function uploadFile(Request $request) {

        $image                  = $request->file('image');
        $image_name             = $image->getRealPath();
        $publicId               = $image->getClientOriginalName();
        Cloudder::upload($image_name, $publicId);
        list($width, $height)   = getimagesize($image_name);
        $image_url              = Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height" => $height]);

        return $image_url;
    }
}
