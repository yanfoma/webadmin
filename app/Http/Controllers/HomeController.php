<?php

namespace App\Http\Controllers;

use App\Category;
use Auth;
use App\Coupon;
use App\Like;
use App\Post;
use App\PostTranslation;
use App\Product;
use App\Purchased;
use App\Sale;
use App\SlideShow;
use App\Store;
use App\Tag;
use App\User;
use App\Settings;
use App\Wishlist;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $store_id        = Auth::user()->store_id;
        $language        = app()->getLocale();
        $users_count     = User::where('store_id',$store_id)->get()->count();
        $payments_count  = Purchased::whereIn('payed',[1,2])->where('store_id',$store_id)->count();
        $coupons_count   = Coupon::where('store_id',$store_id)->get()->count();
        $sales_count     = Sale::where('store_id',$store_id)->get()->count();
        $liked_count     = Like::where('store_id',$store_id)->get()->count();
        $liked           = Like::select('product_id','created_at')->where('store_id',$store_id)->groupBy('product_id')->get();
        $favorited_count = Wishlist::where('store_id',$store_id)->get()->count();
        $favorited       = Wishlist::select('product_id','created_at')->where('store_id',$store_id)->groupBy('product_id')->get();
        $purchased_count = Purchased::where('store_id',$store_id)->get()->count();
        $products_count  = Product::where('store_id',$store_id)->get()->count();
        $users           = User::where('store_id',$store_id)->orderBy('created_at','desc')->take(5)->get();
        $categories      = Category::where('store_id',$store_id)->orderBy('created_at','desc')->take(3)->get();
        $products        = Product::where('store_id',$store_id)->orderBy('created_at','desc')->take(3)->get();
        $teams           = User::where('admin','>',0)->where('store_id',$store_id)->get();

        return view('yanfoshop_admin/dashboard',compact('users_count','products_count','purchased_count','liked_count','favorited_count','favorited','liked','sales_count','payments_count','coupons_count','users','categories','products','teams'));
    }

    public function index1()
    {
        $language       = app()->getLocale();
        $users_count    = User::all()->count();
//        $slides         = SlideShow::latest()->first();
        $products_count = Product::all()->count();
        return view('cosmic_admin/dashboard',compact('users_count','products_count'));
    }
}
