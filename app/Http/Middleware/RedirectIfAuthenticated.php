<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            
            if (Auth::user()->admintype == 'yanfoshop') {
                return redirect()->route('yanfoshop');
            }
            
            if (Auth::user()->admintype == 'cosmic') {
                    return redirect()->route('cosmic');
            }
        }

        return $next($request);
    }
}
