<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class Admin
{

    public function handle($request, Closure $next)
    {

        if(Auth::user()->admin != 1){
            Session::flash('info',trans('app.noPermission'));
            return redirect()->back();
        }

        return $next($request);
    }
}
