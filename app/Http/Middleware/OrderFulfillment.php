<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
use Session;

class OrderFulfillment
{
    public function handle($request, Closure $next)
    {
        if(Auth::user()->admin != 2){
            Session::flash('info',trans('app.noPermission'));
            return redirect()->back();
        }
        return $next($request);
    }
}
