<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = ['store_id','name','image','parent_id'];

    public function categories(){

        return $this->hasMany('App\Category','parentSub_id');
    }

    public function parent(){

        return $this->belongsTo('App\ParentCategory');

    }
}
