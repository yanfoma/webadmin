<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpcomingEvents extends Model
{
    protected $table = 'upcomingEvents';

    protected $fillable = ['title','language','body','order'];

}
