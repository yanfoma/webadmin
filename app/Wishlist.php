<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = ['product_id', 'user_id','store_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function users(){
        return Wishlist::where('product_id',$this->product_id)->get();
    }

    public function products(){
        return Wishlist::where('product_id',$this->product_id)->get();
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
