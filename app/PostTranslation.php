<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    //

    use SoftDeletes;

   // public $timestamps = false;
    protected $fillable = ['title','post_id','content','category_id','featuredImage','language','slug','blog','created_at','deleted_at','updated_at','translated','locale'];

    public function category(){

        return $this->belongsTo('App\Category');
    }

    public function tags (){
        return $this->belongsToMany('App\Tag');
    }

    public function user (){
        return $this->belongsTo('App\User');
    }
}
