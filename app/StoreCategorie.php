<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreCategorie extends Model
{
    protected $fillable = ['name'];

    public function stores(){
        return $this->hasMany('App\Stores');
    }

}
