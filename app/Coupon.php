<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = ['name','code','number_of_use','max_uses','max_uses_user','type','discount_amount','starts_at','expires_at','is_fixed','percent_off','store_id'];
}
