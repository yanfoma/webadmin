<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchased extends Model
{
    protected $table        = 'purchased';
    protected $primaryKey   = 'purchasedId';
    protected $fillable     = ['purchasedId','name','pays','ville','email','phone','addresse','nbrProduits','total','view','payed','payedDate','user_id','recu','note','store_id'];


    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function trackings()
    {
        return $this->hasMany('\App\Tracking','purchasedId');
    }


    public function checkouts(){

        return $this->hasMany('\App\Checkouts');

    }
}
