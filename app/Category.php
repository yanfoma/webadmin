<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['store_id','name','parent_id','parentSub_id'];

    public function products(){
        return $this->hasMany('App\Product');
    }

    public function parent(){

        return $this->belongsTo('App\ParentCategory');

    }

    public function parentSub(){

        return $this->belongsTo('App\Subcategory');

    }

    public function postTranslations(){

        return $this->hasMany('App\PostTranslation');

    }
}
