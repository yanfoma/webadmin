<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentCategory extends Model
{
    protected $fillable = ['store_id','name','image'];

    public function Subcategories(){
        return $this->hasMany('App\Subcategory','parent_id');
    }
}
