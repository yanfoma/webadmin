<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Request;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    //protected $namespace = '';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        //$locale = Request::segment(1);
        $locale = Request::segment(1);
        /*$locale = substr(Request::server('HTTP_ACCEPT_LANGUAGE'), 0, 2);

        if ($locale != 'fr' && $locale != 'en') {
            $locale = 'en';
        }*/
        //$locale = Request::server('HTTP_ACCEPT_LANGUAGE');

        /*Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
        */
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace,
            'prefix'     => $locale
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
