<?php

namespace App\Providers;

use App\Settings;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer('errors.404', function($view)
        {
            return $view->with('settings', Settings::first());
        });

        View::composer('layouts/frontEnd/footer', function($view)
        {
            $view->with('settings', Settings::first());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
