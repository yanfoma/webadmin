<?php

namespace App\Providers;

use App\Settings;
use Jenssegers\Date\Date;
use App\SlideShow;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer("layouts/header","App\Http\ViewComposers\NotificationViewComposer");

        View::composer('errors.404', function($view)
        {
            return $view->with('settings', Settings::first());
        });

        View::composer('layouts/frontEnd/footer', function($view)
        {
            $view->with('settings', Settings::first());
        });

        View::composer('layouts/frontEnd/slideshow', function($view)
        {
            $view->with('slides', SlideShow::all());
        });

        // Set the app locale according to the URL
        //app()->setLocale(substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2));


        app()->setLocale(request()->segment(1));

        $locale=app()->getLocale();
        Date::setLocale($locale);

        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
        $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
    }
    }
}
