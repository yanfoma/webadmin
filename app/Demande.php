<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demande extends Model
{
    protected $table     = 'demande';
    protected $fillable  = ['name','email','phone','resume','message','view','code'];
}