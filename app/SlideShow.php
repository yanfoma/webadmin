<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideShow extends Model
{
    //
    protected $table = 'slideshow';

    protected $fillable = ['title','image','order','store_id'];

    public function getImageAttribute($image){

        return asset($image);
    }

    public function store(){
        return $this->belongsTo('App\Store');
    }
}
