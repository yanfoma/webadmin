<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = ['id','store_id','name','slug','about','email','phone','addresse','facebook','messenger','whatsapp','linkedIn','category_id','image','logo'];

    public function category(){
        return $this->hasOne('App\StoreCategorie');
    }

    public function products(){
        return $this->hasMany('App\Product');
    }
}


