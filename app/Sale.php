<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['product_id','store_id'];

    public function product(){
        return $this->belongsTo('App\Product');
    }

//    public function product(){
//        return Product::find($this->product_id);
//    }
}
