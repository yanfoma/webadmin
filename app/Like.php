<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['product_id', 'user_id','store_id'];

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function users(){
        return Like::where('product_id',$this->product_id)->get();
    }
}
