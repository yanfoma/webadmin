<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class purchased extends Mailable
{
    use Queueable, SerializesModels;

    public $name,$country,$city,$phone,$email,$address;

    public function __construct($name,$country,$city,$phone,$email,$address)
    {
        $this->name         = $name;
        $this->country      = $country;
        $this->city         = $city;
        $this->phone        = $phone;
        $this->email        = $email;
        $this->address      = $address;
    }

    public function build()
    {
        return $this->view('emails.purchased');
    }
}
