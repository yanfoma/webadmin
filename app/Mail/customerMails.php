<?php

namespace App\Mail;

use App;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerMails extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $object, $msbody;

    /**
     * CustomerMails constructor.
     * @param $object
     * @param $msbody
     */
    public function __construct($object , $msbody)
    {
        $this->object = $object;
        $this->msbody = $msbody;
        if(App::isLocale('en')) $this->subject('Yanfoma e-mails center');
        else $this->subject('Centre de messagérie de Yanfoma');

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.yanfomaCustomersMails');
    }
}
