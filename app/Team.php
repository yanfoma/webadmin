<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    protected $table = 'team';

    protected $fillable = ['name','position','bio','title','image','langue','translated'];

    public function getImageAttribute($image){

        return asset($image);
    }
}
