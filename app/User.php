<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password','admin' , 'admintype', 'permission', 'wa_number', 'sexe',
        'dob', 'addresse', 'pays', 'ville','store_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function position(){

        $position ="";
        switch ($this->admin){
            case 0: $position ="Client"; Break;
            case 1: $position ="Admin"; Break;
            case 2: $position ="Order Fulfillment"; Break;
            case 3: $position ="Super Admin"; Break;
        }
        return $position;
    }

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function postTranslations(){
        return $this->hasMany('App\PostTranslations');
    }
}
