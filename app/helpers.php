<?php
/**
 * User: fabioued
 * Date: 10/29/18
 * Time: 15:09
 */

if (! function_exists('getUser')) {
    function getUser($id)
    {
         return \App\User::findOrFail($id);
    }
}