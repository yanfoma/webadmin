@extends('layouts.appWelcome')

@section('title')
    {{ trans('app.adminTitle') }}
@endsection

@section('style')
    <style>
        .circle{
            box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
        }
        .edit{
            border-radius: 4px;
            background-color: rgba(242, 242, 242, 0.8);
            border: 1px solid #cccccc;
            padding: 10px!important;
        }
        .card-panel{
            border-radius: 4px;
            box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m12 l12 gradient-shadow gradient-45deg-light-blue-cyan white-text z-depth-3">
            <h4 class="text center white-text">
                {{ trans('app.welcome') }} {{Auth::user()->name}}
            </h4>
        </div>
    </div>
    <div id="card-panel-type " class="section" style="padding: 20px; border-radius: 20px;">
        <h4 class="header center">Nos Stores</h4>
        <a class="waves-effect waves-light btn-large " href="{{route('store.create')}}">Ajouter Un Store</a>
        <div class="row mt-4">
            @foreach($stores as $store)
                <div class="col s12 m6 l6 card-width">
                    <div class="card-panel border-radius-6 mt-2 card-animation-1">
                    <h4 class="center"><a href="#" class="#">{{$store->name}}</a></h4>
                    <p>{!! $store->about !!}</p>
                    <div class="row mt-4">
                        <div class="col s5 mt-1">
                            <img src="{{$store->logo}}" alt="logo" class="circle mr-3 width-30 vertical-text-middle">
                            <span class="pt-2">{{$store->name}}</span>
                        </div>
                        <div class="col s7 mt-3 right-align social-icon">
                            <span class="material-icons">shopping_basket</span> <span class="ml-3 vertical-align-top">{{$store->products()->count()}}</span>
                        </div>

                        <div class="col s12 mt-4 edit">
                            <div class="col s4">
                                <a class="mb-6 btn btn-medium waves-effect waves-light gradient-45deg-light-blue-cyan" href="{{route('dashboard')}}"><i class="material-icons">remove_red_eye</i></a>
                            </div>
                            {{--<div class="col s4">--}}
                                {{--<a class="mb-6 btn btn-small waves-effect waves-light gradient-45deg-amber-amber" href="{{route('store.edit',['id' => $store->id])}}"><i class="material-icons">edit</i></a>--}}
                            {{--</div>--}}
                            @if($store->id != 1)
                                <div class="col s4">
                                    <a class="mb-6 btn btn-small waves-effect waves-light gradient-45deg-red-pink" href="{{route('store.delete',['id' => $store->id])}}"><i class="material-icons">delete</i></a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('scripts')
@endsection
