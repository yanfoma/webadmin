@extends('layouts.app')

@section('title')
    {{trans('app.SlideshowTitle')}}
@endsection

@section('content')
    <section id="content">
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    {{trans('app.slideshow')}}
                </h4>
            </div>
        </div>
        <br> <br>
        <a  class="waves-effect waves-light btn modal-trigger  yellow red-text center-block text-darken-3" href="#modal3"><i class="mdi-alert-warning"></i>{{trans('app.important')}}</a>

        <div id="modal3" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 491.5489130434783px;">
            <div class="modal-content teal white-text large text-capitalize">
                <p style="text-align: center">
                   {{trans('app.SlideShowNotice')}}
                </p>
            </div>
            <div class="modal-footer  teal lighten-2">
                <a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">{{trans('app.ok')}}</a>
            </div>
        </div>
        <!--start container-->
        <div class="container">
            <form action="{{ route('slideshow.update') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="input-field col s12">
                        <i class="mdi-action-account-circle prefix"></i>
                        <input id="title" name="title" type="text" class="validate" value="{{ old('title') }}">
                        <label for="title">{{trans('app.title')}}</label>
                        @if ($errors->has('title'))
                            <div id="uname-error" class="error">{{ $errors->first('title') }}</div>
                        @endif
                    </div>
                    <div class="col s12 m8 l12">
                        @if ($errors->has('image'))
                            <br><div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                        @endif
                        <label for="image" class="">{{trans('app.image')}}</label><br>
                        <input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <br/>
                    <button type="submit" class="btn btn-success">{{trans('app.add')}}</button>
                </div>
            </form>
            <div class="section">
                <table class="striped hoverable">
                    <thead>
                    <tr>
                        <th data-field="id">{{trans('app.image')}}</th>
                        <th data-field="id">{{trans('app.title')}}</th>
                        <th data-field="id">{{trans('app.delete')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($slides as $slide)
                        <tr>
                            <td> <img src="{{asset($slide->image)}}" width="90px" height="50px" alt="{{$slide->title}}"></td>
                            <td>{{$slide->title}}</td>
                            <td>
                                <form action="{{ route('slideshow.delete',$slide->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="delete">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="waves-effect waves-light btn-large " >{{trans('app.delete')}}</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!--end container-->
    </section>
@endsection