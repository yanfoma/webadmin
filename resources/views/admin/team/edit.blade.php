@extends('layouts.app')

@section('title')
    {{trans('app.editTeamTitle')}}
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                {{trans('app.edit')}}: {{$member->name}}
            </h4>
        </div>
    </div>
    <br> <br>
    <div class="row">
        <form action="{{ route('team.update',['id' => $member->id ])}}"  method="post" class="col s12 right-alert" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="row">
                <div class="input-field col s4">
                    <i class="mdi-action-account-circle prefix"></i>
                    <input id="name" name="name" type="text" class="validate" value="{{$member->name}}">
                    <label for="name">{{trans('app.nom')}}</label>
                    @if ($errors->has('name'))
                        <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="input-field col s4">
                    <i class="mdi-action-account-circle prefix"></i>
                    <input id="position" name="position" type="text" class="validate" value="{{$member->position}}">
                    <label for="position">{{trans('app.position')}}</label>
                    @if ($errors->has('position'))
                        <div id="uname-error" class="error">{{ $errors->first('position') }}</div>
                    @endif
                </div>
                <div class="input-field col s4">
                    <div class="select-wrapper initialized">
                        <select name="langue" class="initialized">
                            <option value="{{$member->langue}}"  selected>{{trans("app.$member->langue")}}</option>
                            <option value="english">{{trans('app.english')}} </option>
                            <option value="french">{{trans('app.french')}}</option>
                        </select>
                    </div>
                    @if ($errors->has('langue'))
                        <div id="uname-error" class="error">{{ $errors->first('langue') }}</div><br>
                    @endif
                </div>
                <div class="col s12 m8 l12">
                    @if ($errors->has('image'))
                        <br><div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                    @endif
                    <label for="image"  class="">{{trans('app.image')}}</label><br>
                    <input name="image" type="file" id="input-file-events" class="dropify-event" data-default-file="{{asset($member->image)}}""/>
                </div>
                <br>
                <div class="row">
                    <div class="input-field col s12">{{trans('app.bio')}}
                        <textarea id="bio" name="bio" class="materialize-textarea">{{$member->bio}}</textarea>
                        @if ($errors->has('bio'))
                            <div id="uname-error" class="error">{{ $errors->first('bio') }}</div><br>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.update')}}
                            <i class="mdi-content-send right"></i>
                        </button>
                    </div>
                </div>
        </form>
    </div>
@endsection