@extends('layouts.app')

@section('title')
    {{trans('app.allTeamTitle')}}
@endsection

@section('style')
    <style>
        p{
            word-wrap: break-word;
        }
    </style>
@endsection

@section('content')
    @if($members->count()> 0)
        <!------------------------------Membres Publies------------------------------------------------------------------------>
            <div class="row">
                <div class="col s12 m9 l12 invoice-brief grey lighten-3 red-text ">
                    <h4 class="text center">
                        {{trans('app.team')}}
                    </h4>
                </div>
            </div>
            <br> <br>
            <a class="waves-effect waves-light btn-large " href="{{route('team.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newTeam')}}</a>
        <br> <br>
            <div class="row">
                <div class="col s12 m9 l12 invoice-brief grey lighten-4 red-text z-depth-1">
                    <h4 class="text center">
                        {{trans('app.public')}}
                    </h4>
                </div>
            </div>
            <br> <br>
            <!------------------------------Membres Publies Anglais------------------------------------------------------------------------>
            <div class="row">
                <div class="col s12 m9 l12  red-text">
                    <h4 class="text center">
                        {{trans('app.english')}}
                    </h4>
                </div>
            </div>
            <div class="col s12 m8 l9">
            <br>
                @foreach($members_english as $member)
            <div id="blog-posts" class="row-blog">
                <div class="blog" style="position: inherit; ">
                    <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                            <a href="#"><img src="{{asset($member->image)}}" alt="{{$member->name}}">
                            </a>
                        </div>
                        <ul class="card-action-buttons">
                            <li>
                                <a class="btn-floating waves-effect waves-light orange" href="{{route('team.edit', ['id' => $member->id ])}}">
                                    <i class="mdi-editor-border-color right"></i>
                                </a>
                            </li>
                            <li>
                                <a class="btn-floating waves-effect waves-light red accent-4" href="{{route('team.delete',   ['id' => $member->id ])}}">
                                    <i class="mdi-action-delete right"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="card-content">
                            <p class="row">
                                <span class="left"><a href="#">{{trans('app.nom')}}: {{$member->name}}</a></span>
                            </p>
                            <h5 class="card-title grey-text text-darken-4">
                                <a href="#" class="grey-text text-darken-4"> {{$member->position}}</a>
                            </h5>
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4"><i class="mdi-navigation-close right"></i> Apple MacBook Pro A1278 13"</span>
                            <p>{!! $member->bio !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            </div>
            <!------------------------------Membres Publies Francais------------------------------------------------------------------------>
            <div class="row">
                <div class="col s12 m9 l12 invoice-brief grey lighten-3 red-text ">
                    <h4 class="text center">
                        {{trans('app.french')}}
                    </h4>
                </div>
            </div>
            <div class="col s12 m8 l9">
                <br>@foreach($members_french as $member)
                    <div id="blog-posts" class="row-blog">
                        <div class="blog" style="position: inherit; ">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <a href="#"><img src="{{asset($member->image)}}" alt="{{$member->name}}">
                                    </a>
                                </div>
                                <ul class="card-action-buttons">
                                    <li>
                                        <a class="btn-floating waves-effect waves-light orange" href="{{route('team.edit', ['id' => $member->id ])}}">
                                            <i class="mdi-editor-border-color right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="btn-floating waves-effect waves-light red accent-4" href="{{route('team.delete',   ['id' => $member->id ])}}">
                                            <i class="mdi-action-delete right"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div class="card-content">
                                    <p class="row">
                                        <span class="left"><a href="#">{{trans('app.nom')}}: {{$member->name}}</a></span>
                                    </p>
                                    <h5 class="card-title grey-text text-darken-4">
                                        <a href="#" class="grey-text text-darken-4"> {{$member->position}}</a>
                                    </h5>
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4"><i class="mdi-navigation-close right"></i> Apple MacBook Pro A1278 13"</span>
                                    <p>{!! $member->bio !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!------------------------------Membres Non Publies------------------------------------------------------------------------>
            <div class="row">
                <div class="col s12 m9 l12  lighten-2 red-text z-depth-1">
                    <h4 class="text center">
                        {{trans('app.notPublic')}}
                    </h4>
                </div>
            </div>
            <div class="col s12 m8 l9">
                <br>@foreach($members_noTranslated as $member)
                    <div id="blog-posts" class="row-blog">
                        <div class="blog" style="position: inherit; ">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <a href="#"><img src="{{asset($member->image)}}" alt="{{$member->name}}">
                                    </a>
                                </div>
                                <ul class="card-action-buttons">
                                    <li>
                                        <a class="btn-floating waves-effect waves-light blue" href="{{route('team.translate', ['id' => $member->id ])}}">
                                            <i class="mdi-action-translate right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="btn-floating waves-effect waves-light orange" href="{{route('team.edit', ['id' => $member->id ])}}">
                                            <i class="mdi-editor-border-color right"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="btn-floating waves-effect waves-light red accent-4" href="{{route('team.delete',   ['id' => $member->id ])}}">
                                            <i class="mdi-action-delete right"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div class="card-content">
                                    <p class="row">
                                        <span class="left"><a href="#">{{trans('app.nom')}}: {{$member->name}}</a></span>
                                    </p>
                                    <h5 class="card-title grey-text text-darken-4">
                                        <a href="#" class="grey-text text-darken-4"> {{$member->position}}</a>
                                    </h5>
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4"><i class="mdi-navigation-close right"></i></span>
                                    <p>{!! $member->bio !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
    @else
        <div id="error-page">

            <div class="row">
                <div class="col s12">
                    <div class="browser-window">
                        <div class="content">
                            <div class="row">
                                <div id="site-layout-example-top" class="col s12">
                                    <p class="flat-text-logo center white-text caption-uppercase"></p>
                                </div>
                                <div id="site-layout-example-right" class="col s12 m12 l12">
                                    <div class="row center">
                                        <br><br><br><br><br>
                                        <h1 class="white-text  col s12">{{trans('app.noTeam')}}</h1>
                                        <a class="waves-effect waves-light btn-large " href="{{route('team.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newTeam')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
