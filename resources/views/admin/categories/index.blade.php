@extends('layouts.app')

@section('title')
    {{trans('app.allCategoriesTitle')}}
@endsection
@section('content')
    @if($categories->count() > 0)
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    {{trans('app.allCategories')}}
                </h4>
            </div>
        </div>
        <br> <br>
    <a class="waves-effect waves-light btn-large " href="{{route('category.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newCategory')}}</a>

    <div class="col s12 m8 l9">
        <br>
        <table class="striped">
            <thead>
                <tr>
                    <th data-field="id">{{trans('app.nom')}}</th>
                    <th data-field="image">Image</th>
                    <th data-field="name">{{trans('app.edit')}}</th>
                    <th data-field="price">{{trans('app.delete')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->name}}</td>
                    <td> <img src="{{asset($category->image_url)}}" width="90px" height="50px" alt="{{$category->name}}"></td>
                    <td><a class="waves-effect waves-light btn blue" href="{{route('category.edit',   ['id' => $category->id ])}}"><i class="mdi-editor-border-color right"></i>{{trans('app.edit')}}</a></td>
                    <td><a class="waves-effect waves-light btn red " href="{{route('category.delete', ['id' => $category->id ])}}"><i class="mdi-action-delete right"></i>{{trans('app.delete')}}</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <div id="error-page">
                <div class="row">
                    <div class="col s12">
                        <div class="browser-window">
                            <div class="content">
                                <div class="row">
                                    <div id="site-layout-example-top" class="col s12">
                                        <p class="flat-text-logo center white-text caption-uppercase"></p>
                                    </div>
                                    <div id="site-layout-example-right" class="col s12 m12 l12">
                                        <div class="row center">
                                            <h3 class="col s12">Vous n'avez aucune Catégories pour l'instant !!!</h3>
                                            <a class="waves-effect waves-light btn-large  mt-5 mb-10" href="{{route('category.create')}}">
                                                <i class="mdi-content-add-circle right"></i>{{trans('app.newCategory')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>
@endsection