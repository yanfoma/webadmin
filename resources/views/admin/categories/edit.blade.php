@extends('layouts.app')

@section('title')
    {{trans('app.editCategoryTitle')}}
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                {{trans('app.editCategory')}} : {{$category->name}}
            </h4>
        </div>
    </div>
    <br> <br>
    <div class="col s12 m12 l6">
        <div class="card-panel">
            <div class="row">
                <form action="{{ route('category.update',['id' => $category->id]) }}"  method="post" class="col s12 right-alert">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="category" name="category" type="text" class="validate" value="{{$category->name}}">
                            <label for="category">{{trans('app.nom')}}</label>
                            @if ($errors->has('category'))
                                <div id="uname-error" class="error">{{ $errors->first('category') }}</div>
                            @endif
                        </div>
                        <div class="col s12 m8 l12">
                            @if ($errors->has('image'))
                                <br>
                                <div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                            @endif
                            <label for="image" class="">Image</label><br>
                            <input name="image" type="file" id="input-file-events" class="dropify-event"
                                   data-default-file="{{asset($category->image_url)}}" value="{{$category->image_url}}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.update')}}
                                <i class="mdi-content-save right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection