@extends('layouts.app')

@section('title')
    {{trans('app.trashedPostTitle')}}
@endsection

@section('content')
    <div class="col s12 m8 l9">
        @if($posts->count() > 0)
            <div id="card-alert" class="card red lighten-5">
                <div class="card-content red-text center-align">
                    <p>{{trans('app.completeDeleted')}}</p>
                </div>
                <button type="button" class="close red-text" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="row">
                <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                    <h4 class="text center">
                        {{trans('app.alltrashedPosts')}}
                    </h4>
                </div>
            </div>
            <br> <br>
        <table class="striped hoverable">
            <thead>
            <tr>
                <th data-field="id">{{trans('app.image')}}</th>
                <th data-field="id">{{trans('app.title')}}</th>
                <th data-field="id">{{trans('app.language')}}</th>
                <th data-field="price">{{trans('app.restore')}}</th>
                <th data-field="price">{{trans('app.delete')}}</th>
            </tr>
            </thead>
            <tbody>


                @foreach($posts as $post)
                    <tr>
                        <td> <img src="{{asset($post->featuredImage)}}" width="90px" height="50px" alt="{{$post->title}}"></td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->language}}</td>
                        <td><a class="waves-effect waves-light btn green " href="{{route('post.restore', ['id' => $post->post_id ])}}"><i class="mdi-action-cached right "></i> {{trans('app.restore')}}</a></td>
                        <td>
                            <form  id="myform" class=" waves-effect waves-light btn red" action="{{route('post.kill',    ['id' => $post->post_id ])}}" method="POST">
                                {{csrf_field()}}
                                <input type="submit" value="{{trans('app.delete')}}">
                                <i class="mdi-content-remove-circle right "></i>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
            @else
            <div id="error-page">

                <div class="row">
                    <div class="col s12">
                        <div class="browser-window">
                            <div class="content">
                                <div class="row">
                                    <div id="site-layout-example-top" class="col s12">
                                        <p class="flat-text-logo center white-text caption-uppercase"></p>
                                    </div>
                                    <div id="site-layout-example-right" class="col s12 m12 l12">
                                        <div class="row center">
                                            <br><br><br><br><br>
                                            <h1 class="white-text  col s12">{{trans('app.noTrashedPosts')}}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>
@endsection
@section('scripts')

    <script>
        $("#myform").on("submit", function(){
            event.preventDefault();
            swal({
                    title: 'Sure?',
                    text: "",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'No',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: true
                },
                function(){
                    $("#myform").submit();
                });
            /*.then(function() {
             $(".delete").off("submit").submit();
             }, function(dismiss) {
             // dismiss can be 'cancel', 'overlay',
             // 'close', and 'timer'
             if (dismiss === 'cancel') {
             alert('dismissed!');//swal('Cancelled', 'Delete Cancelled :)', 'error');
             }
             })*/
        });
    </script>
    <script>
        $("n#delete").on('click', function(){
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this lorem ipsum!",         type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function(){
                    $("#myform").submit();
                });
        })
    </script>
@endsection