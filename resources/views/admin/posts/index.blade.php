@extends('layouts.app')

@section('title')
    {{trans('app.allPostsTitle')}}
@endsection

@section('content')

    @if($posts->count() > 0)
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    {{trans('app.allPosts')}}
                </h4>
            </div>
        </div>
        <br> <br>
    <a class="waves-effect waves-light btn-large " href="{{route('post.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newPost')}}</a>

    <div class="col s12 m8 l9">
        <br>
        <table class="striped hoverable">
            <thead>
            <tr>
                <th data-field="id">{{trans('app.image')}}</th>
                <th data-field="id">{{trans('app.title')}}</th>
                <th data-field="id">{{trans('app.statut')}}</th>
                <th data-field="id">{{trans('app.type')}}</th>
                <th data-field="id">{{trans('app.language')}}</th>
                <th data-field="id">{{trans('app.edit')}}</th>
                <th data-field="id">{{trans('app.delete')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <td> <img src="{{asset($post->featuredImage)}}" width="90px" height="50px" alt="{{$post->title}}"></td>
                    <td>{{substr($post->title,0,40)."..."}}</td>
                    <td>
                        @if($post->translated == 1)<h9 class="blue-text  col s12">{{trans('app.public')}}</h9>
                        @else <h9 class="red-text  col s12">{{trans('app.notPublic')}}</h9>
                        @endif
                    </td>
                    <td>{{$post->blog}}</td>
                    <td>{{$post->language}}</td>
                    <td><a class="btn-floating activator btn waves-effect waves-light darken-2 orange"     href="{{route('post.edit',   ['id' => $post->post_id,'locale'=> $post->locale ])}}"><i class="mdi-editor-border-color right"></i></a></td>
                    <td><a class="btn-floating activator btn waves-effect waves-light darken-2 red right " href="{{route('post.delete', ['id' => $post->post_id ])}}"><i class="mdi-action-delete right"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br>
        <div class="col s12 m8 l3 center-align">
            {{$posts->render()}}
        </div>
        @else
            <div id="error-page">

                <div class="row">
                    <div class="col s12">
                        <div class="browser-window">
                            <div class="content">
                                <div class="row">
                                    <div id="site-layout-example-top" class="col s12">
                                        <p class="flat-text-logo center white-text caption-uppercase"></p>
                                    </div>
                                    <div id="site-layout-example-right" class="col s12 m12 l12">
                                        <div class="row center">
                                            <br><br><br><br><br>
                                            <h1 class="white-text  col s12">{{trans('app.noPost')}}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection