@extends('layouts.app')

@section('title')
    Notification for Applicant: {{$notification->name}}
@endsection

@section('content')
    <section id="content">
        <!--start container-->
            <div class="container"><div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                    <h4 class="text center">
                        Applicant: {{$notification->name}}
                    </h4>
                </div>
                <div class="invoice-lable">
                    <div class="row">
                        <div class="col s12 m3 l3 cyan">
                            <h4 class="white-text invoice-text">Client: {{$notification->name}}</h4>
                        </div>
                        <div class="col s12 m12 l12 invoice-brief cyan white-text">
                            <div class="row">
                                <div class="col s12 m4 l3">
                                    <p class="strong">Pays {{$notification->pays}}</p>
                                    <p class="strong">Ville: {{$notification->ville}}</p>
                                </div>
                                <div class="col s12 m4 l3">
                                    <p class="strong">Email: {{$notification->email}}</p>
                                    <p class="strong">Phone: {{$notification->phone}}</p>
                                </div>
                                <div class="col s12 m4 l3">
                                    <p class="strong">Address: {{$notification->addresse}}</p>
                                    <p class="strong">Nombre de Prosuits: {{$notification->nbrProduits}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="invoice-table">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <table class="striped">
                                <thead>
                                    <tr>
                                        <th data-field="no">No</th>
                                        <th data-field="item">Item</th>
                                        <th data-field="uprice">Price</th>
                                        <th data-field="price">Quantity</th>
                                        <th data-field="price">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($checkouts->count())
                                    @foreach($checkouts as $index=>$checkout)
                                        <tr>
                                            <td>{{++$index}}</td>
                                            <td>{{$checkout->name}}</td>
                                            <td>{{$checkout->prix}}</td>
                                            <td>{{$checkout->qty}}</td>
                                            <td>{{$checkout->total}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <h1>No product!!!</h1>
                                @endif
                                    <tr>
                                        <td colspan="3"></td>
                                        <td>Nombre de Prosuits</td>
                                        <td>{{$notification->nbrProduits}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="cyan white-text">Grand Total</td>
                                        <td class="cyan strong white-text">{{$notification->total}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
             </div>
        </div>
        <!--end container-->
    </section>
@endsection