@extends('layouts.app')

@section('title')
    Notification for Applicant: {{$notification->name}}
@endsection

@section('content')
    <section id="content">
        <!--start container-->
            <div class="container"><div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                    <h4 class="text center">
                        Applicant: {{$notification->name}}
                    </h4>
                </div>
                <div id="profile-page" class="section">
                    <!-- profile-page-content -->
                    <div id="profile-page-content" class="row">
                        <!-- profile-page-sidebar-->
                        <div id="profile-page-sidebar" class="col s12 m5">
                            <ul id="profile-page-about-details" class="collection z-depth-1">
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-social-person prefix"></i> {{trans('app.nom')}}</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$notification->name}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-communication-email"></i> Email</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$notification->email}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-communication-call"></i> {{trans('app.phone')}}</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$notification->phone}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-action-description"></i> Curriculum Vitae</div>
                                        <div class="col s7 grey-text text-darken-4 right-align"><a href="{{asset($notification->resume)}}" class="waves-effect waves-light btn"><i class="mdi-file-cloud-download"></i></a></div>
                                    </div>
                                </li>
                            </ul>
                            <!-- Profile About  -->
                        </div>
                        <div id="profile-page-sidebar" class="col s12 m7">
                            <div id="profile-page-wall-post" class="card">
                                <div class="card-profile-title">
                                    <div class="row">
                                        <div class="card-content">
                                            <p style="text-align: justify">{{$notification->message}}</p>
                                        </div>
                                        <div class="col s12" style="padding: 20px;">
                                            <span class="grey-text text-darken-1 ultra-small">{{trans('app.createdAt')}} </u></b>{{Date::parse($notification->created_at)->diffForHumans()}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Profile About  -->
                        </div>
                    </div>
                    <h4 class="text center">
                        JOB: {{$offer->name}}
                    </h4>
                    <div id="profile-page-content" class="row">
                        <!-- profile-page-sidebar-->
                        <div id="profile-page-sidebar" class="col s12 m12">
                            <ul id="profile-page-about-details" class="collection z-depth-1">
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-action-work prefix"></i> Job ID</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$offer->code}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-editor-format-list-bulleted"></i> {{trans('app.category')}}</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$offer->category}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-social-location-city"></i> {{trans('app.location')}}</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$offer->location}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-social-poll"></i> {{trans('app.position')}}</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$offer->nbrPosition}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-social-plus-one"></i> {{trans('app.experience')}}</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$offer->experience}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-content-report"></i> {{trans('app.dateEnd')}}</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$offer->dateEnd}}</div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <div class="row">
                                        <div class="col s5 grey-text darken-1"><i class="mdi-editor-attach-money"></i> {{trans('app.price')}}</div>
                                        <div class="col s7 grey-text text-darken-4 right-align">{{$offer->price}}</div>
                                    </div>
                                </li>
                            </ul>
                            <!-- Profile About  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end container-->
    </section>
@endsection