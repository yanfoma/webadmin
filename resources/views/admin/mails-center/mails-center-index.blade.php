@extends('layouts.app')

@section('title')
    Mails center
@stop

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                Espace d'envoie de mails aux clients
            </h4>
        </div>
    </div>
    &nbsp;
    &nbsp;
    <div class="row card-panel s12 m12 l6">
        <form action="{{route('yanfoma-customers-mails')}}" method="POST" class="col s12 right-alert" id="mailForm"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card-panel">
                <label for="moreusers" class="black-text text-darken-3" style="font-size: 15px; font-weight: bold">Destinataires Bcc</label>
                <select class="blue-text" multiple name="moreusers[]" id="moreusers">
                    <option selected disabled>Choisir les autres destinataires</option>
                    @foreach($users as $user)
                        <option value="{{$user->email}}">{{$user->email}}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-field col s12 card-panel">
                <i class="mdi-action-account-circle prefix"></i>
                <input id="subject" name="subject" type="text" class="validate" value="{{ old('subject') }}">
                <label for="subject">Objet</label>
                @if ($errors->has('subject'))
                    <div id="uname-error" class="error">{{ $errors->first('subject') }}</div>
                @endif
            </div>

            <div class="input-field col s12">
                <i class="mdi-action-question-answer prefix"></i>
                <textarea id="message" name="message" data-value="{{ old('message') }}"></textarea>
                @if ($errors->has('message'))
                    <div id="uname-error" class="error">{{ $errors->first('message') }}</div><br>
                @endif
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <button class="btn waves-effect waves-light right" type="submit"
                            name="submit">{{trans('app.create')}}
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection