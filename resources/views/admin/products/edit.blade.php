@extends('layouts.app')

@section('title')
    Modifier Produit
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                Modifier: {{$product->name}}
            </h4>
        </div>
    </div>
    <br> <br>
    <div class="row">
        <form action="{{ route('products.update',['id' => $product->id ] )}}" method="post" class="col s12 right-alert"
              enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-account-circle prefix"></i>
                    <input id="name" name="name" type="text" class="validate" value="{{$product->name}}">
                    <label for="name">Nom</label>
                    @if ($errors->has('name'))
                        <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                    @endif
                </div>
            </div>
            <div class="input-field col s6">
                <i class="mdi-action-account-circle prefix"></i>
                <input id="price" name="price" type="number" class="validate" value="{{$product->price}}">
                <label for="price">Prix en CFA</label>
                @if ($errors->has('price'))
                    <div id="uname-error" class="error">{{ $errors->first('price') }}</div>
                @endif
            </div>
            <div class="input-field col s6">
                <i class="mdi-action-account-circle prefix"></i>
                <input id="minQty" name="minQty" type="number" class="validate" value="{{$product->minQty}}">
                <label for="minQty">Quantité minimum</label>
                @if ($errors->has('minQty'))
                    <div id="uname-error" class="error">{{ $errors->first('minQty') }}</div>
                @endif
            </div>
            <div class="col s12 m8 l12">
                @if ($errors->has('image'))
                    <br>
                    <div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                @endif
                <label for="image" class="">Image</label><br>
                <input name="image" type="file" id="input-file-events" class="dropify-event"
                       data-default-file="{{asset($product->image)}}" value="{{$product->image}}"/>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi-action-question-answer prefix"></i>
                    <textarea id="description" name="description"
                              class="materialize-textarea">{{ $product->description }}</textarea>
                    @if ($errors->has('description'))
                        <div id="uname-error" class="error">{{ $errors->first('description') }}</div><br>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s8">
                    <button class="btn waves-effect waves-light right" type="submit" name="submit">Sauvegarder
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection