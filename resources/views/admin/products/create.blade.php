@extends('layouts.app')

@section('title')

    Créer Un Nouveau Produit
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                Nouveau Produit
            </h4>
        </div>
    </div>
    <br> <br>
    <a class="waves-effect waves-light btn modal-trigger  yellow red-text center-block text-darken-3" href="#modal3" id="modalTrigger"><i
                class="mdi-alert-warning"></i>{{trans('app.important')}}</a>

    <div id="modal3" class="modal"
         style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 491.5489130434783px;">
        <div class="modal-content teal white-text large text-capitalize">
            <p style="text-align: center">
                {{trans('app.SlideShowNotice')}}
            </p>
        </div>
        <div class="modal-footer  teal lighten-2">
            <a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">{{trans('app.ok')}}</a>
        </div>
    </div>
    <div class="col s12 m12 l6">
        <div class="card-panel">
            <div class="row">
                <form action="{{ route('products.store')}}" method="post" class="col s12 right-alert" id="pdCreateFormId"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="name" name="name" type="text" class="validate" value="{{ old('name') }}">
                            <label for="name">Nom</label>
                            @if ($errors->has('name'))
                                <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="price" name="price" type="number" class="validate" value="{{ old('price') }}">
                            <label for="price">Prix en CFA</label>
                            @if ($errors->has('price'))
                                <div id="uname-error" class="error">{{ $errors->first('price') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="minQty" name="minQty" type="number" class="validate" value="{{ old('minQty') }}">
                            <label for="minQty">Quantité minimum</label>
                            @if ($errors->has('minQty'))
                                <div id="uname-error" class="error">{{ $errors->first('minQty') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col s12 m8 l12">
                        @if ($errors->has('image'))
                            <br>
                            <div id="uname-error" class="error">{{ $errors->first('image') }}</div>

                        @endif
                        <label for="image" class="">Image</label><br>
                        <input name="image" type="file" id="input-file-events" class="dropify-event"
                               value="{{ old('image') }}"/>
                    </div>
                    <br>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="description" name="description"
                                      class="materialize-textarea">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <div id="uname-error" class="error">{{ $errors->first('description') }}</div><br>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light right" type="submit"
                                    name="submit">{{trans('app.create')}}
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
