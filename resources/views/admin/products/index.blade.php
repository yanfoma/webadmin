@extends('layouts.app')

@section('title')
    Tous les Produits dans le YanfomaShop
@endsection

@section('content')

    @if($products->count() > 0)
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    Tous les Produits dans le YanfomaShop
                </h4>
            </div>
        </div>
        <br> <br>
    <a class="waves-effect waves-light btn-large " href="{{route('products.create')}}"><i class="mdi-content-add-circle right"></i>Ajouter Un produit</a>

    <div class="col s12 m8 l9">
        <br>
        <table class="striped hoverable">
            <thead>
            <tr>
                <th data-field="id">Image</th>
                <th data-field="id">Nom</th>
                <th data-field="id">Prix</th>
                <th data-field="id">Quantité Minimum(MOQ)</th>
                <th data-field="id">Description</th>
                <th data-field="id">Modifier</th>
                <th data-field="id">Supprimer</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td> <img src="{{asset($product->image)}}" width="90px" height="50px" alt="{{$product->name}}"></td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->minQty}}</td>
                    <td>{!!$product->description !!}</td>
                    <td><a class="btn-floating activator btn waves-effect waves-light darken-2 orange"     href="{{route('products.edit',   ['id' => $product->id ])}}"><i class="mdi-editor-border-color right"></i></a></td>
                    <td><a class="btn-floating activator btn waves-effect waves-light darken-2 red right " href="{{route('products.delete', ['id' => $product->id ])}}"><i class="mdi-action-delete right"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br>
        <div class="col s12 m8 l3 center-align">
            {{$products->render()}}
        </div>
        @else
            <div id="error-page">

                <div class="row">
                    <div class="col s12">
                        <div class="browser-window">
                            <div class="content">
                                <div class="row">
                                    <div id="site-layout-example-top" class="col s12">
                                        <p class="flat-text-logo center white-text caption-uppercase"></p>
                                    </div>
                                    <div id="site-layout-example-right" class="col s12 m12 l12">
                                        <div class="row center">
                                            <br><br><br><br><br>
                                            <h1 class="white-text  col s12">Aucun Produit pour l'instant</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection