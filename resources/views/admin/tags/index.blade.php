@extends('layouts.app')

@section('title')
    {{trans('app.allTagsTitle')}}
@endsection

@section('content')
    @if($tags->count() > 0)
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1" >
            <h4 class="text center">
                {{trans('app.allTags')}}
            </h4>
        </div>
    </div>
    <br> <br>
    <a class="waves-effect waves-light btn-large " href="{{route('tags.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newTag')}}</a>

    <div class="col s12 m8 l9">
        <br>
        <table class="striped">
            <thead>
                <tr>
                    <th data-field="id">{{trans('app.nom')}}</th>
                    <th data-field="name">{{trans('app.edit')}}</th>
                    <th data-field="price">{{trans('app.delete')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tags as $tag)
                <tr>
                    <td>{{$tag->tag}}</td>
                    <td><a class="waves-effect waves-light btn blue" href="{{route('tags.edit',   ['id' => $tag->id ])}}"><i class="mdi-editor-border-color right"></i>{{trans('app.edit')}}</a></td>
                    <td>
                        <form  id="myform" class=" waves-effect waves-light btn red" action="{{route('tags.delete',    ['id' => $tag->id ])}}" method="POST">
                            {{csrf_field()}}
                            <input type="submit" value="{{trans('app.delete')}}">
                            <i class="mdi-action-delete right"></i>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <div id="error-page">

                <div class="row">
                    <div class="col s12">
                        <div class="browser-window">
                            <div class="content">
                                <div class="row">
                                    <div id="site-layout-example-top" class="col s12">
                                        <p class="flat-text-logo center white-text caption-uppercase"></p>
                                    </div>
                                    <div id="site-layout-example-right" class="col s12 m12 l12">
                                        <div class="row center">

                                            <br><br><br><br><br>
                                            <h1 class="white-text  col s12">{{trans('app.noTag')}}</h1>
                                            <a class="waves-effect waves-light btn-large " href="{{route('tags.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newTag')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>
@endsection

    @section('scripts')

        <script>
            $("#myform").on("submit", function(){
                event.preventDefault();
                swal({
                    title: 'Sure?',
                    text: "",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'No',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: true
                },
                    function(){
                        $("#myform").submit();
                    });
                    /*.then(function() {
                    $(".delete").off("submit").submit();
                }, function(dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    if (dismiss === 'cancel') {
                        alert('dismissed!');//swal('Cancelled', 'Delete Cancelled :)', 'error');
                    }
                })*/
            });
        </script>
        <script>
            $("n#delete").on('click', function(){
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this lorem ipsum!",         type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function(){
                        $("#myform").submit();
                    });
            })
        </script>
    @endsection