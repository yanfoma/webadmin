@extends('layouts.login')

@section('title')
    YanfoShop Login
@endsection

@section('content')
    <div class="row">
        <div class="col s12">
            <div class="container"><div id="login-page" class="row">
                    <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                        <form class="login-form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="input-field col s12 center-align mt-10">
                                    <img class="z-depth-4 circle responsive-img" width="100" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1538335932/Yanfoma/Yanfoma-New-Logo1.png" alt="">
                                    <h5>WEB ADMIN</h5>
                                </div>
                            </div>
                            <div class="row margin">
                                <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <i class="material-icons prefix pt-2">email</i>
                                    <input id="email" type="email" name="email" value="{{ old('email')}}">
                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                    <label for="email" class="center-align">{{ trans('app.email') }} </label>
                                </div>
                            </div>
                            <div class="row margin">
                                <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <i class="material-icons prefix pt-2">lock_outline</i>
                                    <input id="password" type="password" class="form-control" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                    @endif
                                    <label for="password">{{ trans('app.password') }} </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l12 ml-2 mt-1">
                                    <p>
                                        <label>
                                            <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <span>{{ trans('app.rememberMe') }}</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-light-blue-cyan col s12">
                                        {{ trans('app.login') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection