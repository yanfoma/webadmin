@extends('layouts.app')

@section('title')
    404 Yanfoma
@endsection

@section('content')
    <section class="error-section sec-padd">
        <div class="container">
            <div class="row">
                <div class="image-column col-md-5 col-sm-12 col-xs-12">
                    <div class="error-image">
                        <img src="{{asset('images/frontEnd/error-image.jpg')}}" alt="error-image"></div>
                </div>
                <div class="content-column col-md-7 col-sm-12 col-xs-12">
                    <h2 class="extra-big">404!!!</h2>
                    <div class="bigger-text">La page que vous recherchez n'existe pas. Elle a peut-être été déplacée ou retirée tout simplement. Veuillez revenir à la page</div>
                    <div class="text-lower center">
                        <a href="https://yanfoma.tech/fr/acceuil" class="theme-btn btn-style-one">Accueil</a>
                    </div>
                    {{--<div class="small-text">{{ $exception->getMessage() }} </div>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
