@extends('layouts.app')

@section('title')
    Liked Items
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default">
                                <div class="card-content">
                                    <h4 class="card-title center"> Liked Items</h4>
                                    <div class="row">
                                        @if($liked->count() > 0)
                                            <div class="col s12 m12 l12">
                                                <br>
                                                <table id="example" class="striped hoverable cell-border">
                                                    <thead>
                                                    <tr>
                                                        <th>{{ trans('app.nom') }}</th>
                                                        <th>By</th>
                                                        <th>Count</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($liked as $item)
                                                            <tr>
                                                                <td>{{$item->product->name}}</td>
                                                                <td> @foreach($item->users() as $user) {{getUser($user->user_id)->name}},  @endforeach
                                                                </td>
                                                                <td>{{$item->product->likes->count()}}</td>
                                                                <td>{{$item->created_at}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Total</th>
                                                            <th></th>
                                                            <th>{{$liked_count}}</th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                {{--<div class="col s12 m8 l3 center-align pagination">--}}
                                                    {{--{{$products->render()}}--}}
                                                {{--</div>--}}
                                                @else
                                                    <div id="error-page">
                                                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="browser-window">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div id="site-layout-example-top" class="col s12">
                                                                                <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                            </div>
                                                                            <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                                <div class="row center">
                                                                                    <br><br><br><br><br>
                                                                                    <h1 class="white-text  col s12">No Purcahsed Item</h1>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                paging: false,
                searching: true,
                ordering:  true
            });
        } );
    </script>
@endsection