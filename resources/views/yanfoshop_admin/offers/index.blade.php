@extends('layouts.app')

@section('title')
    {{trans('app.offers')}}
@endsection

@section('content')

    @if($offers->count() > 0)
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    {{trans('app.offers')}}
                </h4>
            </div>
        </div>
        <br> <br>
    <a class="waves-effect waves-light btn-large " href="{{route('offer.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newOffer')}}</a>

    <div class="col s12 m8 l9">
        <br>
        <table class="striped hoverable">
            <thead>
            <tr>
                <th data-field="id">{{trans('code')}}</th>
                <th data-field="id">{{trans('app.nom')}}</th>
                <th data-field="id">{{trans('app.description')}}</th>
                <th data-field="id">{{trans('app.category')}}</th>
                <th data-field="id">{{trans('app.nbrPosition')}}</th>
                <th data-field="id">{{trans('app.experience')}}</th>
                <th data-field="id">{{trans('app.location')}}</th>
                <th data-field="id">{{trans('app.dateEnd')}}</th>
                <th data-field="id">{{trans('app.price')}}</th>
                <th data-field="id">{{trans('app.status')}}</th>
                <th data-field="id">{{trans('app.edit')}}</th>
                <th data-field="id">{{trans('app.delete')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($offers as $offer)
                <tr>
                    <td>{{$offer->code}}</td>
                    <td>{{$offer->name}}</td>
                    <td>{{substr($offer->title,0,40)."..."}}</td>
                    <td>{{$offer->category}}</td>
                    <td>{{$offer->nbrPosition}}</td>
                    <td>{{$offer->experience}}</td>
                    <td>{{$offer->location}}</td>
                    <td>{{$offer->dateEnd}}</td>
                    <td>{{$offer->price}}</td>
                    <td>{{$offer->status}}</td>
                    <td><a class="btn-floating activator btn waves-effect waves-light darken-2 orange"     href="{{route('offer.edit',   ['id' => $offer->id ])}}"><i class="mdi-editor-border-color right"></i></a></td>
                    <td><a class="btn-floating activator btn waves-effect waves-light darken-2 red right " href="{{route('offer.delete', ['id' => $offer->id ])}}"><i class="mdi-action-delete right"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br>
        <div class="col s12 m8 l3 center-align">
            {{$offers->render()}}
        </div>
        @else
            <div id="error-page">

                <div class="row">
                    <div class="col s12">
                        <div class="browser-window">
                            <div class="content">
                                <div class="row">
                                    <div id="site-layout-example-top" class="col s12">
                                        <p class="flat-text-logo center white-text caption-uppercase"></p>
                                    </div>
                                    <div id="site-layout-example-right" class="col s12 m12 l12">
                                        <div class="row center">
                                            <br><br><br><br><br>
                                            <h1 class="white-text  col s12">{{trans('app.noOffer')}}</h1>
                                            <a class="waves-effect waves-light btn-large " href="{{route('offer.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newOffer')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection