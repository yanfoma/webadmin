@extends('layouts.app')

@section('title')
    {{trans('app.newOffer')}}
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                {{trans('app.newOffer')}}
            </h4>
        </div>
    </div>
    <br> <br>
    <div class="col s12 m12 l6">
        <div class="card-panel">
            <div class="row">
                <form action="{{ route('offer.store')}}"  method="post" class="col s12 right-alert" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="name" name="name" type="text" class="validate" value="{{ old('name') }}">
                            <label for="name">{{trans('app.offerName')}}</label>
                            @if ($errors->has('name'))
                                <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="category" name="category" type="text" class="validate" value="{{ old('category') }}">
                            <label for="category">{{trans('app.category')}}</label>
                            @if ($errors->has('category'))
                                <div id="uname-error" class="error">{{ $errors->first('category') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="experience" name="experience" type="text" class="validate" value="{{ old('experience') }}">
                            <label for="experience">{{trans('app.experience')}}</label>
                            @if ($errors->has('experience'))
                                <div id="uname-error" class="error">{{ $errors->first('experience') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="location" name="location" type="text" class="validate" value="{{ old('location') }}">
                            <label for="location">{{trans('app.location')}}</label>
                            @if ($errors->has('location'))
                                <div id="uname-error" class="error">{{ $errors->first('location') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="dateEnd" name="dateEnd" type="text" class="validate datepicker" value="{{ old('dateEnd') }}">
                            <label for="dateEnd">{{trans('app.dateEnd')}}</label>
                            @if ($errors->has('dateEnd'))
                                <div id="uname-error" class="error">{{ $errors->first('dateEnd') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="price" name="price" type="number" class="validate" value="{{ old('price') }}">
                            <label for="price">{{trans('app.price')}}</label>
                            @if ($errors->has('price'))
                                <div id="uname-error" class="error">{{ $errors->first('price') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="nbrPosition" name="nbrPosition" type="number" class="validate" value="{{ old('nbrPosition') }}">
                            <label for="nbrPosition">{{trans('app.nbrPosition')}}</label>
                            @if ($errors->has('nbrPosition'))
                                <div id="uname-error" class="error">{{ $errors->first('nbrPosition') }}</div>
                            @endif
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="description" name="description" class="materialize-textarea">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <div id="uname-error" class="error">{{ $errors->first('description') }}</div><br>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.create')}}
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection