@extends('layouts.app')

@section('title')
   Create A New Sale
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center"> New Sale</h4>
                                    <div class="row">
                                        <form action="{{ route('sales.store') }}"  method="post" enctype="multipart/form-data" class="col s12 right-alert">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <span class="card-title">For a  Specific Product in the store</span>
                                                    <div class="select-wrapper initialized">
                                                        <select name="product_id" class="initialized">
                                                            <option value="" disabled="" selected="">Product</option>
                                                            @if(old('product_id'))
                                                                <option value="{{ old('product_id') }}" selected disabled>{{ old('product_id') }}</option>
                                                            @endif
                                                            @foreach($products as $product)
                                                                <option value="{{ $product->id }}"> {{ $product->name }}  {{ $product->price }} CFA </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('product_id'))
                                                        <div id="uname-error" class="error">{{ $errors->first('product_id') }}</div><br>
                                                    @endif
                                                </div>
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-stars prefix"></i>
                                                    <input id="sale_amount" name="sale_amount" type="text" class="validate" value="{{old('sale_amount')}}">
                                                    <label for="sale_amount">Sale Amount in CFA</label>
                                                    @if ($errors->has('sale_amount'))
                                                        <div id="uname-error" class="error">{{ $errors->first('sale_amount') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.create')}}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <h5 class="text center">
                                Or
                            </h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <div class="row">
                                        <form action="{{ route('sales.storePercentage') }}"  method="post" enctype="multipart/form-data" class="col s12 right-alert">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <span class="card-title">For All The Products in the store</span>
                                                </div>
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-stars prefix"></i>
                                                    <input id="sale_percentage" name="sale_percentage" type="number" class="validate" value="{{old('sale_percentage')}}">
                                                    <label for="sale_percentage">Sale Percentage</label>
                                                    @if ($errors->has('sale_percentage'))
                                                        <div id="uname-error" class="error">{{ $errors->first('sale_percentage') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.create')}}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/scripts/form-elements.js')}}" type="text/javascript"></script>
@endsection