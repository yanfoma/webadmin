@extends('layouts.app')

@section('title')
    Tous Les Soldes
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    @if($sales->count() > 0)
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div id="highlight-table" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <h4 class="card-title center">Tous Les Soldes</h4>
                                        <a class="waves-effect waves-light btn-large gradient-45deg-blue-indigo gradient-shadow " href="{{route('sales.create')}}"><i class="material-icons">add_circle</i>New Sale</a>
                                        <a class="waves-effect waves-light btn-large gradient-45deg-red-pink gradient-shadow " href="{{route('sales.deleteAll')}}"><i class="material-icons">delete</i>Delete All</a>
                                        <div class="row">
                                            <div class="col s12">
                                            </div>
                                            <div class="col s12">
                                                <table class="highlight">
                                                    <thead>
                                                    <tr>
                                                        <th>{{trans('app.nom')}}</th>
                                                        <th>Sale Amount</th>
                                                        <th>{{trans('app.edit')}}</th>
                                                        <th>{{trans('app.delete')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse($sales as $sale)
                                                            <tr>
                                                                <td>{{$sale->product->name}}</td>
                                                                <td>{{$sale->product->sale_amount}} CFA</td>
                                                                <td><a class="waves-effect waves-light btn blue" href="{{route('sales.edit',   ['id' => $sale->id ])}}"><i class="material-icons">edit</i></a></td>
                                                                <td><a class="waves-effect waves-light btn red " href="{{route('sales.delete', ['id' => $sale->id ])}}"><i class="material-icons">delete</i></a></td>
                                                            </tr>
                                                        @empty
                                                            <h1> No Sales yet!!!</h1>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                                @else
                                                    <div id="error-page">
                                                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="browser-window">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div id="site-layout-example-top" class="col s12">
                                                                                <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                            </div>
                                                                            <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                                <div class="row center ">
                                                                                    <h1 class="col s12 black-text">Aucun Solde !!!</h1>
                                                                                    @if($products->count())
                                                                                        <a class="waves-effect waves-light btn-large gradient-45deg-red-pink gradient-shadow " href="{{route('sales.create')}}">Create a Sale</a>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection