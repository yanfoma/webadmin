@extends('layouts.app')

@section('title')
    Sale {{$sale->product->name}}
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                Edit Sale {{$sale->product->name}}
            </h4>
        </div>
    </div>
    <br> <br>
    <div class="col s12 m12 l6">
        <div class="card-panel">
            <div class="row">
                <form action="{{ route('sales.update',['id' => $sale->id]) }}"  method="post" class="col s12 right-alert">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12">
                            <div class="input-field col s12">
                                <i class="mdi-action-account-circle prefix"></i>
                                <input id="product_name" name="product_name" type="text"     class="validate" value="{{ $sale->product->name }} {{ $sale->product->price }} CFA" disabled>
                                <input id="product_id"   name="product_id"   type="hidden"   class="validate" value="{{ $sale->product->id }}">
                                <label for="product_id">Product</label>
                                @if ($errors->has('product_id'))
                                    <div id="uname-error" class="error">{{ $errors->first('product_id') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="input-field col s12">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="sale_amount" name="sale_amount" type="text" class="validate" value="{{$sale->product->sale_amount}}">
                            <label for="sale_amount">Sale Amount in CFA</label>
                            @if ($errors->has('sale_amount'))
                                <div id="uname-error" class="error">{{ $errors->first('sale_amount') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.update')}}
                                <i class="mdi-content-save right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection