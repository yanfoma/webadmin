@extends('layouts.app')

@section('title')
    {{ trans('app.usersTitle') }}
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center"> Nos Clients</h4>
                                        <div class="row">
                                            @if($users->count() > 0)
                                            <div class="col s12 m12 l12">
                                                <br>
                                                <table id="example" class="striped hoverable cell-border">
                                                    <thead>
                                                    <tr>
                                                        <th data-field="id">{{ trans('app.nom') }}</th>
                                                        <th data-field="id">Email</th>
                                                        <th data-field="name">Joined</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($users as $user)
                                                        <tr>
                                                            <td>{{$user->name}}</td>
                                                            <td>{{$user->email}}</td>
                                                            <td>{{$user->created_at}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <div class="col s12 m8 l3 center-align pagination">
                                                    {{$users->links()}}
                                                </div>
                                                @else
                                                    <div id="error-page">

                                                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="browser-window">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div id="site-layout-example-top" class="col s12">
                                                                                <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                            </div>
                                                                            <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                                <div class="row center">
                                                                                    <br><br><br><br><br>
                                                                                    <h1 class="white-text  col s12">No Users</h1>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                paging: false,
                searching: true,
                ordering:  true
            });
        } );
    </script>
@endsection