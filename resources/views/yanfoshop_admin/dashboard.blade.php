@extends('layouts.app')

@section('title')
    Admin
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 gradient-shadow gradient-45deg-light-blue-cyan white-text z-depth-3">
            <h4 class="text center white-text">
                {{ trans('app.welcome') }} {{Auth::user()->name}}
            </h4>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card" style="padding: 20px;">
                <div id="card-stats">
                    <div class="row">
                        <div class="col s12 m6 l6 xl3">
                            <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text animate fadeLeft">
                                <div class="padding-4">
                                    <div class="col s7 m7">
                                        <i class="material-icons background-round mt-5">add_shopping_cart</i>
                                        <p>Commandes</p>
                                    </div>
                                    <div class="col s5 m5 right-align">
                                        <h5 class="mb-0 white-text">{{ $purchased_count }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3">
                            <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text animate fadeLeft">
                                <div class="padding-4">
                                    <div class="col s7 m7">
                                        <i class="material-icons background-round mt-5">perm_identity</i>
                                        <p>Clients</p>
                                    </div>
                                    <div class="col s5 m5 right-align">
                                        <h5 class="mb-0 white-text">{{ $users_count }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3">
                            <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text animate fadeRight">
                                <div class="padding-4">
                                    <div class="col s7 m7">
                                        <i class="material-icons background-round mt-5">payment</i>
                                        <p>Payments</p>
                                    </div>
                                    <div class="col s5 m5 right-align">
                                        <h5 class="mb-0 white-text">{{ $payments_count }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3">
                            <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text animate fadeRight">
                                <div class="padding-4">
                                    <div class="col s7 m7">
                                        <i class="material-icons background-round mt-5">trending_down</i>
                                        <p>Coupons</p>
                                    </div>
                                    <div class="col s5 m5 right-align">
                                        <h5 class="mb-0 white-text">{{ $coupons_count }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m6 l6 xl3">
                            <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text animate fadeLeft">
                                <div class="padding-4">
                                    <div class="col s7 m7">
                                        <i class="material-icons background-round mt-5">add_shopping_cart</i>
                                        <p>Produits</p>
                                    </div>
                                    <div class="col s5 m5 right-align">
                                        <h5 class="mb-0 white-text">{{ $products_count }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3">
                            <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text animate fadeLeft">
                                <div class="padding-4">
                                    <div class="col s7 m7">
                                        <i class="material-icons background-round mt-5">favorite_border</i>
                                        <p>Favoris</p>
                                    </div>
                                    <div class="col s5 m5 right-align">
                                        <h5 class="mb-0 white-text">{{ $favorited_count }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3">
                            <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text animate fadeRight">
                                <div class="padding-4">
                                    <div class="col s7 m7">
                                        <i class="material-icons background-round mt-5">thumb_up</i>
                                        <p>Aimes</p>
                                    </div>
                                    <div class="col s5 m5 right-align">
                                        <h5 class="mb-0 white-text">{{ $liked_count }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m6 l6 xl3">
                            <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text animate fadeRight">
                                <div class="padding-4">
                                    <div class="col s7 m7">
                                        <i class="material-icons background-round mt-5">timeline</i>
                                        <p>Soldes</p>
                                    </div>
                                    <div class="col s5 m5 right-align">
                                        <h5 class="mb-0 white-text">{{ $sales_count }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($categories->count())
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card subscriber-list-card animate fadeRight">
                <div class="card-content pb-1">
                    <h4 class="card-title center">Nos Récents Produits</h4>
                </div>
                <div class="section mt-1 mb-10" id="blog-list">
                    @if($products->count())
                        <div class="row">
                            @foreach($products as $product)
                                <div class="col s12 m6 l4">
                                    <div class="card-panel border-radius-6 mt-10 card-animation-1">
                                        <a href="#"><img class="responsive-img border-radius-8 z-depth-4 image-n-margin" src="{{$product->image_url}}" alt=""></a>
                                        <h6 class="deep-purple-text text-darken-3 mt-5"><a href="#">{{$product->name}}</a></h6>
                                        {!! html_entity_decode(strip_tags($product->description)) !!}
                                        <div class="row mt-4">
                                            <div class="col s5 p-0 mt-1">
                                                <span class="pt-2">Prix: {{$product->price}}</span>
                                            </div>
                                            <div class="col s7 mt-3 right-align social-icon">
                                                <span class="material-icons">favorite_border</span> <span class="ml-3 vertical-align-top">{{$product->wishlists()->count()}}</span>
                                                <span class="material-icons ml-10">thumb_up</span> <span class="ml-3 vertical-align-top">{{$product->likes()->count()}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="col s12 m12 l12">
                            <div class="card-alert card pink lighten-5">
                                <div class="card-content pink-text darken-1 center">
                                    <p>Vous n'avez aucun produit pour l'instant !!!</p>
                                </div>
                                <div class="card-action pink lighten-4">
                                    <a class="btn-flat waves-effect red white-text mb-1" style="margin-left: 30%" href="{{route('products.create')}}">
                                        <i class="material-icons left">add</i> Ajouter un Nouveau Produit
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
            <div class="col s12 m12 l12">
                <div class="card subscriber-list-card animate fadeRight">
                        <div class="card-content pb-1">
                            <h4 class="card-title center">Nos Récentes Catégories</h4>
                        </div>
                    @if($categories->count())
                        @foreach($categories as $category)
                            <div class="col s12 m6 l4 pb-2">
                                <div class="card-panel border-radius-6 pt-4 pb-4">
                                    <span class="red-text"><i class="material-icons vertical-align-bottom"> trending_up </i> Catégorie</span>
                                    <p class="mt-4 mb-0">{{$category->name}} <br>({{$category->products()->count()}}) produits</p>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col s12 m12 l12">
                            <div class="card-alert card pink lighten-5">
                                <div class="card-content pink-text darken-1 center">
                                    <p>Vous n'avez aucune Catégories pour l'instant !!!</p>
                                </div>
                                <div class="card-action pink lighten-4">
                                    <a class="btn-flat waves-effect red white-text mb-1" style="margin-left: 30%" href="{{route('category.create')}}">
                                        <i class="material-icons left">add</i> Créer une Nouvelle Catégorie
                                    </a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        @endif
    </div>
    <div class="row">
       @if($users->count()) <div class="col s12 m12 l12">
            <div class="card subscriber-list-card animate fadeRight">
                <div class="card-content pb-1">
                    <h4 class="card-title center">Nos Cients</h4>
                </div>
                <table class="subscription-table responsive-table highlight">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Email</th>
                        <th>Portable</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->wa_number}}</td>
                                <td>{{$user->created_at}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div> @endif
    </div>
    @if(Auth::user()->admin == 1 || Auth::user()->admin == 4)
        <div id="ecommerce-product">
            <!-- ecommerce offers start-->
            <div id="ecommerce-offer">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card subscriber-list-card animate fadeRight">
                            <div class="card-content pb-1">
                                <h4 class="card-title center">Nos Managers</h4>
                            </div>
                            @foreach($teams as $team)
                                <div class="col s12 m3">
                                    <div class="card gradient-shadow @if($team->admin == 1) gradient-45deg-light-blue-cyan  @elseif($team->admin ==2 ) gradient-45deg-red-pink @else gradient-45deg-green-teal @endif border-radius-3 animate fadeUp">
                                        <div class="card-content center">
                                            <img src="https://res.cloudinary.com/yanfomaweb/image/upload/v1539208885/Yanfoma/avatar-1.png" class="width-40 border-round z-depth-5"
                                                 alt="" class="responsive-img" />
                                            <h5 class="white-text lighten-4">{{$team->name}}</h5>
                                            <p class="white-text lighten-4">{{$team->position()}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($favorited_count !=0 || $liked_count != 0)
        <canvas id="myChart" width="400" height="200"></canvas>
    @endif
@endsection

@section('scripts')

    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{ asset('vendors/chartjs/chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendors/chartist-js/chartist.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendors/chartist-js/chartist-plugin-tooltip.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendors/chartist-js/chartist-plugin-fill-donut.min.js') }}" type="text/javascript"></script>

    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ["Number of Favorited", "Favorited Products", "Liked Products", "Number of Likes", "All Products"],
                datasets: [{
                    label: '# of Votes',
                    data: [{{$favorited_count}}, {{$favorited->count()}}, {{$liked->count()}}, {{$liked_count}}, {{$products_count}}],
                    backgroundColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Stats Aimes et Favoris'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });
    </script>
@endsection
