@extends('layouts.app')

@section('title')
    All Coupons
@endsection

@section('content')
    @if($coupons->count() > 0)
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    All Coupons
                </h4>
            </div>
        </div>
        <br> <br>
        <a class="waves-effect waves-light btn-large " href="{{route('coupons.create')}}"><i class="mdi-content-add-circle right"></i>{{trans('app.newCategory')}}</a>
        <div class="col s12 m8 l9">
            <br>
                <table class="striped">
                    <thead>
                    <tr>
                        <th data-field="id">{{trans('app.nom')}}</th>
                        <th data-field="name">{{trans('app.edit')}}</th>
                        <th data-field="price">{{trans('app.delete')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($coupons as $coupon)
                        <tr>
                            <td>{{$coupon->name}}</td>
                            <td><a class="waves-effect waves-light btn blue" href="{{route('coupon.edit',   ['id' => $coupon->id ])}}"><i class="mdi-editor-border-color right"></i>{{trans('app.edit')}}</a></td>
                            <td><a class="waves-effect waves-light btn red " href="{{route('coupon.delete', ['id' => $coupon->id ])}}"><i class="mdi-action-delete right"></i>{{trans('app.delete')}}</a></td>
                        </tr>
                        @empty
                        <h1> No Coupons yet!!!</h1>
                    @endforelse
                    </tbody>
                </table>
            @else
                <div id="error-page">
                    <div class="row">
                        <div class="col s12">
                            <div class="browser-window">
                                <div class="content">
                                    <div class="row">
                                        <div id="site-layout-example-top" class="col s12">
                                            <p class="flat-text-logo center white-text caption-uppercase"></p>
                                        </div>
                                        <div id="site-layout-example-right" class="col s12 m12 l12">
                                            <div class="row center">
                                                <br><br><br><br><br>
                                                <h1 class="white-text  col s12">No Coupons Yet !!!</h1>
                                                <a class="waves-effect waves-light btn-large " href="{{route('coupons.create')}}">
                                                    <i class="mdi-content-add-circle right"></i>Create a Coupon </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
@endsection