@extends('layouts.app')

@section('title')
   Create A New Coupon
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                New Coupon
            </h4>
        </div>
    </div>
    <br> <br>
    <div class="col s12 m12 l6">
        <div class="card-panel">
            <div class="row">
                <form action="{{ route('category.store') }}"  method="post" enctype="multipart/form-data" class="col s12 right-alert">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-field col s4">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="name" name="name" type="text" class="validate" value="{{old('name')}}">
                            <label for="name">{{trans('app.nom')}}</label>
                            @if ($errors->has('name'))
                                <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s4">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="type" name="type" type="text" class="validate" value="{{old('type')}}">
                            <label for="type">type</label>
                            @if ($errors->has('type'))
                                <div id="uname-error" class="error">{{ $errors->first('type') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s4">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="discount_amount" name="discount_amount" type="text" class="validate" value="{{old('discount_amount')}}">
                            <label for="discount_amount">discount_amount</label>
                            @if ($errors->has('discount_amount'))
                                <div id="uname-error" class="error">{{ $errors->first('discount_amount') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s2">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="is_fixed" name="is_fixed" type="text" class="validate" value="{{old('is_fixed')}}">
                            <label for="is_fixed">is_fixed</label>
                            @if ($errors->has('is_fixed'))
                                <div id="uname-error" class="error">{{ $errors->first('is_fixed') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s2">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="percent_off" name="percent_off" type="text" class="validate" value="{{old('percent_off')}}">
                            <label for="percent_off">percent_off</label>
                            @if ($errors->has('percent_off'))
                                <div id="uname-error" class="error">{{ $errors->first('percent_off') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s4">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="max_uses_user" name="max_uses_user" type="text" class="validate" value="{{old('max_uses_user')}}">
                            <label for="max_uses_user">max_uses_user</label>
                            @if ($errors->has('max_uses_user'))
                                <div id="uname-error" class="error">{{ $errors->first('max_uses_user') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s4">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="category" name="category" type="text" class="validate" value="{{old('category')}}">
                            <label for="category">number_of_use</label>
                            @if ($errors->has('category'))
                                <div id="uname-error" class="error">{{ $errors->first('category') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="starts_at" name="starts_at" type="text" class="validate datepicker" value="{{old('starts_at')}}">
                            <label for="starts_at">starts_at</label>
                            @if ($errors->has('starts_at'))
                                <div id="uname-error" class="error">{{ $errors->first('starts_at') }}</div>
                            @endif
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="expires_at" name="expires_at" type="text" class="validate datepicker" value="{{old('expires_at')}}">
                            <label for="expires_at">expires_at</label>
                            @if ($errors->has('expires_at'))
                                <div id="uname-error" class="error">{{ $errors->first('expires_at') }}</div>
                            @endif
                        </div>
                        {{--<div class="col s12 m8 l12">--}}
                            {{--@if ($errors->has('image'))--}}
                                {{--<br>--}}
                                {{--<div id="uname-error" class="error">{{ $errors->first('image') }}</div>--}}
                            {{--@endif--}}
                            {{--<label for="image" class="">Image</label><br>--}}
                            {{--<input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>--}}
                        {{--</div>--}}
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.create')}}
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection