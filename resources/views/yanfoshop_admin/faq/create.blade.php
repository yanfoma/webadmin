@extends('layouts.app')

@section('title')
    New Faq
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center"> New FAQ</h4>
                                    <div class="row">
                                        <form action="{{ route('faq.store') }}"  method="post" enctype="multipart/form-data" class="col s12 right-alert">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="question" name="question" type="text" class="validate" value="{{old('question')}}">
                                                    <label for="question">Question</label>
                                                    @if ($errors->has('question'))
                                                        <div id="uname-error" class="error">{{ $errors->first('question') }}</div>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <i class="mdi-action-question-answer prefix"></i>
                                                        <textarea id="response" name="response" class="materialize-textarea">{{ old('response') }}</textarea>
                                                        @if ($errors->has('response'))
                                                            <div id="uname-error" class="error">{{ $errors->first('response') }}</div><br>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.create')}}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection