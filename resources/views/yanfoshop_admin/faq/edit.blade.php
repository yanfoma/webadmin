@extends('layouts.app')

@section('title')
    Edit Faq : {{$faq->question}}
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h2 class="card-title center">  Edit FAQ: {{$faq->question}}</h2>
                                    <div class="row">
                                        <form action="{{ route('faq.update',['id' => $faq->id]) }}"  method="post" class="col s12 right-alert">
                                            {{ csrf_field() }}

                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="question" name="question" type="text" class="validate" value="{{$faq->question}}">
                                                    <label for="interest">Question</label>
                                                    @if ($errors->has('question'))
                                                        <div id="uname-error" class="error">{{ $errors->first('question') }}</div>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <i class="mdi-action-question-answer prefix"></i>
                                                        <textarea id="response" name="response" class="materialize-textarea">{{$faq->response}}</textarea>
                                                        @if ($errors->has('response'))
                                                            <div id="uname-error" class="error">{{ $errors->first('response') }}</div><br>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.update')}}
                                                        <i class="mdi-content-save right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection