@extends('layouts.app')

@section('title')
    Faqs
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-faq.css')}}">
@endsection

@section('content')
    <div class="section" id="faq-detail">
        <div class="row">
            <div class="col s12">
                <div id="faq-search" class="card z-depth-0 faq-search-image center-align p-35">
                    <div class="card-content">
                        <h5>Frequently Asked Questions</h5>
                        <a class="waves-effect waves-light btn-large " href="{{route('faq.create')}}"><i class="mdi-content-add-circle right"></i>New Faq</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @if($faqs->count() > 0)
                <div class="col s12">
                    <h6 class="text-uppercase font-weight-900 mb-1"></h6>
                    <ul class="collapsible categories-collapsible">
                        @foreach($faqs as $faq)
                            <li>
                                <div class="collapsible-header">Q: {{$faq->question}} <i class="material-icons"> keyboard_arrow_right </i></div>
                                <div class="collapsible-body" style="display: none;">
                                    <p>{!! $faq->response !!}<br><br>
                                        <a class="waves-effect waves-light btn blue" href="{{route('faq.edit',    ['id' => $faq->id ])}}"><i class="mdi-editor-border-color right"></i>{{trans('app.edit')}}</a>
                                        <a class="waves-effect waves-light btn red " href="{{route('faq.destroy', ['id' => $faq->id ])}}"><i class="mdi-action-delete right"></i>{{trans('app.delete')}}</a>
                                    </p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @else
                <div id="error-page">
                    <div class="row">
                        <div class="col s12">
                            <div class="browser-window">
                                <div class="content">
                                    <div class="row">
                                        <div id="site-layout-example-top" class="col s12">
                                            <p class="flat-text-logo center white-text caption-uppercase"></p>
                                        </div>
                                        <div id="site-layout-example-right" class="col s12 m12 l12">
                                            <div class="row center">
                                                <br><br><br><br><br>
                                                <h1 class="white-text  col s12">No Faq</h1>
                                                <a class="waves-effect waves-light btn-large " href="{{route('faq.create')}}">
                                                    <i class="mdi-content-add-circle right"></i>New Faq</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection