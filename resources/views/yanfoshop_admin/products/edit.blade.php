@extends('layouts.app')

@section('title')
    Modifier Produit
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h2 class="card-title center"> Modifier: {{$product->name}}</h2>
                                    <div class="row">
                                        <form action="{{ route('products.update',['id' => $product->id ] )}}" method="post" class="col s12 right-alert"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}

                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="name" name="name" type="text" class="validate" value="{{$product->name}}">
                                                    <label for="name">Nom</label>
                                                    @if ($errors->has('name'))
                                                        <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <div class="select-wrapper initialized">{{trans('app.category')}}
                                                        <select name="category_id" class="initialized">
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->id }}"
                                                                        @if($product->category_id == $category->id) selected
                                                                        @endif
                                                                > {{ $category->name }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('category_id'))
                                                        <div id="uname-error" class="error">{{ $errors->first('category_id') }}</div><br>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="input-field col s6">
                                                <i class="mdi-action-account-circle prefix"></i>
                                                <input id="price" name="price" type="number" class="validate" value="{{$product->price}}">
                                                <label for="price">Prix en CFA</label>
                                                @if ($errors->has('price'))
                                                    <div id="uname-error" class="error">{{ $errors->first('price') }}</div>
                                                @endif
                                            </div>
                                            <div class="input-field col s6">
                                                <i class="mdi-action-account-circle prefix"></i>
                                                <input id="minQty" name="minQty" type="number" class="validate" value="{{$product->minQty}}">
                                                <label for="minQty">Quantité minimum</label>
                                                @if ($errors->has('minQty'))
                                                    <div id="uname-error" class="error">{{ $errors->first('minQty') }}</div>
                                                @endif
                                            </div>
                                            <div class="input-field col s6">
                                                <i class="mdi-action-account-circle prefix"></i>
                                                <h8>Disponibilité</h8>
                                                <select name="stock_location" class="initialized">
                                                    <option value="{{$product->stock_location}}" disabled="" selected="">@if($product->stock_location == "bf")Burkina Faso
                                                       @elseif($product->stock_location == "ci")Cote D'Ivoire
                                                       @else Cameroun
                                                       @endif
                                                    </option>
                                                    @if(old('stock_location'))
                                                        <option value="{{ old('stock_location') }}" selected disabled>{{ old('stock_location') }}</option>
                                                    @endif
                                                    <option value="bf"> Burkina Faso </option>
                                                    <option value="ci"> Cote D'Ivoire </option>
                                                    <option value="cm"> Cameroun </option>
                                                </select>
                                            </div>
                                            @if($product->livraison == "Gratuite")
                                                <div class="col s4">
                                                    <p>Livraison</p>
                                                    <p>
                                                        <label>
                                                            <input class="validate" required="" aria-required="true" onclick="javascript:productLivraison();" name="livraison" id="gratuite" type="radio" checked="">
                                                            <span>Gratuite</span>
                                                        </label>
                                                        <label>
                                                            <input class="validate" required="" aria-required="true" onclick="javascript:productLivraison();" name="livraison" id="payante" type="radio">
                                                            <span>Payante</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="input-field col s4" style="visibility:hidden" id="prixLivraison">
                                                    <input id="prixLivraison2" name="prixLivraison" type="number" class="validate" value="{{ old('prixLivraison') }}">
                                                    <label for="prixLivraison2">Prix De La livraison</label>
                                                    @if ($errors->has('prixLivraison'))
                                                        <div id="uname-error" class="error">{{ $errors->first('prixLivraison') }}</div>
                                                    @endif
                                                </div>
                                                @else
                                                <div class="col s4">
                                                    <p>Livraison</p>
                                                    <p>
                                                        <label>
                                                            <input class="validate" required="" aria-required="true" onclick="javascript:productLivraison();" name="livraison" id="gratuite" type="radio" >
                                                            <span>Gratuite</span>
                                                        </label>
                                                        <label>
                                                            <input class="validate" required="" aria-required="true" onclick="javascript:productLivraison();" name="livraison" id="payante" type="radio" checked="">
                                                            <span>Payante</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="input-field col s4" style="visibility:visible" id="prixLivraison">
                                                    <input id="prixLivraison2" name="prixLivraison" type="number" class="validate" value="{{ $product->prixLivraison}}">
                                                    <label for="prixLivraison2">Prix De La livraison</label>
                                                    @if ($errors->has('prixLivraison'))
                                                        <div id="uname-error" class="error">{{ $errors->first('prixLivraison') }}</div>
                                                    @endif
                                                </div>
                                            @endif
                                            <div class="col s12 m8 l12">
                                                @if ($errors->has('image_url'))
                                                    <br>
                                                    <div id="uname-error" class="error">{{ $errors->first('image_url') }}</div>
                                                @endif
                                                <label for="image" class="">Image</label><br>
                                                <input name="image" type="file" id="input-file-events" class="dropify-event"
                                                       data-default-file="{{asset($product->image_url)}}" value="{{$product->image_url}}"/>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    Description
                                                    <textarea id="description" name="description"
                                                              class="materialize-textarea">{{ $product->description }}</textarea>
                                                    @if ($errors->has('description'))
                                                        <div id="uname-error" class="error">{{ $errors->first('description') }}</div><br>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    Plus D'informations
                                                    <textarea id="more_info" name="more_info"
                                                              class="materialize-textarea">{{ $product->more_info }}</textarea>
                                                    @if ($errors->has('more_info'))
                                                        <div id="uname-error" class="error">{{ $errors->first('more_info') }}</div><br>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s8">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">
                                                        Sauvegarder
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/scripts/form-elements.js')}}" type="text/javascript"></script>
    <script>
        function productLivraison(){
            if (document.getElementById('payante').checked) {
                document.getElementById('prixLivraison').style.visibility = 'visible';
            }
            else {
                document.getElementById('prixLivraison').style.visibility = 'hidden';
                document.getElementById('prixLivraison2').value =null;
            }
        }
    </script>
@endsection