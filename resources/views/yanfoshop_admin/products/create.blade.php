@extends('layouts.app')

@section('title')

    Créer Un Nouveau Produit
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dropzone.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/basic.css') }}">

@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center">  Nouveau Produit</h4>
                                    @if($categories->count())
                                        <div class="row">
                                        <form action="{{ route('products.store')}}" method="post" class="col s12 right-alert" id="pdCreateFormId"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input id="name" name="name" type="text" class="validate" value="{{ old('name') }}">
                                                    <label for="name">Nom</label>
                                                    @if ($errors->has('name'))
                                                        <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <div class="select-wrapper initialized">
                                                        <select name="category_id" class="initialized">
                                                            <option value="" disabled="" selected="">{{trans('app.category')}}</option>
                                                            @if(old('category_id'))
                                                                <option value="{{ old('category_id') }}" selected disabled>{{ old('category_id') }}</option>
                                                            @endif
                                                            @foreach($categories as $category)

                                                                <option value="{{ $category->id }}"> {{ $category->name }} </option>

                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('category_id'))
                                                        <div id="uname-error" class="error">{{ $errors->first('category_id') }}</div><br>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input id="price" name="price" type="number" class="validate" value="{{ old('price') }}">
                                                    <label for="price">Prix en CFA</label>
                                                    @if ($errors->has('price'))
                                                        <div id="uname-error" class="error">{{ $errors->first('price') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input id="minQty" name="minQty" type="number" class="validate" value="{{ old('minQty') }}">
                                                    <label for="minQty">Quantité minimum</label>
                                                    @if ($errors->has('minQty'))
                                                        <div id="uname-error" class="error">{{ $errors->first('minQty') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s4">
                                                    <div class="select-wrapper initialized">
                                                        <select name="stock_location" class="initialized">
                                                            <option value="" disabled="" selected="">Disponibilité ?</option>
                                                            @if(old('stock_location'))
                                                                <option value="{{ old('stock_location') }}" selected disabled>{{ old('stock_location') }}</option>
                                                            @endif
                                                            <option value="bf"> Burkina Faso </option>
                                                            <option value="ci"> Cote D'Ivoire </option>
                                                            <option value="cm"> Cameroun </option>
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('stock_location'))
                                                        <div id="uname-error" class="error">{{ $errors->first('stock_location') }}</div><br>
                                                    @endif
                                                </div>
                                                <div class="col s4">
                                                    <p>Livraison</p>
                                                    <p>
                                                        <label>
                                                            <input class="validate" required="" aria-required="true" onclick="javascript:productLivraison();" name="livraison" id="gratuite" type="radio" checked="">
                                                            <span>Gratuite</span>
                                                        </label>
                                                        <label>
                                                            <input class="validate" required="" aria-required="true" onclick="javascript:productLivraison();" name="livraison" id="payante" type="radio">
                                                            <span>Payante</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="input-field col s4" style="visibility:hidden" id="prixLivraison">
                                                    <input id="prixLivraison2" name="prixLivraison" type="number" class="validate" value="{{ old('prixLivraison') }}">
                                                    <label for="prixLivraison2">Prix De La livraison</label>
                                                    @if ($errors->has('prixLivraison'))
                                                        <div id="uname-error" class="error">{{ $errors->first('prixLivraison') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col s12 m8 l12">
                                                @if ($errors->has('image'))
                                                    <br>
                                                    <div id="uname-error" class="error">{{ $errors->first('image') }}</div>

                                                @endif
                                                <label for="image" class="">Image Principale</label><br>
                                                <input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>
                                            </div>

                                            {{--<div class="col s12 m8 l12">--}}
                                                {{--@if ($errors->has('productImages'))--}}
                                                    {{--<br>--}}
                                                    {{--<div id="uname-error" class="error">{{ $errors->first('productImages') }}</div>--}}
                                                {{--@endif--}}
                                                {{--<label for="productImages" class="">Images Secondaires</label><br>--}}
                                                {{--<input name="file" type="file" id="my-awesome-dropzone" class="dropzone" value="{{ old('productImages') }}" multiple />--}}
                                            {{--</div>--}}
                                            <br>
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    Description
                                                    <textarea id="description" name="description"
                                                              class="materialize-textarea">{{ old('description') }}</textarea>
                                                    @if ($errors->has('description'))
                                                        <div id="uname-error" class="error">{{ $errors->first('description') }}</div><br>
                                                    @endif
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    Plus d'informations
                                                    <textarea id="more_info" name="more_info"
                                                              class="materialize-textarea">{{ old('more_info') }}</textarea>
                                                    @if ($errors->has('more_info'))
                                                        <div id="uname-error" class="error">{{ $errors->first('more_info') }}</div><br>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit"
                                                            name="submit">{{trans('app.create')}}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    @else
                                        <div id="error-page">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="browser-window">
                                                        <div class="content">
                                                            <div class="row">
                                                                <div id="site-layout-example-top" class="col s12">
                                                                    <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                </div>
                                                                <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                    <div class="row center">
                                                                        <br><br><br><br><br>
                                                                        <h1 class="white-text  col s12">You must have Category  Before attempting to create a new product</h1>
                                                                        <a class="waves-effect waves-light btn-large " href="{{route('category.create')}}">
                                                                            <i class="mdi-content-add-circle right"></i>{{trans('app.newCategory')}}</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/dropzone.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/scripts/form-elements.js')}}" type="text/javascript"></script>
    <script>
        function productLivraison(){
            if (document.getElementById('payante').checked) {
                document.getElementById('prixLivraison').style.visibility = 'visible';
            }
            else document.getElementById('prixLivraison').style.visibility = 'hidden';
        }
        Dropzone.options.myDropzone = {
            paramName: 'file',
            maxFilesize: 5, // MB
            maxFiles: 20,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            init: function() {
                this.on("success", function(file, response) {
                    var a = document.createElement('span');
                    a.className = "thumb-url btn btn-primary";
                    a.setAttribute('data-clipboard-text','{{url('/uploads')}}'+'/'+response);
                    a.innerHTML = "copy url";
                    file.previewTemplate.appendChild(a);
                });
            }
        };
    </script>
@endsection
