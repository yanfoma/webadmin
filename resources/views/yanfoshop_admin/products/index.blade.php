@extends('layouts.app')

@section('title')
    Tous les Produits dans le YanfomaShop
@endsection

@section('style')
    <link href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css"        type="text/css" rel="stylesheet" media="screen,projection">
    <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"        type="text/css" rel="stylesheet" media="screen,projection">
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default">
                                <div class="card-content">
                                    <h4 class="card-title center"> Tous les Produits dans Votre Shop</h4>
                                    @if($categories->count() > 0)
                                        <a class="waves-effect waves-light btn-large " href="{{route('products.create')}}">Ajouter Un produit</a>
                                        <div class="row">
                                            @if($products->count() > 0)
                                                <div class="col s12 m12 l12">
                                                    <br>
                                                    <table id="example" class="striped hoverable cell-border">
                                                        <thead>
                                                        <tr>
                                                            <th>Image</th>
                                                            <th>Nom</th>
                                                            <th>Prix</th>
                                                            <th>MOQ</th>
                                                            <th>Description</th>
                                                            <th>Modifier</th>
                                                            <th>Supprimer</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($products as $product)
                                                            <tr>
                                                                <td><img src="{{asset($product->image_url)}}" width="100px" height="100px"
                                                                         alt="{{$product->name}}"></td>
                                                                <td>{{$product->name}}</td>
                                                                <td>{{$product->price}}</td>
                                                                <td>{{$product->minQty}}</td>
                                                                <td width="50">{{strip_tags($product->description )}}</td>
                                                                <td><a class="btn-floating activator btn waves-effect waves-light darken-2 orange"
                                                                       href="{{route('products.edit',   ['id' => $product->id ])}}">
                                                                        <i class="material-icons">edit</i>
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a class="btn-floating activator btn waves-effect waves-light darken-2 red right " href="{{route('products.delete', ['id' => $product->id ])}}">
                                                                        <i class="material-icons">delete</i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <div class="col s12 m8 l3 center-align pagination">
                                                        {{$products->render()}}
                                                    </div>
                                                    @else
                                                        <div id="error-page">
                                                            <div class="row">
                                                                <div class="col s12">
                                                                    <div class="browser-window">
                                                                        <div class="content">
                                                                            <div class="row">
                                                                                <div id="site-layout-example-top" class="col s12">
                                                                                    <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                                </div>
                                                                                <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                                    <div class="row center">
                                                                                        <h4 class="black-text  col s12">Aucun Produit pour l'instant</h4>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                                @endif
                                        </div>
                                    @else
                                        <div id="site-layout-example-right" class="col s12 m12 l12">
                                            <div class="row center">
                                                <h3 class="black-text  col s12">Aucune Catégorie!!! Veuillez en créer une avant de pouvoir ajouter vos produits</h3>
                                                <a class="waves-effect waves-light btn-large  mt-5 mb-10" href="{{route('category.create')}}">{{trans('app.newCategory')}}</a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                paging: false,
                searching: true,
                ordering:  true,
                select: true
            });
        } );
    </script>
@endsection