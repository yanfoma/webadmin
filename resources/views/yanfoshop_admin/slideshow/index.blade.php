@extends('layouts.app')

@section('title')
    {{trans('app.SlideshowTitle')}}
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center">{{trans('app.slideshow')}}</h4>
                                    <div class="card-alert card gradient-45deg-amber-amber">
                                        <div class="card-content white-text">
                                            <p>
                                                <i class="material-icons">warning</i> {{trans('app.SlideShowNotice')}}</p>
                                        </div>
                                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="row">
                                        <form action="{{ route('slideshow.update') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
                                            {!! csrf_field() !!}
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="title" name="title" type="text" class="validate" value="{{ old('title') }}">
                                                    <label for="title">{{trans('app.title')}}</label>
                                                    @if ($errors->has('title'))
                                                        <div id="uname-error" class="error">{{ $errors->first('title') }}</div>
                                                    @endif
                                                </div>
                                                <div class="col s12 m8 l12">
                                                    @if ($errors->has('image'))
                                                        <br><div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                                                    @endif
                                                    <label for="image" class="">{{trans('app.image')}}</label><br>
                                                    <input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <br/>
                                                <button type="submit" class="btn btn-success">{{trans('app.add')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                    <h5>Drag and Drop the table rows to reoder</h5>
                                    <table id="table">
                                        <thead>
                                        <tr>
                                            <th>Order</th>
                                            <th>{{trans('app.image')}}</th>
                                            <th>{{trans('app.delete')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tablecontents">
                                        @foreach($slides as $slide)
                                            <tr class="row1" data-id="{{ $slide->id }}">
                                                <td>{{ $slide->order }}</td>
                                                <td> <img src="{{asset($slide->image)}}" width="750px" height="150px" alt="{{$slide->title}}"></td>
                                                <td>
                                                    <form action="{{ route('slideshow.delete',$slide->id) }}" method="POST">
                                                        <input type="hidden" name="_method" value="delete">
                                                        {!! csrf_field() !!}
                                                        <button type="submit" class="waves-effect waves-light btn-small " ><i class="material-icons">delete</i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Datatables Js-->
    <script type="text/javascript" src="//cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
    <script type="text/javascript">
        $(function () {
            $("#table").DataTable();

            $( "#tablecontents" ).sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {

                var order = [];
                $('tr.row1').each(function(index,element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        position: index+1
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('en/webadmin-panel/sortSlideShow') }}",
                    data: {
                        order:order,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            console.log(response);
                        } else {
                            console.log(response);
                        }
                    }
                });

            }
        });

    </script>
@endsection