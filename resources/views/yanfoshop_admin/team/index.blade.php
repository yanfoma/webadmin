@extends('layouts.app')

@section('title')
    {{trans('app.allTeamTitle')}}
@endsection

@section('style')
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div id="highlight-table" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <h4 class="card-title center">{{trans('app.allTeamTitle')}}</h4>
                                        <a class="waves-effect waves-light btn-large gradient-45deg-red-pink gradient-shadow " href="{{route('team.create')}}"> <i class="material-icons">add_circle</i>Add A User</a>
                                        <div class="row">
                                            @foreach($users as $user)
                                                <div class="col s12 m6 l4 card-width">
                                                    <div class="card card-border center-align gradient-45deg-indigo-purple">
                                                        <div class="card-content white-text">
                                                            <img class="responsive-img circle z-depth-4" width="100" src="{{env('AVATAR_LINK')}}" alt="avatar">
                                                            <h5 class="white-text mb-1">{{$user->name}}</h5>
                                                            <p class="m-0">{{$user->email}}</p>
                                                            <p class="mt-8">
                                                                @switch($user->admin)
                                                                @case(1)
                                                                    Admin
                                                                @break

                                                                @case(2)
                                                                    Order Fulfilment
                                                                @break

                                                                @case(3)
                                                                    Super Admin
                                                                    <a class="waves-effect waves-light btn gradient-45deg-deep-orange-orange border-round mt-7 z-depth-4" href="{{route('team.delete',['id' => $user->id])}}">Delete</a>
                                                                @break
                                                                @endswitch
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
