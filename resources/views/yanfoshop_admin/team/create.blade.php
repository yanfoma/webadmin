@extends('layouts.app')

@section('title')
    {{trans('app.createTeamTitle')}}
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center">{{trans('app.team')}}</h4>
                                    <div class="card-alert card gradient-45deg-amber-amber">
                                        <div class="card-content white-text">
                                            <p>
                                                <i class="material-icons">warning</i> The Default Password is WebAdmin@2019</p>
                                        </div>
                                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="row">
                                        <form action="{{ route('team.store')}}"  method="post" class="col s12 right-alert" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="name" name="name" type="text" class="validate" value="{{old('name')}}" required>
                                                    <label for="name">{{ trans('app.nom') }}</label>
                                                    @if ($errors->has('title'))
                                                        <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <div class="select-wrapper initialized">
                                                        <select name="admin" class="initialized">
                                                            <option value="" disabled="" selected="">Position</option>
                                                            @if(old('admin'))
                                                                <option value="{{ old('admin') }}" selected disabled>{{ trans(old('admin')) }}</option>
                                                            @endif
                                                            <option value="1">Admin</option>
                                                            <option value="2">Order Fulfilment</option>
                                                            <option value="3">SuperAdmin</option>
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('admin'))
                                                        <div id="uname-error" class="error">{{ $errors->first('admin') }}</div><br>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <i class="mdi-communication-email prefix"></i>
                                                    <input id="email" name="email" type="email" class="validate" value="{{old('email')}}" required>
                                                    <label for="name">Email</label>
                                                    @if ($errors->has('email'))
                                                        <div id="uname-error" class="error">{{ $errors->first('email') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{ trans('app.create') }}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection