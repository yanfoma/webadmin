@extends('layouts.app')

@section('title')
    Profile
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center">Profile</h4>
                                    <div class="row">
                                        <div class="col s10 ml-10">
                                            <div id="profile-card" class="card animate fadeRight">
                                                <div class="card-image waves-effect waves-block waves-light">
                                                    <img class="activator" src="https://res.cloudinary.com/yanfomaweb/image/upload/v1550569893/Yanfoma/3.png" alt="user bg">
                                                </div>
                                                <div class="card-content">
                                                    <img src="{{env('AVATAR_LINK')}}" alt="" class="circle responsive-img activator card-profile-image cyan lighten-1 padding-1">
                                                    {{--<a class="btn-floating activator btn-move-up waves-effect waves-light red accent-2 z-depth-4 right">--}}
                                                        {{--<i class="material-icons">edit</i>--}}
                                                    {{--</a>--}}
                                                    <h5 class="card-title activator grey-text text-darken-4"></h5>
                                                    <p><i class="material-icons profile-card-i">perm_identity</i>Nom: {{Auth::user()->name}} </p>
                                                    <p><i class="material-icons profile-card-i">email</i>Email: {{Auth::user()->email}}</p>
                                                    <p><i class="material-icons profile-card-i">enhanced_encryption</i>Position: {{Auth::user()->position()}}</p>
                                                    <p><i class="material-icons profile-card-i">event_available</i>Date: {{Auth::user()->created_at}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<form action="{{ route('team.update')}}"  method="post" class="col s12 right-alert" enctype="multipart/form-data">--}}
                                            {{--{{ csrf_field() }}--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="input-field col s12">--}}
                                                    {{--<i class="material-icons prefix">account_circle</i>--}}
                                                    {{--<input id="name" name="name" type="text" class="validate" value="{{Auth::user()->name}}">--}}
                                                    {{--<label for="name">{{ trans('app.nom') }}</label>--}}
                                                    {{--@if ($errors->has('title'))--}}
                                                        {{--<div id="uname-error" class="error">{{ $errors->first('name') }}</div>--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                                {{--<div class="input-field col s12">--}}
                                                    {{--<i class="material-icons prefix">email</i>--}}
                                                    {{--<input id="email" name="email" type="email" class="validate" value="{{Auth::user()->email}}">--}}
                                                    {{--<label for="name">Email</label>--}}
                                                    {{--@if ($errors->has('email'))--}}
                                                        {{--<div id="uname-error" class="error">{{ $errors->first('email') }}</div>--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                                {{--<div class="input-field col s12">--}}
                                                    {{--<i class="mdi-action-account-circle prefix"></i>--}}
                                                    {{--<input id="position" name="position" type="text" class="validate" value="{{Auth::user()->position()}}" disabled>--}}
                                                    {{--<label for="position">Position</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="input-field col s6">--}}
                                                    {{--<button class="btn waves-effect waves-light right" type="submit" name="submit">{{ trans('app.update') }}--}}
                                                        {{--<i class="material-icons right">send</i>--}}
                                                    {{--</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</form>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection