@extends('layouts.app')

@section('title')
    {{trans('app.communities')}}
@endsection

@section('content')
    <section id="content">
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    {{trans('app.communities')}}
                </h4>
            </div>
        </div>
        <br> <br>
        <a  class="waves-effect waves-light btn modal-trigger  yellow red-text center-block text-darken-3" href="#modal3">
            <i class="mdi-alert-warning"></i>
            {{trans('app.important')}}
        </a>

        <div id="modal3" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 491.5489130434783px;">
            <div class="modal-content teal white-text large text-capitalize">
                <p style="text-align: center">
                    {{trans('app.SlideShowNotice')}}
                </p>
            </div>
            <div class="modal-footer  teal lighten-2">
                <a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">{{trans('app.ok')}}</a>
            </div>
        </div>
        <!--start container-->
        <div class="container">
            <form action="{{ route('adminCommunityUpdate') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="input-field col s6">
                        <i class="mdi-action-account-circle prefix"></i>
                        <input id="name" name="name" type="text" class="validate" value="{{ old('name') }}">
                        <label for="name">{{trans('app.nom')}}</label>
                        @if ($errors->has('name'))
                            <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <i class="mdi-action-list prefix"></i>
                        <input id="category" name="category" type="text" class="validate" value="{{ old('category') }}">
                        <label for="category">{{trans('app.category')}}</label>
                        @if ($errors->has('category'))
                            <div id="uname-error" class="error">{{ $errors->first('category') }}</div>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <i class="mdi-action-stars prefix"></i>
                        <input id="purpose" name="purpose" type="text" class="validate" value="{{ old('purpose') }}">
                        <label for="purpose">{{trans('app.purpose')}}</label>
                        @if ($errors->has('purpose'))
                            <div id="uname-error" class="error">{{ $errors->first('purpose') }}</div>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <i class="mdi-action-language prefix"></i>
                        <input id="status" name="status" type="text" class="validate" value="{{ old('status') }}">
                        <label for="status">{{trans('app.status')}}</label>
                        @if ($errors->has('status'))
                            <div id="uname-error" class="error">{{ $errors->first('status') }}</div>
                        @endif
                    </div>
                    <div class="input-field col s12">
                        <i class="mdi-editor-insert-link prefix"></i>
                        <input id="link" name="link" type="text" class="validate" value="{{ old('link') }}">
                        <label for="link">{{trans('app.link')}}</label>
                        @if ($errors->has('link'))
                            <div id="uname-error" class="error">{{ $errors->first('link') }}</div>
                        @endif
                    </div>
                    <div class="col s12 m8 l12">
                        @if ($errors->has('image'))
                            <br><div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                        @endif
                        <label for="image" class="">{{trans('app.image')}}</label><br>
                        <input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="description" name="description" class="materialize-textarea" value="{{ old('description') }}">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <div id="uname-error" class="error">{{ $errors->first('description') }}</div><br>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <br/>
                    <button type="submit" class="btn btn-success">{{trans('app.add')}}</button>
                </div>
            </form>
            <div class="section">
                <table class="striped hoverable">
                    <thead>
                    <tr>
                        <th data-field="id">{{trans('app.image')}}</th>
                        <th data-field="id">{{trans('app.nom')}}</th>
                        <th data-field="id">{{trans('app.category')}}</th>
                        <th data-field="id">{{trans('app.purpose')}}</th>
                        <th data-field="id">{{trans('app.status')}}</th>
                        <th data-field="id">{{trans('app.description')}}</th>
                        <th data-field="id">{{trans('app.delete')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($communities as $community)
                        <tr>
                            <td>
                                <figure class="card-profile-image">
                                    <img src="{{asset($community->image)}}" alt="{{$community->name}}" class="square responsive-img">
                                </figure>
                            </td>
                            <td>{{$community->name}}</td>
                            <td>{{$community->category}}</td>
                            <td>{{$community->purpose}}</td>
                            <td>{{$community->status}}</td>
                            <td>
                                {!! $community->description !!}
                                <a class="btn waves-effect waves-light indigo" href="{{$community->link}}" target="_blank">{{trans('app.view')}}</a>
                            </td>
                            <td>{{$community->order}}</td>
                            <td>
                                <form action="{{ route('adminCommunityDelete',$community->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="delete">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn-floating red"><i class="mdi-content-clear"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!--end container-->
    </section>
@endsection