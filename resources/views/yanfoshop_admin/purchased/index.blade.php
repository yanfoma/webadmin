@extends('layouts.app')

@section('title')
    Mes Commandes
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center"> Mes Commandes</h4>
                                    <div class="row">
                                        @if($purcahsed->count() > 0)
                                            <div class="col s12 m12 l12">
                                                <br>
                                                <table id="example" class="striped hoverable cell-border">
                                                    <thead>
                                                    <tr>
                                                        <th>{{ trans('app.nom') }}</th>
                                                        <th>Email</th>
                                                        <th>Addresse</th>
                                                        <th>Qty</th>
                                                        <th>Total</th>
                                                        <th>Date</th>
                                                        <th>Details</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($purcahsed as $item)
                                                        <tr>
                                                            <td>{{$item->name}}</td>
                                                            <td>{{$item->email}}</td>
                                                            <td>{{$item->addresse}}</td>
                                                            <td>{{$item->nbrProduits}}</td>
                                                            <td>{{$item->total}}</td>
                                                            <td>{{$item->created_at}}</td>
                                                            <td><a class="waves-effect waves-light  btn" href="{{route ('singleShopNotification',['id' => $item->id])}}">Voir</a></td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                {{--<div class="col s12 m8 l3 center-align pagination">--}}
                                                    {{--{{$users->links()}}--}}
                                                {{--</div>--}}
                                                @else
                                                    <div id="error-page">
                                                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="browser-window">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div id="site-layout-example-top" class="col s12">
                                                                                <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                            </div>
                                                                            <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                                <div class="row center">
                                                                                    <h4 class="black-text  col s12">Aucune Commande pour l'instant !</h4>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                paging: false,
                searching: true,
                ordering:  true,
                "order": [[ 3, "desc" ]]
            });
        } );
    </script>
@endsection