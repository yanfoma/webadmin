@extends('layouts.app')

@section('title')
    Comfirmation des Transactions Ecobank
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center"> Comfirmation des Transactions Ecobank </h4>
                                    <div class="row">
                                        @if($purcahsed->count() > 0)
                                            @foreach($purcahsed as $item)
                                                <div id="profile-page-wall-posts" class="row" >
                                                    <div class="col s12">
                                                        <!-- small -->
                                                        <div id="profile-page-wall-post" class="card" style="padding: 10px;">
                                                            <div class="card-profile-title">
                                                                <div class="row">
                                                                    <div class="col s1">
                                                                        <img src="{{env('AVATAR_LINK')}}" alt="" class="circle responsive-img valign profile-post-uer-image">
                                                                    </div>
                                                                    <div class="col s11">
                                                                        <p class="grey-text text-darken-4 margin">Code Commande : {{$item->purchasedId}} | Montant: {{$item->total}} CFA</p>
                                                                        @if($item->payed ==1 || $item->payed ==2)
                                                                            <p class="grey-text text-darken-4 margin">Payer par : {{$item->user->name}} | Email: {{$item->user->email}}</p>
                                                                            <span class="red-text text-darken-1 ultra-small">payed le   - {{$item->payedDate}}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @if($item->payed ==1 || $item->payed ==2)
                                                                <div class="card-image profile-large">
                                                                    <img src="{{$item->recu}}" alt="sample" class="responsive-img profile-post-image">
                                                                    <span class="card-title">Recu de Versement Ecobank</span>
                                                                </div>
                                                                <div class="card-content">
                                                                    <p>{!! $item->note !!}</p>
                                                                </div>
                                                            @endif
                                                            <div class="card-action row">
                                                                @switch($item->payed)
                                                                @case(0)
                                                                <div class="col s4">
                                                                    <a href="#" class=" btn red darken-2" disabled="" style="color: white!important;">Pas De Transaction</a>
                                                                </div>

                                                                <div class="col s8 card-action-share right-align">
                                                                    <a href="{{route('mails-center')}}" class="btn teal">
                                                                        <i class="mdi-communication-email"></i>
                                                                        Contacter
                                                                    </a>
                                                                </div>
                                                                @break

                                                                @case(1)
                                                                <div class="col s4 card-action-share">
                                                                    <a href="{{route('purchased.ecobankConfirmed',['id' => $item->id])}}" class="btn blue" disabled="">
                                                                        <i class="mdi-action-help"></i>
                                                                        Confirmer
                                                                    </a>
                                                                </div>

                                                                <div class="col s4 card-action-share right-align">
                                                                    <a href="{{$item->recu}}" title="Recu de Versement Ecobank Transaction {{$item->purchasedId}}" target="_blank" class="btn purple darken-1">
                                                                        <i class="mdi-file-cloud-download"></i>
                                                                        Telecharger Recu
                                                                    </a>
                                                                </div>

                                                                <div class="col s4 card-action-share right-align">
                                                                    <a href="{{route('mails-center')}}" class="btn teal">
                                                                        <i class="mdi-communication-email"></i>
                                                                        Contacter
                                                                    </a>
                                                                </div>
                                                                @break

                                                                @case(2)
                                                                <div class="col s4 card-action-share">
                                                                    <a href="#" class="btn teal lighten-1" disabled="" style="color: white!important;">Déja Confirmé
                                                                    </a>
                                                                </div>

                                                                <div class="col s4 card-action-share right-align">
                                                                    <a href="{{$item->recu}}" title="Recu de Versement Ecobank Transaction {{$item->purchasedId}}" target="_blank" class="btn purple darken-1">
                                                                        <i class="mdi-file-cloud-download"></i>
                                                                        Telecharger Recu
                                                                    </a>
                                                                </div>

                                                                <div class="col s4 card-action-share right-align">
                                                                    <a href="{{route('mails-center')}}" class="btn teal">
                                                                        <i class="mdi-communication-email"></i>
                                                                        Contacter
                                                                    </a>
                                                                </div>
                                                                @break
                                                                @endswitch
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <div class="col s12 m8 l3 center-align pagination">
                                                {{$purcahsed->render()}}
                                            </div>
                                        @else
                                            <div id="error-page">
                                                <div class="row">
                                                    <div class="col s12">
                                                        <div class="browser-window">
                                                            <div class="content">
                                                                <div class="row">
                                                                    <div id="site-layout-example-top" class="col s12">
                                                                        <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                    </div>
                                                                    <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                        <div class="row center">
                                                                            <h4 class="black-text  col s12">Aucune Transaction pour l'instant !</h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                paging: false,
                searching: true,
                ordering:  true
            });
        } );
    </script>
@endsection