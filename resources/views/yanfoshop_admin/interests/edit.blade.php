@extends('layouts.app')

@section('title')
    {{trans('app.editCategoryTitle')}}
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h2 class="card-title center">  Edit Interest : {{$interest->name}} </h2>
                                    <div class="row">
                                        <form action="{{ route('interest.update',['id' => $interest->id]) }}"  method="post" class="col s12 right-alert">
                                            {{ csrf_field() }}

                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="interest" name="interest" type="text" class="validate" value="{{$interest->name}}">
                                                    <label for="interest">{{trans('app.nom')}}</label>
                                                    @if ($errors->has('interest'))
                                                        <div id="uname-error" class="error">{{ $errors->first('interest') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.update')}}
                                                        <i class="mdi-content-save right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection