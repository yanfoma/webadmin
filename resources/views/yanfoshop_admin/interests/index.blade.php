@extends('layouts.app')

@section('title')
    All Interests
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    @if($interests->count() > 0)
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div id="highlight-table" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <h4 class="card-title center">Interets</h4>
                                        <a class="waves-effect waves-light btn-large gradient-45deg-red-pink gradient-shadow " href="{{route('interest.create')}}"><i class="material-icons">add_circle</i>New Interest</a>
                                        <div class="row">
                                            <div class="col s12">
                                            </div>
                                            <div class="col s12">
                                                <table class="highlight">
                                                    <thead>
                                                    <tr>
                                                        <th>{{trans('app.nom')}}</th>
                                                        <th>{{trans('app.edit')}}</th>
                                                        <th>{{trans('app.delete')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($interests as $interest)
                                                            <tr>
                                                                <td>{{$interest->name}}</td>
                                                                <td><a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan" href="{{route('interest.edit',    ['id' => $interest->id ])}}"><i class="mdi-editor-border-color right"></i>edit{{trans('app.edit')}}</a></td>
                                                                <td><a class="waves-effect waves-light btn red " href="{{route('interest.destroy', ['id' => $interest->id ])}}"><i class="mdi-action-delete right"></i>{{trans('app.delete')}}</a></td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                @else
                                                    <div id="error-page">
                                                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="browser-window">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div id="site-layout-example-top" class="col s12">
                                                                                <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                            </div>
                                                                            <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                                <div class="row center">
                                                                                    <br><br><br><br><br>
                                                                                    <h1 class="white-text  col s12">No Interests</h1>
                                                                                    <a class="waves-effect waves-light btn-large " href="{{route('interest.create')}}">
                                                                                        <i class="mdi-content-add-circle right"></i>New Interest</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection