@extends('layouts.app')

@section('title')
    New Interest
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center"> New Interest </h4>
                                    <div class="row">
                                        <form action="{{ route('interest.store') }}"  method="post" enctype="multipart/form-data" class="col s12 right-alert">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="interest" name="interest" type="text" class="validate" value="{{old('interest')}}">
                                                    <label for="interest">{{trans('app.nom')}}</label>
                                                    @if ($errors->has('interest'))
                                                        <div id="uname-error" class="error">{{ $errors->first('interest') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.create')}}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection