@extends('layouts.app')

@section('title')
   Suivi de Commandes
@stop

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/page-faq.css')}}">
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center"> Suivi de Commandes</h4>
                                    @if($purchased->count())
                                        <a class="waves-effect waves-light btn-large " href="{{route('tracking.create')}}"><i class="mdi-content-add-circle right"></i>Ajouter Un Suivi</a>
                                        <div class="faq row">
                                        <div class="col s12 m12 l12">
                                            <ul class="categories-collapsible">
                                                @foreach($purchased as $item)
                                                    <div class="" style="margin-bottom: 20px;">
                                                        <div class="card-content">
                                                            <h5 class="card-title">Commande : {{$item->purchasedId}}</h5>
                                                            <div class="row">
                                                                <ul class="collapsible categories-collapsible">
                                                                    @foreach($item->trackings as $tracking)
                                                                    <li>
                                                                        <div class="collapsible-header">{{$tracking->step}} <i class="material-icons"> keyboard_arrow_right </i></div>
                                                                        <div class="collapsible-body">
                                                                            <p>{{$tracking->note}}</p>
                                                                        </div>
                                                                    </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    @else
                                        <div id="site-layout-example-right" class="col s12 m12 l12 mt-5 mb-10">
                                            <div class="row center">
                                                <h4 class="black-text  col s12">Aucune Commande pour l'instant ! Donc aucun suivi ne peut être effectué</h4>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/custom/custom-script.js" type="text/javascript') }}"></script>
@endsection