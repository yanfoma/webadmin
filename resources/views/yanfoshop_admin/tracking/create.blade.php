@extends('layouts.app')

@section('title')
    Suivi de Commandes
@stop

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h2 class="card-title center">  Ajouter Un Suivi</h2>
                                    <div class="row">
                                        <form action="{{route('tracking.store')}}" method="POST" class="col s12 right-alert" id="mailForm"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="#">
                                                <label for="commandes" class="black-text text-darken-3" style="font-size: 15px; font-weight: bold">Commandes</label>
                                                <select class="#" multiple name="commandes[]" id="commandes">
                                                    <option value="" disabled="" selected="">Choisir les commandes</option>
                                                    @foreach($purchased as $item)
                                                        @if(old('commandes'))
                                                            <option value="{{ old('commandes') }}" selected disabled>{{ old('commandes') }}</option>
                                                        @endif
                                                        <option value="{{$item->purchasedId}}">{{$item->purchasedId}} : {{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="input-field col s6">
                                                <i class="mdi-action-account-circle prefix"></i>
                                                <input id="trackingCode" name="trackingCode" type="number" class="validate" value="{{ old('trackingCode') }}">
                                                <label for="trackingCode">Tracking Code</label>
                                                @if ($errors->has('trackingCode'))
                                                    <div id="uname-error" class="error">{{ $errors->first('trackingCode') }}</div>
                                                @endif
                                            </div>
                                            <div class="input-field col s6">
                                                <i class="mdi-action-account-circle prefix"></i>
                                                <input id="date" name="date" type="date" class="datepicker picker__input" value="{{ old('date') }}">
                                                <label for="date">Date</label>
                                                @if ($errors->has('date'))
                                                    <div id="uname-error" class="error">{{ $errors->first('date') }}</div>
                                                @endif
                                            </div>
                                            <div class="input-field col s6">
                                                <div class="select-wrapper initialized">
                                                    <select name="step" class="">
                                                        <option value="" disabled="" selected="">Etape</option>
                                                        @if(old('step'))
                                                            <option value="{{ old('step') }}" selected disabled>{{ old('step') }}</option>
                                                        @endif
                                                        <option value="Expédié">Expédié</option>
                                                        <option value="En transit">En transit</option>
                                                        <option value="Livré">Livré</option>
                                                        <option value="Erreur de Livraison">Erreur de Livraison</option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('step'))
                                                    <div id="uname-error" class="error">{{ $errors->first('step') }}</div><br>
                                                @endif
                                            </div>
                                            <div class="input-field col s6">
                                                <div class="select-wrapper initialized">
                                                    <select name="notificationMode" class="">
                                                        <option value="" disabled="" selected="">Mode De Notification</option>
                                                        @if(old('notificationMode'))
                                                            <option value="{{ old('notificationMode') }}" selected disabled>{{ old('notificationMode') }}</option>
                                                        @endif
                                                        <option value="mail">Mail</option>
                                                        <option value="sms" disabled>SMS (A venir)</option>
                                                        <option value="mailSms" disabled>Mail & SMS (A venir)</option>
                                                    </select>
                                                </div>
                                                @if ($errors->has('notificationMode'))
                                                    <div id="uname-error" class="error">{{ $errors->first('notificationMode') }}</div><br>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <i class="mdi-action-question-answer prefix"></i>
                                                <textarea id="note" name="note" data-value="{{ old('note') }}"></textarea>
                                                @if ($errors->has('note'))
                                                    <div id="uname-error" class="error">{{ $errors->first('note') }}</div><br>
                                                @endif
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit"
                                                            name="submit">{{trans('app.create')}}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection