@extends('layouts.app')

@section('title')
    {{trans('app.SlideshowTitle')}}
@endsection

@section('content')
    <section id="content">
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    {{trans('app.UpcomingEvents')}}
                </h4>
            </div>
        </div>
        <br> <br>
        <a  class="waves-effect waves-light btn modal-trigger  yellow red-text center-block text-darken-3" href="#modal3">
            <i class="mdi-alert-warning"></i>
            {{trans('app.important')}}
        </a>

        <div id="modal3" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 491.5489130434783px;">
            <div class="modal-content teal white-text large text-capitalize">
                <p style="text-align: center">
                    {{trans('app.SlideShowNotice')}}
                </p>
            </div>
            <div class="modal-footer  teal lighten-2">
                <a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">{{trans('app.ok')}}</a>
            </div>
        </div>
        <!--start container-->
        <div class="container">
            <form action="{{ route('upcominEventsUpdate') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="input-field col s12">
                        <i class="mdi-action-account-circle prefix"></i>
                        <input id="title" name="title" type="text" class="validate" value="{{ old('title') }}">
                        <label for="title">{{trans('app.title')}}</label>
                        @if ($errors->has('title'))
                            <div id="uname-error" class="error">{{ $errors->first('title') }}</div>
                        @endif
                    </div>
                    <div class="input-field col s12">
                        <div class="select-wrapper initialized">{{trans('app.language')}}:
                            <select name="language" class="initialized">
                                <option value="english">English </option>
                                <option value="french">French</option>
                            </select>
                        </div>
                        @if ($errors->has('language'))
                            <div id="uname-error" class="error">{{ $errors->first('language') }}</div><br>
                        @endif
                    </div>
                    <div class="input-field col s12">
                        <div class="select-wrapper initialized">{{trans('app.order')}}:
                            <select name="order" class="initialized">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                        @if ($errors->has('order'))
                            <div id="uname-error" class="error">{{ $errors->first('order') }}</div><br>
                        @endif
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="body" name="body" class="materialize-textarea" ></textarea>
                            @if ($errors->has('body'))
                                <div id="uname-error" class="error">{{ $errors->first('body') }}</div><br>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <br/>
                    <button type="submit" class="btn btn-success">{{trans('app.add')}}</button>
                </div>
            </form>
            <div class="section">
                <table class="striped hoverable">
                    <thead>
                    <tr>
                        <th data-field="id">{{trans('app.title')}}</th>
                        <th data-field="id">{{trans('app.language')}}</th>
                        <th data-field="id">{{trans('app.content')}}</th>
                        <th data-field="id">{{trans('app.order')}}</th>
                        <th data-field="id">{{trans('app.delete')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($upcominEvents as $upcominEvent)
                        <tr>
                            <td>{{$upcominEvent->title}}</td>
                            <td>{{$upcominEvent->language}}</td>
                            <td>{!! $upcominEvent->body !!}</td>
                            <td>{{$upcominEvent->order}}</td>
                            <td>
                                <form action="{{ route('upcominEventsDelete',$upcominEvent->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="delete">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="waves-effect waves-light btn-large " >{{trans('app.delete')}}</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>


        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    {{trans('app.whoAreWe')}}
                </h4>
            </div>
        </div>
        <br> <br>
        <div class="container">
            <form action="{{ route('introductionUpdate') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="input-field col s12">
                        <div class="select-wrapper initialized">{{trans('app.language')}}:
                            <select name="language" class="initialized">
                                <option value="english">English </option>
                                <option value="french">French</option>
                            </select>
                        </div>
                        @if ($errors->has('language'))
                            <div id="uname-error" class="error">{{ $errors->first('language') }}</div><br>
                        @endif
                    </div>
                    <div class="col s12 m8 l12">
                        @if ($errors->has('image'))
                            <br><div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                        @endif
                        <label for="image" class="">{{trans('app.image')}}</label><br>
                        <input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="body" name="body" class="materialize-textarea" ></textarea>
                            @if ($errors->has('body'))
                                <div id="uname-error" class="error">{{ $errors->first('body') }}</div><br>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <br/>
                    <button type="submit" class="btn btn-success">{{trans('app.add')}}</button>
                </div>
            </form>
            <div class="section">
                <table class="striped hoverable">
                    <thead>
                    <tr>
                        <th data-field="id">{{trans('app.image')}}</th>
                        <th data-field="id">{{trans('app.title')}}</th>
                        <th data-field="id">{{trans('app.language')}}</th>
                        <th data-field="id">{{trans('app.content')}}</th>
                        <th data-field="id">{{trans('app.delete')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($introductions as $introduction)
                        <tr>
                            <td> <img src="{{asset($introduction->image)}}" width="90px" height="50px" alt="{{$introduction->title}}"></td>
                            <td>{{$introduction->title}}</td>
                            <td>{{$introduction->language}}</td>
                            <td>{!! $introduction->body !!}</td>
                            <td>
                                <form action="{{ route('introductionDelete',$introduction->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="delete">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="waves-effect waves-light btn-large " >{{trans('app.delete')}}</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
                <h4 class="text center">
                    {{trans('app.Testimony')}}
                </h4>
            </div>
        </div>
        <br> <br>
        <div class="container">
            <form action="{{ route('testimonyUpdate') }}" class="form-image-upload" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="input-field col s6">
                        <i class="mdi-action-account-circle prefix"></i>
                        <input id="name" name="name" type="text" class="validate" value="{{ old('name') }}">
                        <label for="title">{{trans('app.name')}}</label>
                        @if ($errors->has('name'))
                            <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <i class="mdi-action-account-circle prefix"></i>
                        <input id="title" name="title" type="text" class="validate" value="{{ old('title') }}">
                        <label for="title">{{trans('app.title')}}</label>
                        @if ($errors->has('title'))
                            <div id="uname-error" class="error">{{ $errors->first('title') }}</div>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <div class="select-wrapper initialized">{{trans('app.language')}}:
                            <select name="language" class="initialized">
                                <option value="english">English </option>
                                <option value="french">French</option>
                            </select>
                        </div>
                        @if ($errors->has('language'))
                            <div id="uname-error" class="error">{{ $errors->first('language') }}</div><br>
                        @endif
                    </div>
                    <div class="col s12 m8 l12">
                        @if ($errors->has('image'))
                            <br><div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                        @endif
                        <label for="image" class="">{{trans('app.image')}}</label><br>
                        <input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="body" name="body" class="materialize-textarea" ></textarea>
                            @if ($errors->has('body'))
                                <div id="uname-error" class="error">{{ $errors->first('body') }}</div><br>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <br/>
                    <button type="submit" class="btn btn-success">{{trans('app.add')}}</button>
                </div>
            </form>
            <div class="section">
                <table class="striped hoverable">
                    <thead>
                    <tr>
                        <th data-field="id">{{trans('app.image')}}</th>
                        <th data-field="id">{{trans('app.name')}}</th>
                        <th data-field="id">{{trans('app.title')}}</th>
                        <th data-field="id">{{trans('app.language')}}</th>
                        <th data-field="id">{{trans('app.content')}}</th>
                        <th data-field="id">{{trans('app.delete')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($testymonies as $testymony)
                        <tr>
                            <td> <img src="{{asset($testymony->image)}}" width="90px" height="50px" alt="{{$testymony->title}}"></td>
                            <td>{{$testymony->name}}</td>
                            <td>{{$testymony->title}}</td>
                            <td>{{$testymony->language}}</td>
                            <td>{!!$testymony->body!!}</td>
                            <td>
                                <form action="{{ route('testimonyDelete',$testymony->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="delete">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="waves-effect waves-light btn-large " >{{trans('app.delete')}}</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!--end container-->
    </section>
@endsection