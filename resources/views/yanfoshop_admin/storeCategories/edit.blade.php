@extends('layouts.appWelcome')

@section('title')
   Edit Store Category
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h2 class="card-title center">  {{trans('app.editCategory')}} : {{$storeCategory->name}}</h2>
                                    <div class="row">
                                        <form action="{{ route('storeCategories.update',['id' => $storeCategory->id]) }}"  method="post" class="col s12 right-alert">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="category" name="category" type="text" class="validate" value="{{$storeCategory->name}}">
                                                    <label for="category">{{trans('app.nom')}}</label>
                                                    @if ($errors->has('category'))
                                                        <div id="uname-error" class="error">{{ $errors->first('category') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.update')}}
                                                        <i class="mdi-content-save right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection