@extends('layouts.app')

@section('title')
    SHOP NOTIFICATIONS
@endsection

@section('style')
    <style>
        .read{
            background: rgba(59, 251, 96, 0.09)!important;
            box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.3);
            color: green !important;
            /*font-size: 15px !important;*/
        }
        .read a{
            color: green !important;
            font-weight: 400!important;
        }

        .unread{
            background: rgba(251, 197, 239, 0.05)!important;
            box-shadow: 0 0 4px 0 rgba(149, 33, 105,0.2);
            color: #ff3180!important;
            /*font-size: 15px !important;*/
        }
        .unread a{
            color: #ff1a72;
            font-weight: 400!important;
        }
        .details{
            padding-top: 5px;
            font-style: italic;
        }
        .details a{
            text-decoration: underline;
        }
        .lol{

            font-size: 16px!important;
            font-style: italic;

        }
        .collection{
            padding:10px;
        }

        .collection-item{
            padding:10px!important;
            line-height: 30px!important;
        }
    </style>

        @endsection

        @section('content')
            <div class="container">
        <div class="section">
            <div class="col s12">
                @if($allNotifications->count())
                    <form action="delete/bulk-shop-notifications" method="post" enctype="multipart/form-data" >
                        {{csrf_field()}}
                        <ul id="task-card" class="collection with-header">
                            <li class="collection-header gradient-45deg-light-blue-cyan" style="background: #42a5f5;">
                                <h4 class="task-card-title-blue">NOTIFICATIONS ({{$allNotifications->count()}})</h4>
                            </li>
                            <div class="row col s12">
                                <div class="input-field col s1">
                                    <input type="checkbox" name="deleteAll" id="deleteAll" />
                                    <label for="deleteAll">{{trans('app.all')}}</label>
                                </div>
                                <div class="input-field col s3">
                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.delete')}}
                                        <i class="mdi-action-delete right"></i>
                                    </button>
                                </div>
                            </div>

                                @foreach($notifications as $notification)
                                     @if($notification->view == "Yes")
                                        <li class="collection-item read">
                                            <input class="checkBoxes" type="checkbox" name="bulkNotifications[]" value="{{$notification->id}}" id="{{$notification->id}}" multiple/>
                                            <label for="{{$notification->id}}"></label>
                                            <span class="task-cat lol red accent-3">Purchaser ID:{{$notification->id}}</span>
                                            <label class="lol" for="task5">
                                                <span class="text-black"><b><u>{{trans('app.nom')}}:</u></b></span> {{$notification->name}}
                                                <span class="text-black"><b><u>{{trans('app.country')}}:</u></b></span> {{$notification->pays}}
                                                <span class="text-black"><b><u>{{trans('app.city')}}:</u></b></span> {{$notification->ville}}
                                                <span class="text-black"><b><u>Email:</u></b></span> {{$notification->email}}
                                                <a href="#" class="content">
                                                    <span class="text-black"><b><u>{{trans('app.createdAt')}}:</u></b>{{Date::parse($notification->created_at)->diffForHumans()}}</span>
                                                </a>
                                                <a  href="{{route('singleShopNotification',['id'=>$notification->id])}}" class="content pull-right">
                                                    <span class="text-black">{{trans('app.view')}}</span>
                                                </a>
                                            </label>
                                        </li>
                                    @else
                                        <li class="collection-item unread" >
                                            <span  class="task-cat lol red accent-3">Purchaser ID:{{$notification->id}}</span>
                                            <label class="lol" for="task">
                                                <span class="text-black"><b><u>{{trans('app.nom')}}:</u></b></span> {{$notification->name}}
                                                <span class="text-black"><b><u>{{trans('app.country')}}:</u></b></span> {{$notification->pays}}
                                                <span class="text-black"><b><u>{{trans('app.city')}}:</u></b></span> {{$notification->ville}}
                                                <span class="text-black"><b><u>Email:</u></b></span> {{$notification->email}}
                                                <a href="#" class="content">
                                                    <span class="text-black"><b><u>{{trans('app.createdAt')}}:</u></b>{{Date::parse($notification->created_at)->diffForHumans()}}</span>
                                                </a>
                                                <a  href="{{route('singleShopNotification',['id'=>$notification->id])}}" class="content pull-right">
                                                    <span class="text-black">{{trans('app.view')}}</span>
                                                </a>
                                            </label>
                                        </li>
                                    @endif
                                @endforeach
                        </ul>
                        <div class="col s12 m8 l3 center-align">
                            {{$notifications->render()}}
                        </div>
                    </form>
                @else
                    <h2 class="center">No Notifications</h2>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {

            $('#deleteAll').click(function() {

                if(this.checked){
                    $('.checkBoxes').each(function () {

                        this.checked = true;

                    })
                }else{
                    $('.checkBoxes').each(function () {

                        this.checked = false;

                    })
                }
            });
            
        });
    </script>
@endsection
