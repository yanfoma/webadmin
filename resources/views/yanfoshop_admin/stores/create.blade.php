@extends('layouts.appWelcome')

@section('title')

    Créer Un Nouveau Store
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center">  Nouveau Store</h4>
                                    @if($storeCategories->count())
                                        <div class="row">
                                        <form action="{{ route('store.store')}}" method="post" class="col s12 right-alert"  enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input id="name" name="name" type="text" class="validate" value="{{ old('name') }}" required>
                                                    <label for="name">Nom</label>
                                                    @if ($errors->has('name'))
                                                        <div id="uname-error" class="error">{{ $errors->first('name') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <div class="select-wrapper initialized">
                                                        <select name="category_id" class="initialized" required>
                                                            <option value="" disabled="" selected="">{{trans('app.category')}}</option>
                                                            @if(old('category_id'))
                                                                <option value="{{ old('category_id') }}" selected disabled>{{ old('category_id') }}</option>
                                                            @endif
                                                            @foreach($storeCategories as $category)

                                                                <option value="{{ $category->id }}"> {{ $category->name }} </option>

                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    @if ($errors->has('category_id'))
                                                        <div id="uname-error" class="error">{{ $errors->first('category_id') }}</div><br>
                                                    @endif
                                                </div>

                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="email" name="email" type="email" class="validate" value="{{ old('email') }}" required>
                                                    <label for="email">Email</label>
                                                    @if ($errors->has('email'))
                                                        <div id="uname-error" class="error">{{ $errors->first('email') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">local_phone</i>
                                                    <input id="phone" name="phone" type="tel" class="validate" value="{{ old('phone') }}" required>
                                                    <label for="phone">Portable</label>
                                                    @if ($errors->has('phone'))
                                                        <div id="uname-error" class="error">{{ $errors->first('phone') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">insert_link</i>
                                                    <input id="facebook" name="facebook" type="text" class="validate" value="{{ old('facebook') }}">
                                                    <label for="facebook">Facebook</label>
                                                    @if ($errors->has('facebook'))
                                                        <div id="uname-error" class="error">{{ $errors->first('facebook') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">insert_link</i>
                                                    <input id="messenger" name="messenger" type="text" class="validate" value="{{ old('messenger') }}">
                                                    <label for="messenger">Messenger</label>
                                                    @if ($errors->has('messenger'))
                                                        <div id="uname-error" class="error">{{ $errors->first('messenger') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">insert_link</i>
                                                    <input id="whatsappp" name="whatsappp" type="text" class="validate" value="{{ old('whatsappp') }}">
                                                    <label for="whatsappp">Whatsapp</label>
                                                    @if ($errors->has('whatsappp'))
                                                        <div id="uname-error" class="error">{{ $errors->first('whatsappp') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s6">
                                                    <i class="material-icons prefix">insert_link</i>
                                                    <input id="linkedIn" name="linkedIn" type="text" class="validate" value="{{ old('linkedIn') }}">
                                                    <label for="linkedIn">LinkedIn</label>
                                                    @if ($errors->has('linkedIn'))
                                                        <div id="uname-error" class="error">{{ $errors->first('linkedIn') }}</div>
                                                    @endif
                                                </div>
                                                <div class="input-field col s12">
                                                    <i class="material-icons prefix">place</i>
                                                    <input id="addresse" name="addresse" type="text" class="validate" value="{{ old('addresse') }}">
                                                    <label for="addresse">Addresse</label>
                                                    @if ($errors->has('addresse'))
                                                        <div id="uname-error" class="error">{{ $errors->first('addresse') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col s12 m6 l6">
                                                @if ($errors->has('logo'))
                                                    <br>
                                                    <div id="uname-error" class="error">{{ $errors->first('logo') }}</div>
                                                @endif
                                                <label for="logo" class="">Logo</label><br>
                                                <input name="logo" type="file" id="input-file-events" class="dropify-event" value="{{ old('logo') }}" required/>
                                            </div>
                                            <div class="col s12 m6 l6">
                                                @if ($errors->has('image'))
                                                    <br>
                                                    <div id="uname-error" class="error">{{ $errors->first('image') }}</div>
                                                @endif
                                                <label for="image" class="">Image</label><br>
                                                <input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}" required/>
                                            </div>
                                            <br>
                                            <div class="row"><label for="image" class="">A Propos</label><br>
                                                <div class="input-field col s12">
                                                    <textarea name="about" class="materialize-textarea">{{ old('about') }}</textarea>
                                                    @if ($errors->has('about'))
                                                        <div id="uname-error" class="error">{{ $errors->first('about') }}</div><br>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit"
                                                            name="submit">{{trans('app.create')}}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    @else
                                        <div id="error-page">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="browser-window">
                                                        <div class="content">
                                                            <div class="row">
                                                                <div id="site-layout-example-top" class="col s12">
                                                                    <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                </div>
                                                                <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                    <div class="row center">
                                                                        <br><br><br><br><br>
                                                                        <h1 class="black-text  col s12">You must have Store Category  Before attempting to create a new store</h1>
                                                                        <a class="waves-effect waves-light btn-large " href="{{route('storeCategories.create')}}">
                                                                            <i class="mdi-content-add-circle right"></i>New Store Category</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/scripts/form-elements.js')}}" type="text/javascript"></script>
@endsection
