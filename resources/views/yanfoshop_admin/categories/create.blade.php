@extends('layouts.app')

@section('title')
    Sous Categories | Yanfoma
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div id="highlight-table" class="card card card-default scrollspy">
                                <div class="card-content">
                                    <h4 class="card-title center"> Ajouter une Sous-Catégorie</h4>
                                    <div class="row">
                                        <form action="{{ route('category.store') }}"  method="post" enctype="multipart/form-data" class="col s12 right-alert">
                                            {{ csrf_field() }}

                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <i class="mdi-action-account-circle prefix"></i>
                                                    <input id="category" name="category" type="text" class="validate" value="{{old('category')}}">
                                                    <label for="category">{{trans('app.nom')}}</label>
                                                    @if ($errors->has('category'))
                                                        <div id="uname-error" class="error">{{ $errors->first('category') }}</div>
                                                    @endif
                                                </div>
                                                {{--<div class="col s12 m8 l12">--}}
                                                {{--@if ($errors->has('image'))--}}
                                                {{--<br>--}}
                                                {{--<div id="uname-error" class="error">{{ $errors->first('image') }}</div>--}}
                                                {{--@endif--}}
                                                {{--<label for="image" class="">Image</label><br>--}}
                                                {{--<input name="image" type="file" id="input-file-events" class="dropify-event" value="{{ old('image') }}"/>--}}
                                                {{--</div>--}}
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s6">
                                                    <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.create')}}
                                                        <i class="mdi-content-send right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection