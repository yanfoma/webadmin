@extends('layouts.app')

@section('title')
    Sous Categories | Yanfoma
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="card card card-default" style="height: 500px!important;">
                            <div class="card-content">
                                <h4 class="card-title center"> Ajouter des Catégories a {{$subCategory->name}}</h4>
                                <div class="col s12">
                                    <div id="tags" class="card card-default">
                                        <div class="card-content">
                                            <div id="view-tags" class="">
                                                <div class="row">
                                                    <div class="col s12">
                                                        @foreach($subCategory->categories as $category)
                                                            <div class="chip">
                                                                {{$category->name}}
                                                                <a href="{{route('subcategories.delCategories', ['id' => $category->id,'parent_id'=> $subCategory->id ])}}"> <i class="close material-icons">close</i></a>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <form action="{{ route('subcategories.storeCategories') }}"  method="post" enctype="multipart/form-data" class="col s12 right-alert">
                                        {{ csrf_field() }}
                                        <div class="input-field col s12">
                                            <input type="hidden" name="parentSub_id" value="{{$subCategory->id}}">
                                            <input type="hidden" name="parent_id" value="{{$subCategory->parent->id}}">
                                            <div class="select-wrapper initialized">
                                                <select name="categories[]" class="initialized" multiple>
                                                    <option value="" disabled="">Veuillez selectionner les categories</option>
                                                    @foreach($categories as $cat)
                                                        <option value="{{$cat->name}}" @foreach($subCategory->categories as $cat2) @if($cat->name == $cat2->name) disabled @endif @endforeach>{{$cat->name}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if ($errors->has('categories'))
                                                <div id="uname-error" class="error">{{ $errors->first('categories') }}</div><br>
                                            @endif
                                        </div>
                                        <div class="input-field col s6">
                                            <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.add')}}
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection