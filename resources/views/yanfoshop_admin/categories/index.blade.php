@extends('layouts.app')

@section('title')
    Toutes Les Sous-Catégories
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <!-- Highlight Table -->
                    @if($categories->count() > 0)
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div id="highlight-table" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <h4 class="card-title center">Toutes Les Catégories</h4>
                                        <a class="waves-effect waves-light btn-large gradient-45deg-red-pink gradient-shadow " href="{{route('category.create')}}"> <i class="material-icons">add_circle</i>Nouvelle Sous-Categorie</a>
                                        <div class="row">
                                            <div class="col s12">
                                            </div>
                                            <div class="col s12">
                                                <table class="highlight">
                                                    <thead>
                                                        <tr>
                                                            <th>{{trans('app.nom')}}</th>
                                                            <th>{{trans('app.edit')}}</th>
                                                            <th>Nombre de Produits</th>
                                                            <th>{{trans('app.delete')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($categories as $category)
                                                        <tr>
                                                            <td>{{$category->name}}</td>
                                                            <td>{{$category->products->count()}} Produits</td>
                                                            <td><a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan" href="{{route('category.edit',   ['id' => $category->id ])}}"><i class="mdi-editor-border-color right"></i>{{trans('app.edit')}}</a></td>
                                                            <td><a class="waves-effect waves-light btn red " href="{{route('category.delete', ['id' => $category->id ])}}"><i class="mdi-action-delete right"></i>{{trans('app.delete')}}</a></td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                @else
                                                    <div id="error-page">
                                                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="browser-window">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div id="site-layout-example-top" class="col s12">
                                                                                <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                            </div>
                                                                            <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                                <div class="row center">
                                                                                    <h4 class="col s12">Vous n'avez aucune Catégories pour l'instant !!!</h4>
                                                                                    <a class="waves-effect waves-light btn-large  mt-5 mb-10" href="{{route('category.create')}}">
                                                                                        <i class="mdi-content-add-circle right"></i>{{trans('app.newCategory')}}
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection