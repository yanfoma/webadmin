@extends('layouts.app')

@section('title')
    Ajouter des Sous Categories | Yanfoma
@endsection

@section('content')
    <div class="row">
        <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <!-- Highlight Table -->
                    @if($subCategories->count() > 0)
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div id="highlight-table" class="card card card-default scrollspy">
                                    <div class="card-content">
                                        <h4 class="card-title center"> Toutes Les Sous-Catégories</h4>
                                        <a class="waves-effect waves-light btn-large gradient-45deg-red-pink gradient-shadow " href="{{route('subCategory.create',['id' => $parentCategory->id ])}}"> <i class="material-icons">add_circle</i>Nouvelle Sous-Categorie</a>
                                        <div class="row">
                                            <div class="col s12">
                                            </div>
                                            <div class="col s12">
                                                <table class="highlight">
                                                    <thead>
                                                    <tr>
                                                        <th>{{trans('app.nom')}}</th>
                                                        <th>Nombre de Categories</th>
                                                        <th>Ajouter Categories</th>
                                                        <th>Modifier</th>
                                                        <th>{{trans('app.delete')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($subCategories as $subcat)
                                                        <tr>
                                                            <td>{{$subcat->name}}</td>
                                                            <td><span class="new badge" data-badge-caption=" Categories">{{$subcat->categories->count()}}</span></td>
                                                            <td>
                                                                <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan" href="{{route('subcategories.addCategories',   ['subcat_id' => $subcat->id,'parent_id' => $parentCategory->id ])}}">
                                                                    <i class="material-icons delete right">add_box</i>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a class="waves-effect waves-light btn gradient-45deg-amber-amber" href="{{route('subCategory.edit',   ['id' => $subcat->id ,'parent_id' => $parentCategory->id])}}">
                                                                    <i class="material-icons delete right">mode_edit</i>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a class="waves-effect waves-light btn red " href="{{route('subCategory.delete', ['id' => $subcat->id ,'parent_id' => $parentCategory->id ])}}">
                                                                    <i class="material-icons right">delete_forever</i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                @else
                                                    <div id="error-page">
                                                        <div class="row">
                                                            <div class="col s12">
                                                                <div class="browser-window">
                                                                    <div class="content">
                                                                        <div class="row">
                                                                            <div id="site-layout-example-top" class="col s12">
                                                                                <p class="flat-text-logo center white-text caption-uppercase"></p>
                                                                            </div>
                                                                            <div id="site-layout-example-right" class="col s12 m12 l12">
                                                                                <div class="row center">
                                                                                    <h4 class="col s12">Vous n'avez aucune Catégories pour l'instant !!!</h4>
                                                                                    <a class="waves-effect waves-light btn-large  mt-5 mb-10" href="{{route('subCategory.create',['id' => $parentCategory->id ])}}">
                                                                                        <i class="mdi-content-add-circle right"></i>{{trans('app.newCategory')}}
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection