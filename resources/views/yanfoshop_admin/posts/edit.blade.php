@extends('layouts.app')

@section('title')
    {{trans('app.editPostsTitle')}}
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                {{trans('app.editPost')}}: {{$post->title}}
            </h4>
        </div>
    </div>
    <br> <br>
            <div class="row">
                <form action="{{ route('post.update',['id' => $post->post_id,'locale'=> $post->locale ] )}}"  method="post" class="col s12 right-alert" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="title" name="title" type="text" class="validate" value="{{$post->title}}">
                            <label for="title">{{trans('app.title')}}</label>
                            @if ($errors->has('title'))
                                <div id="uname-error" class="error">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col s12 m8 l12">
                        @if ($errors->has('featuredImage'))
                            <br><div id="uname-error" class="error">{{ $errors->first('featuredImage') }}</div>
                        @endif
                        <label for="featuredImage" class="">{{trans('app.image')}}</label><br>
                        <input name="featuredImage" type="file" id="input-file-events" class="dropify-event" data-default-file="{{asset($post->featuredImage)}}" value="{{$post->featuredImage}}"/>
                    </div>
                    <div class="input-field col s6">
                        <div class="select-wrapper initialized">{{trans('app.category')}}
                            <select name="category_id" class="initialized">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                            @if($post->category->id == $category->id)

                                                selected
                                            @endif

                                            > {{ $category->name }} </option>
                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('category_id'))
                            <div id="uname-error" class="error">{{ $errors->first('category_id') }}</div><br>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <div class="select-wrapper initialized">{{trans('app.type')}}
                            <select name="blog" class="initialized">
                                <option value="{{$post->blog}}"  @if($post->blog)
                                         selected
                                        @endif> {{ $post->blog }} </option>
                                <option value="blog"> {{trans('app.blog')}} </option>
                                <option value="post"> {{trans('app.post')}} </option>
                                <option value="ghanaHub"> {{trans('app.ghanaHub')}} </option>
                            </select>
                        </div>
                        @if ($errors->has('blog'))
                            <div class="error">{{ $errors->first('blog') }}</div><br>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <div class="select-wrapper initialized">{{trans('app.tags')}}:<br><br>
                            @foreach($postTag->tags as $tag)
                                <input name="tags[]" value="{{ $tag->id }}"
                                       @foreach($postTag->tags as $t)
                                            @if($tag->id == $t->id)
                                               checked
                                            @endif
                                       @endforeach
                                       type="checkbox" class="filled-in" id="{{ $tag->tag }}">
                                <label for="{{ $tag->tag }}">{{ $tag->tag }}</label>
                            @endforeach
                        </div>
                        @if ($errors->has('tag_id'))
                            <div id="uname-error" class="error">{{ $errors->first('tag_id') }}</div><br>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <div class="select-wrapper initialized">{{trans('app.language')}}:
                            <select name="language" class="initialized">
                                <option value="{{$post->language}}" elected="{{$post->language}}">{{$post->language}}</option>
                                <option value="english">English </option>
                                <option value="french">French</option>
                            </select>
                        </div>
                        @if ($errors->has('language'))
                            <div id="uname-error" class="error">{{ $errors->first('language') }}</div><br>
                        @endif
                    </div><br><br>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="body" name="body" class="materialize-textarea" >{{$post->content}}</textarea>
                            @if ($errors->has('body'))
                                <div id="uname-error" class="error">{{ $errors->first('body') }}</div><br>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s8">
                            <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.update')}}
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                </form>
@endsection