@extends('layouts.app')

@section('title')
    {{trans('app.TranslatePostsTitle')}}
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                {{trans('app.translate')}}: {{$post->title}}
            </h4>
        </div>
    </div>
    <br> <br>

            <div class="row">
                <form action="{{ route('post.storeTranslate',['id' => $post['id']] )}}"  method="post" class="col s12 right-alert" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="title" name="title" type="text" class="validate" value="{{old('title')}}" >
                            <label for="title">{{trans('app.title')}}</label>
                            @if ($errors->has('title'))
                                <div id="uname-error" class="error">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col s12 m8 l12">
                        @if ($errors->has('featuredImage'))
                            <br><div id="uname-error" class="error">{{ $errors->first('featuredImage') }}</div>
                        @endif
                        <label for="featuredImage" class="">{{trans('app.image')}}</label><br>
                        <input name="featuredImage" type="file" id="input-file-events" class="dropify-event" data-default-file="{{asset($post['featuredImage'])}}" disabled/>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="body" name="body" class="materialize-textarea" >{{old('body')}}</textarea>
                            @if ($errors->has('body'))
                                <div id="uname-error" class="error">{{ $errors->first('body') }}</div><br>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s8">
                            <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.translate')}}
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                </form>
@endsection