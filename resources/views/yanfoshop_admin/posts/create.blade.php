@extends('layouts.app')

@section('title')

    {{trans('app.newPostTitle')}}
@endsection

@section('content')
    <div class="row">
        <div class="col s12 m9 l12 invoice-brief grey lighten-2 red-text z-depth-1">
            <h4 class="text center">
                {{trans('app.newPost')}}
            </h4>
        </div>
    </div>
    <br> <br>
    <a  class="waves-effect waves-light btn modal-trigger  yellow red-text center-block text-darken-3" href="#modal3"><i class="mdi-alert-warning"></i>{{trans('app.important')}}</a>

    <div id="modal3" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 491.5489130434783px;">
        <div class="modal-content teal white-text large text-capitalize">
            <p style="text-align: center">
                {{trans('app.SlideShowNotice')}}
            </p>
        </div>
        <div class="modal-footer  teal lighten-2">
            <a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">{{trans('app.ok')}}</a>
        </div>
    </div>
    <div class="col s12 m12 l6">
        <div class="card-panel">
            <div class="row">
                <form action="{{ route('post.store')}}"  method="post" class="col s12 right-alert" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-account-circle prefix"></i>
                            <input id="title" name="title" type="text" class="validate" value="{{ old('title') }}">
                            <label for="title">{{trans('app.title')}}</label>
                            @if ($errors->has('title'))
                                <div id="uname-error" class="error">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col s12 m8 l12">
                        @if ($errors->has('featuredImage'))
                            <br><div id="uname-error" class="error">{{ $errors->first('featuredImage') }}</div>
                        @endif
                            <label for="featuredImage" class="">{{trans('app.image')}}</label><br>
                            <input name="featuredImage" type="file" id="input-file-events" class="dropify-event" value="{{ old('featuredImage') }}"/>
                    </div>
                    <div class="input-field col s6">
                        <div class="select-wrapper initialized">
                            <select name="category_id" class="initialized">
                                <option value="" disabled="" selected="">{{trans('app.category')}}</option>
                                @if(old('category_id'))
                                    <option value="{{ old('category_id') }}" selected disabled>{{ old('category_id') }}</option>
                                @endif
                                @foreach($categories as $category)

                                    <option value="{{ $category->id }}"> {{ $category->name }} </option>

                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('category_id'))
                            <div id="uname-error" class="error">{{ $errors->first('category_id') }}</div><br>
                        @endif
                    </div>
                    <div class="input-field col s6">
                        <div class="select-wrapper initialized">
                            <select name="language" class="initialized">
                                <option value="" disabled="" selected="">{{trans('app.language')}}</option>
                                @if(old('language'))
                                    <option value="{{ old('language') }}" selected disabled>{{ trans(old('language')) }}</option>
                                @endif
                                <option value="english">{{trans('app.english')}} </option>
                                <option value="french">{{trans('app.french')}}</option>
                            </select>
                        </div>
                        @if ($errors->has('language'))
                            <div id="uname-error" class="error">{{ $errors->first('language') }}</div><br>
                        @endif
                    </div>
                    <div class="input-field col s12">
                        <div class="select-wrapper initialized">
                            <label for="Tag" class="">{{trans('app.tags')}}</label><br><br>
                            @if(old('Tag'))
                                <input name="tags[]" value="{{ $tag->id }}" type="checked" class="filled-in" id="{{ $tag->tag }}">
                            @endif
                            @foreach($tags as $tag)
                                <input name="tags[]" value="{{ $tag->id }}" type="checkbox" class="filled-in" id="{{ $tag->tag }}">
                                <label for="{{ $tag->tag }}">{{ $tag->tag }}</label>
                            @endforeach
                        </div>
                        @if ($errors->has('tag_id'))
                            <div id="uname-error" class="error">{{ $errors->first('tag_id') }}</div><br>
                        @endif
                    </div>
                    <br>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="mdi-action-question-answer prefix"></i>
                            <textarea id="body" name="body" class="materialize-textarea">{{ old('body') }}</textarea>
                            @if ($errors->has('body'))
                                <div id="uname-error" class="error">{{ $errors->first('body') }}</div><br>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <button class="btn waves-effect waves-light right" type="submit" name="submit">{{trans('app.create')}}
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection