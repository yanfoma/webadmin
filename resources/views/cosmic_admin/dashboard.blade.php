@extends('layouts.cosmic-app')

@section('title')
    {{ trans('app.adminTitle') }}
@endsection

@section('content')
<div class="container">
    <div id="card-stats">
        <div class="row">
            <div class="col s12 m9 l12 invoice-brief grey lighten-2 green-text z-depth-1">
                <h4 class="text center">
                    {{ trans('app.welcome') }} {{Auth::user()->name}}
                </h4>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content  green white-text">
                        <h4 class="card-stats-number">To implement the cosmic admin, please specify the suitable database connection for each query</h4>
                    </div>
                    <div class="card-action  green darken-2"></div>
                </div>
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content  red white-text">
                        <p class="card-stats-title">Cosmic admin have to be implemented here please. Yanfoshop admin is used by default for demonstration</p>
                    </div>
                    <div class="card-action  red darken-2"></div>
                </div>
            </div>
        </div>

        <h4 class="card-stats-number" style="text-align: center">Banniere</h4>
        <div class="card medium">
            {{--<div class="card-image">
                <img src="{{asset($slides->image)}}" alt="slide">
            </div>--}}
        </div>
    </div>
</div>
@endsection
