<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="{{route('dashboard')}}"><span class="logo-text hide-on-med-and-down">WEBADMIN</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
    </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
            @if(Auth::check())
                    @if(Auth::user()->admin == 1 && Auth::user()->store_id == 1)
                        <li class="bold">
                            <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel','fr/webadmin-panel') ?'active':''}} " href="{{route('welcome')}}" >
                                <i class="material-icons">store</i><span class="menu-title" data-i18n="">Stores</span>
                            </a>
                        </li>
                    @endif
                    <li class=" {{\Request::is('en/webadmin-panel/yanfoshop','fr/webadmin-panel/yanfoshop') ?'active':''}} bold">
                        <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/yanfoshop','fr/webadmin-panel/yanfoshop') ?'active':''}}" href="{{route('dashboard')}}">
                            <i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="">{{ trans('app.dashboard') }}</span>
                        </a>
                    </li>
                    @if(Auth::user()->admin == 1 || Auth::user()->admin == 4)
                            <li class="bold {{\Request::is('en/webadmin-panel/all-categories','fr/webadmin-panel/toutes-les-catégories') ?'active':''}}">
                                <a class="collapsible-header waves-effect waves-cyan {{\Request::is('en/webadmin-panel/all-categories','fr/webadmin-panel/toutes-les-catégories','fr/webadmin-panel/grandes-categories/store-subcategories','en/webadmin-panel/grandes-categories/store-subcategories','fr/webadmin-panel/toutes-les-grandes-categories','en/webadmin-panel/toutes-les-grandes-categories') ?'active':''}}"  href="#" tabindex="0">
                                    <i class="material-icons">dvr</i><span class="menu-title" data-i18n="">{{ trans('app.categories') }}</span></a>
                                    <div class="collapsible-body">
                                        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                            <li><a class="waves-effect waves-cyan " href="{{route('parentCategory.index')}}" data-i18n="" tabindex="0"><span>Grandes {{ trans('app.categories') }} </span></a></li>
                                            <li><a class="waves-effect waves-cyan" href="{{route('category.index')}}" data-i18n="" tabindex="0"><span> {{ trans('app.categories') }}</span></a></li>
                                        </ul>
                                    </div>
                            </li>
                @endif
            @endif
            <li class="bold {{\Request::is('en/webadmin-panel/products','fr/webadmin-panel/products') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/products','fr/webadmin-panel/products') ?'active':''}} " href="{{route('products.index')}}" >
                    <i class="material-icons">store</i><span class="menu-title" data-i18n="">Produits</span>
                </a>
            </li>
            <li class="bold {{\Request::is('en/webadmin-panel/products/create','fr/webadmin-panel/products/create') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/products/create','fr/webadmin-panel/products/create') ?'active':''}} " href="{{route('products.create')}}" >
                    <i class="material-icons">add_box</i><span class="menu-title" data-i18n="">{{ trans('app.newProduct') }}</span>
                </a>
            </li>
            <li class="bold {{\Request::is('en/webadmin-panel/products/purchased-items','fr/webadmin-panel/products/purchased-items') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/products/purchased-items','fr/webadmin-panel/products/purchased-items') ?'active':''}} " href="{{route('purchased.index')}}" >
                    <i class="material-icons">shopping_basket</i><span class="menu-title" data-i18n="">Commandes</span>
                </a>
            </li>
            <li class="bold {{\Request::is('en/webadmin-panel/products/comfirm-ecobank-transaction','fr/webadmin-panel/products/comfirm-ecobank-transaction') ? ' active ' :''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/products/comfirm-ecobank-transaction','fr/webadmin-panel/products/comfirm-ecobank-transaction') ? ' active ' :''}} " href="{{route('purchased.ecobankConfirm')}}" >
                    <i class="material-icons">payments</i><span class="menu-title" data-i18n="">Payments Ecobank</span>
                </a>
            </li>
            <li class="bold {{\Request::is('en/webadmin-panel/products/purchased/tracking','fr/webadmin-panel/products/purchased/tracking') ? ' active ' :''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/products/purchased/tracking','fr/webadmin-panel/products/purchased/tracking') ? ' active ' :''}}" href="{{route('tracking.index')}}">
                    <i class="material-icons">local_shipping</i><span class="menu-title" data-i18n="">Suivi des Commandes</span>
                </a>
            </li>
            <li class="bold {{\Request::is('en/webadmin-panel/all-sales','fr/webadmin-panel/all-sales') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/all-sales','fr/webadmin-panel/all-sales') ?'active':''}} " href="{{route('sales.index')}}" >
                    <i class="material-icons">local_play</i><span class="menu-title" data-i18n="">Soldes</span>
                </a>
            </li>
            @if(Auth::check())
                @if(Auth::user()->admin == 1 && Auth::user()->store_id == 1)
                <li class="bold {{\Request::is('en/webadmin-panel/products/liked-items','fr/webadmin-panel/products/liked-items') ?'active':''}} ">
                    <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/products/liked-items','fr/webadmin-panel/products/liked-items') ?'active':''}} " href="{{route('liked.index')}}" >
                        <i class="material-icons">thumb_up</i><span class="menu-title" data-i18n="">Aimes</span>
                    </a>
                </li>
            @endif

            <li class="bold {{\Request::is('en/webadmin-panel/products/favorited-items','fr/webadmin-panel/products/favorited-items') ?'active':''}} ">
                <a  class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/products/favorited-items','fr/webadmin-panel/products/favorited-items') ?'active':''}} " href="{{route('favorited.index')}}">
                    <i class="material-icons">favorite_border</i><span class="menu-title" data-i18n="">Favoris</span>
                <span></span>
                </a>
            </li>

            <li class="bold {{\Request::is('en/webadmin-panel/interests','fr/webadmin-panel/interests') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/interests','fr/webadmin-panel/interests') ?'active':''}} " href="{{route('interest.index')}}">
                    <i class="material-icons">forum</i><span class="menu-title" data-i18n="">Interets</span>
                </a>
            </li>
            @endif
            {{--<li class="bold {{\Request::is('en/all-coupons','fr/all-coupons') ?'active':''}} ">--}}
                {{--<a class="waves-effect waves-cyan {{\Request::is('en/all-coupons','fr/all-coupons') ?'active':''}} " href="{{route('coupons.index')}}" >--}}
                    {{--<i class="material-icons">shopping_basket</i><span class="menu-title" data-i18n="">Coupons</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="bold {{\Request::is('en/webadmin-panel/all-users','fr/webadmin-panel/tous-les-utilisateurs') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/all-users','fr/webadmin-panel/tous-les-utilisateurs') ?'active':''}} " href="{{route('users.index')}}" >
                    <i class="material-icons">group</i><span class="menu-title" data-i18n="">Clients</span>
                </a>
            </li>
            <li class="bold {{\Request::is('en/webadmin-panel/Slideshow','fr/webadmin-panel/Slideshow') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/Slideshow','fr/webadmin-panel/Slideshow') ?'active':''}} " href="{{route('slideshow.index')}}" >
                    <i class="material-icons">image</i><span class="menu-title" data-i18n="">{{ trans('app.slideshow') }}</span>
                </a>
            </li>
            @if(Auth::check())
                @if(Auth::user()->admin == 1 && Auth::user()->store_id == 1)
                    <li class="bold {{\Request::is('en/webadmin-panel/faq','fr/webadmin-panel/faq') ?'active':''}} ">
                        <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/faq','fr/webadmin-panel/faq') ?'active':''}} " href="{{route('faq.index')}}">
                            <i class="material-icons">help</i><span class="menu-title" data-i18n="">FAQ</span>
                        </a>
                    </li>
                @endif
            @endif
            <li class="bold {{\Request::is('en/webadmin-panel/profile','fr/webadmin-panel/profile') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/profile','fr/webadmin-panel/profile') ?'active':''}} " href="{{route('profile')}}" >
                    <i class="material-icons">face</i><span class="menu-title" data-i18n="">Profile</span>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="material-icons">exit_to_app</i><span class="menu-title" data-i18n="">{{ trans('app.logout') }}</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                </form>
            </li>
        </ul>
    <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->