<style>
    .read{
        background: rgba(59, 251, 96, 0.09);
        box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.3);
        color: green !important;
        font-size: 12px !important;
    }
    .read a{
        color: green !important;
        font-weight: 400!important;
    }

    .unread{
        background: rgba(251, 197, 239, 0.05);
        box-shadow: 0 0 4px 0 rgba(149, 33, 105,0.2);
        color: #ff3180!important;
        font-size: 12px !important;
    }
    .unread a{
        color: #ff1a72;
        font-weight: 400!important;
    }
    .details{
        padding-top: 5px;
        font-style: italic;
    }
    .details a{
        text-decoration: underline;
    }

</style>

<!-- BEGIN: Header-->
<header class="page-topbar" id="header">
    <div class="navbar navbar-fixed">
        <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple gradient-shadow">
            <div class="nav-wrapper">
                <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
                    <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Chercher ici">
                </div>
                <ul class="navbar-list right">
                    <li><a class="waves-effect waves-block waves-light sidenav-trigger" href="https://www.yanfoma.com" target="_blank" data-target="slide-out-right"><i class="material-icons">explore</i></a></li>
                    <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light translation-button" href="javascript:void(0);" data-target="translation-dropdown">{{ trans('app.'.app()->getLocale().'')}}</a></li>
                    <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
                    <li class="hide-on-large-only"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search</i></a></li>
                    {{--<li><a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown"><i class="material-icons">notifications_none<small class="notification-badge">5</small></i></a></li>--}}
                    <li>
                        <a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online">
                                <img src="{{env('AVATAR_LINK')}}" alt="avatar"><i></i></span>
                        </a>
                    </li>
                </ul>
                <!-- translation-button-->
                <ul class="dropdown-content" id="translation-dropdown">
                    @foreach(config('translatable.locales') as $lang => $language)
                        @if ($lang != app()->getLocale())
                            <li><a class="grey-text text-darken-1" href="{{route('lang.switch', $lang)}}">{{ $language }}</a></li>
                        @endif
                    @endforeach
                </ul>
                {{--<!-- notifications-dropdown-->--}}
                {{--<ul class="dropdown-content" id="notifications-dropdown">--}}
                    {{--<li>--}}
                        {{--<h6>NOTIFICATIONS<span class="new badge">5</span></h6>--}}
                    {{--</li>--}}
                    {{--<li class="divider"></li>--}}
                {{--</ul>--}}
                <!-- profile-dropdown-->
                <ul class="dropdown-content" id="profile-dropdown">
                    <li><a class="grey-text text-darken-1" href="{{route('profile')}}"><i class="material-icons">person_outline</i> Profile</a></li>
                    <li class="divider"></li>
                    <li>
                        <a class="grey-text text-darken-1" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="material-icons">exit_to_app</i><span class="menu-title" data-i18n="">Quitter</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
            <nav class="display-none search-sm">
                <div class="nav-wrapper">
                    <form>
                        <div class="input-field">
                            <input class="search-box-sm" type="search" required="">
                            <label class="label-icon" for="search"><i class="material-icons search-sm-icon">search</i></label><i class="material-icons search-sm-close">close</i>
                        </div>
                    </form>
                </div>
            </nav>
        </nav>
    </div>
</header>
<!-- END: Header-->

{{--<!-- BEGIN: Header-->--}}
{{--<header class="page-topbar" id="header">--}}
    {{--<div class="navbar navbar-fixed">--}}
        {{--<nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-indigo-purple no-shadow">--}}
            {{--<div class="nav-wrapper">--}}
                {{--<a href="https://www.yanfoma.com" target="_blank">{{trans('app.GoToWebsite')}}</a>--}}
                {{--<div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>--}}
                    {{--<input class="header-search-input z-depth-2" type="text" name="Search" placeholder="chercher ici">--}}
                {{--</div>--}}
                {{--<ul class="hide-on-med-and-down right">--}}
                    {{--<li>--}}
                        {{--<a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button" data-activates="translation-dropdown">--}}
                            {{--{{ trans('app.'.app()->getLocale().'')}}</a>--}}
                        {{--<ul id="translation-dropdown" class="dropdown-content" style="white-space: nowrap; position: absolute; top: 64px; left: 1052px; opacity: 1; display: none;">--}}
                            {{--@foreach(config('translatable.locales') as $lang => $language)--}}
                                {{--@if ($lang != app()->getLocale())--}}
                                    {{--<li><a href="{{route('lang.switch', $lang)}}">{{ $language }}</a></li>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>--}}
                    {{--<li class="hide-on-large-only"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search</i></a></li>--}}
                    {{--<li>--}}
                        {{--@if($newShopNotifications->count() >1)--}}
                            {{--<a class="waves-effect waves-block waves-light notification-button" href="#" data-target="notifications-dropdown"><i class="material-icons">notifications_none<small class="notification-badge">{{$newNotifications->count()}}</small></i></a>--}}
                        {{--@else--}}
                            {{--<a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse">--}}
                                {{--<i class="mdi-social-notifications"></i>--}}
                            {{--</a>--}}
                        {{--@endif--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online">--}}
                                {{--<img src="{{env('AVATAR_LINK')}}" alt="avatar"><i></i></span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                {{--<!-- translation-button-->--}}
                {{--<ul class="dropdown-content" id="translation-dropdown">--}}
                    {{--<li><a class="grey-text text-darken-1" href="#!"><i class="flag-icon flag-icon-gb"></i> English</a></li>--}}
                {{--</ul>--}}
                {{--<!-- notifications-dropdown-->--}}
                {{--<ul class="dropdown-content" id="notifications-dropdown">--}}
                    {{--<li>--}}
                        {{--<h6>NOTIFICATIONS<span class="new badge">5</span></h6>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                {{--<!-- profile-dropdown-->--}}
                {{--<ul class="dropdown-content" id="profile-dropdown">--}}
                    {{--<li><a class="grey-text text-darken-1" href="user-lock-screen.html"> Se Deconnecter</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</nav>--}}
    {{--</div>--}}
{{--</header>--}}
{{--<!-- BEGIN: SideNav-->--}}

{{--<header id="header" class="page-topbar">--}}
    {{--<!-- start header nav-->--}}
    {{--<div class="navbar-fixed">--}}
        {{--<nav class="navbar-color">--}}
            {{--<div class="nav-wrapper">--}}
                {{--<ul class="right hide-on-med-and-down">--}}
                    {{--<li>--}}
                        {{--<a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button" data-activates="translation-dropdown">--}}
                            {{--{{ trans('app.'.app()->getLocale().'')}}</a>--}}
                        {{--<ul id="translation-dropdown" class="dropdown-content" style="white-space: nowrap; position: absolute; top: 64px; left: 1052px; opacity: 1; display: none;">--}}
                            {{--@foreach(config('translatable.locales') as $lang => $language)--}}
                                {{--@if ($lang != app()->getLocale())--}}
                                    {{--<li><a href="{{route('lang.switch', $lang)}}">{{ $language }}</a></li>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a href="https://www.yanfoma.com" target="_blank">{{trans('app.GoToWebsite')}}</a></li>--}}
                    {{--<li>--}}
                       {{--@if($newShopNotifications->count() >1)--}}
                           {{--<a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse">--}}
                               {{--<i class="mdi-social-notifications"><small class="notification-badge">{{$newNotifications->count()}}</small></i>--}}
                           {{--</a>--}}
                       {{--@else--}}
                           {{--<a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse">--}}
                               {{--<i class="mdi-social-notifications"></i>--}}
                           {{--</a>--}}
                       {{--@endif--}}
                    {{--</li>--}}
                    {{--<li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a></li>--}}
                    {{--<li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a></li>--}}
                {{--</ul>--}}
                {{--<div class="header-search-wrapper hide-on-med-and-down">--}}
                    {{--<i class="mdi-action-search active"></i>--}}
                    {{--<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">--}}
                {{--</div>--}}
                {{--<ul class="left">--}}
                    {{--<li>{{Date::parse(\Carbon\Carbon::now())->format('j F Y')}}</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</nav>--}}
    {{--</div>--}}
    {{--<!-- end header nav-->--}}
    {{--<!-- START RIGHT SIDEBAR NAV-->--}}
    {{--@if($shopNotifications->count())--}}
        {{--<aside id="right-sidebar-nav">--}}
            {{--<ul id="chat-out" class="side-nav rightside-navigation">--}}
                {{--<li class="li-hover">--}}
                    {{--<a href="#" data-activates="chat-out" class="chat-close-collapse right" style="font-size: 20px;"><i class="mdi-navigation-close"></i></a>--}}
                    {{--<div id="right-search" class="row">--}}
                        {{--<form class="col s12">--}}
                            {{--<div class="input-field">--}}
                                {{--<i class="mdi-action-search prefix"></i>--}}
                                {{--<input id="icon_prefix" type="text" class="validate">--}}
                                {{--<label for="icon_prefix">Search</label>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}

                    {{--<h6 style="padding: 10px;">SHOP NOTIFICATIONS--}}
                        {{--@if($newShopNotifications->count())--}}
                            {{--<span class="new badge">{{$newShopNotifications->count()}}</span>--}}
                        {{--@endif--}}
                    {{--</h6>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a class="center bold" href="{{route('shopNotifications')}}" style="font-weight: bold">({{$allNotifications->count()}}) SHOP NOTIFICATIONS <i class="mdi-navigation-arrow-forward"></i> ({{trans('app.voirTout')}}) </a>--}}
                {{--</li>--}}
                {{--<li class="li-hover">--}}
                    {{--<ul class="chat-collapsible" data-collapsible="expandable">--}}
                        {{--<li>--}}
                            {{--<div class="collapsible-header red white-text active">--}}
                                {{--<i class="mdi-social-whatshot"></i>Non Lus</div>--}}
                            {{--<div class="collapsible-body recent-activity">--}}
                                {{--@foreach($shopNotifications as $shopNotification)--}}
                                    {{--@if($shopNotification->view == "No")--}}
                                        {{--<div class="recent-activity-list chat-out-list row unread">--}}
                                            {{--<div class="col s3 recent-activity-list-icon">--}}
                                                {{--<i class="mdi-action-add-shopping-cart"></i>--}}
                                            {{--</div>--}}
                                            {{--<div class="col s9 recent-activity-list-text unread">--}}
                                                {{--<a href="{{route('singleShopNotification',['id'=>$shopNotification->id])}}">{{$shopNotification->name}}</a>--}}
                                                {{--<p>{{$shopNotification->name}}</p>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--@if($shopNotificationsLus->count())--}}
                            {{--<li>--}}
                                {{--<div class="collapsible-header green white-text active">--}}
                                    {{--<i class="mdi-editor-attach-money"></i>Lus</div>--}}
                                    {{--<div class="collapsible-body sales-repoart">--}}
                                        {{--@foreach($shopNotificationsLus as $shopNotification)--}}
                                            {{--<div class="recent-activity-list chat-out-list row read">--}}
                                                {{--<div class="col s3 recent-activity-list-icon">--}}
                                                    {{--<i class="mdi-action-add-shopping-cart"></i>--}}
                                                {{--</div>--}}
                                                {{--<div class="col s9 recent-activity-list-text read">--}}
                                                    {{--<a href="{{route('singleShopNotification',['id'=>$shopNotification->id])}}">{{$shopNotification->name}}</a>--}}
                                                    {{--<p>{{$shopNotification->name}}</p>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@endforeach--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--@endif--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</aside>--}}
    {{--@endif--}}
    {{--<!-- LEFT RIGHT SIDEBAR NAV-->--}}
{{--</header>--}}
