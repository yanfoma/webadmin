<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation">
        <li class="user-details cyan darken-2">
            <div class="row">
                {{--<div class="col col s4 m4 l4">
                    <img src="{{asset(Auth::user()->profile->avatar)}}" alt="" class="circle responsive-img valign profile-image">
                </div>--}}
                <div class="col col s8 m8 l8">
                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#"
                       data-activates="profile-dropdown">{{Auth::user()->name}}<i
                                class="mdi-navigation-arrow-drop-down right"></i></a>

                    <ul id="profile-dropdown" class="dropdown-content">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="mdi-hardware-keyboard-tab"></i> {{ trans('app.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="bold {{ isActiveURL('/cosmic/index') }}"><a href="{{route('cosmic')}}"
                                                               class="waves-effect waves-cyan"><i
                        class="mdi-action-dashboard"></i> {{ trans('app.dashboard') }}</a></li>
        <li class="bold {{ isActiveURL('/yanfoshop/categories') }}"><a href="{{route('products.index')}}"
                                                                       class="waves-effect waves-cyan"><i
                        class="mdi-maps-local-mall"></i> {{ trans('YanfomaShop') }}</a></li>
        <li class="bold {{ isActiveURL('/yanfoshop/post/create') }}"><a href="{{route('products.create')}}"
                                                                        class="waves-effect waves-cyan"><i
                        class="mdi-action-note-add"></i> {{ trans('app.newProduct') }}</a></li>
        @if(Auth::user()->admin)
            <li class="bold {{ isActiveURL('/yanfoshop/users') }}"><a href="{{route('users.index')}}"
                                                                      class="waves-effect waves-cyan"><i
                            class="mdi-action-account-circle prefix"></i> {{ trans('app.users') }}</a></li>
            {{--<li class="bold {{ isActiveURL('/yanfoshop/settings') }}">          <a href="{{route('settings.index')}}"       class="waves-effect waves-cyan"><i class="mdi-action-settings"></i>              {{ trans('app.settings') }}</a></li>--}}
        @endif
        <li class="bold {{ isActiveURL('/yanfoshop/slideshow') }}"><a href="{{route('slideshow.index')}}"
                                                                      class="waves-effect waves-cyan"><i
                        class="mdi-editor-insert-photo"></i> {{ trans('app.slideshow') }}</a></li>
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="mdi-hardware-keyboard-tab"></i> {{ trans('app.logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</aside>
<!-- END LEFT SIDEBAR NAV-->