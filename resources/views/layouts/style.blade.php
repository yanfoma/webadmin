<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- BEGIN: VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/vendors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate-css/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/chartist-js/chartist.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/chartist-js/chartist-plugin-tooltip.css') }}">
<!-- END: VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('css/themes/vertical-modern-menu-template/materialize.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/themes/vertical-modern-menu-template/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/pages/dashboard-modern.css') }}">
<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom/custom.css') }}">


{{--<!-- Styles -->--}}
{{--<link href="{{ asset('css/materialize.css') }}"       type="text/css" rel="stylesheet" media="screen,projection">--}}
{{--<link href="{{ asset('css/style.css') }}"             type="text/css" rel="stylesheet" media="screen,projection">--}}
{{--<!-- Custome CSS-->--}}
{{--<link href="{{ asset('css/custom.css') }}"            type="text/css" rel="stylesheet" media="screen,projection">--}}
{{--<link href="{{ asset('css/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">--}}


<link href="{{ asset('css/dropify.min.css') }}"       type="text/css" rel="stylesheet" media="screen,projection">
<link href="{{ asset('css/toastr.min.css') }}"        type="text/css" rel="stylesheet" media="screen,projection">

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"        type="text/css" rel="stylesheet" media="screen,projection">

{{--<link href="{{asset('css/materialnote.css')}}"        type="text/css" rel="stylesheet" >--}}
{{--<link href="{{ asset('css/animate.css') }}"           type="text/css" rel="stylesheet" media="screen,projection">--}}

<!--Sweet Alert-->
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.css'>



<link href="{{ asset('css/select2.css') }}"        type="text/css" rel="stylesheet" media="screen,projection">

{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />--}}