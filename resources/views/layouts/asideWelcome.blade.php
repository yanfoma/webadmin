<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="{{route('welcome')}}"><span class="logo-text hide-on-med-and-down">WEBADMIN</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
    </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
            <li class="bold">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel','fr/webadmin-panel') ?'active':''}} " href="{{route('welcome')}}" >
                    <i class="material-icons">store</i><span class="menu-title" data-i18n="">Stores</span>
                </a>
            </li>
            <li class="bold">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/store/create','fr/webadmin-panel/store/create') ?'active':''}} " href="{{route('store.create')}}" >
                    <i class="material-icons">create_new_folder</i><span class="menu-title" data-i18n="">Create Store</span>
                </a>
            </li>
            <li class="bold">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/stores-categories','fr/webadmin-panel/stores-categories') ?'active':''}}" href="{{route('storeCategories.index')}}" >
                    <i class="material-icons">view_list</i><span class="menu-title" data-i18n="">Stores Categories</span>
                </a>
            </li>
            <li class="bold {{\Request::is('en/webadmin-panel/products/create','fr/webadmin-panel/products/create') ?'active':''}} ">
                <a class="waves-effect waves-cyan {{\Request::is('en/webadmin-panel/storeCategories/create','fr/webadmin-panel/storeCategories/create') ?'active':''}} " href="{{route('storeCategories.create')}}" >
                    <i class="material-icons">add_box</i><span class="menu-title" data-i18n="">New Store Categories</span>
                </a>
            </li>
        </ul>
    <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->