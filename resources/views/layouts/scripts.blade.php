<!-- Scripts -->
{{--<script type="text/javascript" src="{{ asset('js/jquery-1.11.2.min.js') }}"></script>--}}
<!--materialize js-->
{{--<script type="text/javascript" src="{{ asset('js/materialize.js') }}"></script>--}}
<!--scrollbar-->
{{--<script type="text/javascript" src="{{ asset('js/perfect-scrollbar.min.js') }}"></script>--}}
{{--<!--plugins.js - Some Specific JS codes for Plugin Settings-->--}}
{{--<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>--}}
{{--<!--custom-script.js - Add your own theme custom JS-->--}}

<script src="{{ asset('js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN THEME  JS-->
<script src="{{ asset('js/plugins.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/custom/custom-script.js')}}" type="text/javascript"></script>

<script src="{{ asset('js/dropzone.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/dropzone.min.js')}}" type="text/javascript"></script>
<!-- BEGIN THEME  JS-->
<!--dropify-->
<script type="text/javascript" src="{{asset('js/dropify.min.js')}}"></script>
<!--Sweet Alert-->
<script src="{{asset('js/sweetalert.min.js')}}"></script>

<script src="{{asset('js/select2.min.js')}}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.6/tinymce.min.js"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<!--tostr-->
<script type="text/javascript" src="{{asset('js/toastr.min.js')}}"></script>

<script type="text/javascript" src="{{asset('js/Chart.bundle.min.js')}}"></script>
<script>
    @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
    @endif

    @if(Session::has('info'))

        toastr.info("{{ Session::get('info') }}");

    @endif

    @if (Session::has('sweet_alert.alert'))

        swal({
        text: "{!! Session::get('sweet_alert.text') !!}",
        title: "{!! Session::get('sweet_alert.title') !!}",
        timer: "{!! Session::get('sweet_alert.timer') !!}",
        type: "{!! Session::get('sweet_alert.type') !!}",
        showConfirmButton: "{!! Session::get('sweet_alert.showConfirmButton') !!}",
        confirmButtonText: "{!! Session::get('sweet_alert.confirmButtonText') !!}",
        confirmButtonColor: "#AEDEF4"

	@endif

</script>

<script>
    var editor_config = {
        path_absolute : "/",
        themes: "inlite",
        mobile: {
            theme: 'mobile',
            plugins: [ 'autosave', 'lists', 'autolink' ]
        },
        height : "400",
        selector: "textarea",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo paste  spellchecker pagebreak visualblocks visualchars anchor autolink charmap help linkchecker nonbreaking legacyoutput tabfocus | styleselect | bold italic | forecolor backcolor | restoredraft |alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | advlist | link image media | emoticons | fullpage | preview | hr | insertdatetime | searchreplace",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'en/laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);
</script>

<script type="text/javascript">


    function testAnim(x) {
        $('#animationSandbox').removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass();
        });
    };

    $(document).ready(function(){
        $('.js--triggerAnimation').click(function(e){
            e.preventDefault();
            var anim = $('.js--animations').val();
            testAnim(anim);
        });

        $('.js--animations').change(function(){
            var anim = $(this).val();
            testAnim(anim);
        });
    });


</script>

<script type="text/javascript">
    $(document).ready(function(){
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-event').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove:  'Supprimer',
                error:   'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('.dropify-event').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Souhaitez - vous vraiment supprimer  \"" + element.filename + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('Image  supprimée');
        });
    });
</script>

<script type="text/javascript">
    $('#whatsapp').hide();
    $('#wa_confirmed').on('click' , function (event) {
        if ($(this).prop('checked')) {
            $('#mail').hide();
            $('#whatsapp').show();
        } else {
            $('#mail').show();
            $('#whatsapp').hide();
        }
    });
</script>

