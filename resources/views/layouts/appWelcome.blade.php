<!DOCTYPE html>
<html class="loading" data-textdirection="ltr" lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    @include('layouts.style')
    @yield('style')

</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 2-columns  " data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

@include('layouts.header')
@include('layouts.asideWelcome')
<div id="main">
    <div class="row">
        <div class="content-wrapper-before"></div>
        <div class="col s12">
            <div class="container">
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
@include('layouts.scripts')
@include('sweet::alert')
@yield('scripts')
</body>
</html>
