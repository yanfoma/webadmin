<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>WebAdmin Login</title>
    @include('layouts.style')
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/login.css')}}">
    @yield('style')
</head>

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 1-column login-bg blank-page blank-page">
    @yield('content')
@include('layouts.scripts')

</body>
</html>
