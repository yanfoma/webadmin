@component('mail::message')

<span style="color:black">
    {{trans('app.contactMessage')}} {{ $name }}

    Subject:{{ $subject }}
    Message:{{ $message }}
</span>


@component('mail::panel', ['url' => ''])
#Contacts:

Name: {{ $name }}

Phone: {{ $phone }}

Email: {{ $email }}


@endcomponent

@endcomponent
