<html>
<head></head>
    <body>
    <div class="container">
        <h1 class="text-center">You have a new Purchase</h1>
    </div>
    <table data-module="orderlist_detail" data-bgcolor="Main BG" align="center" bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="20">
            </td>
        </tr>
        <tr>
            <td data-bgcolor="Detail BG" align="center" bgcolor="#f3f3f3">
                <table border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="600" align="center">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td height="30">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="text-align:center;vertical-align:top;font-size:0;">
                                        <div style="display:inline-block;vertical-align:top;">
                                            <!--left-->
                                            <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="300" align="center">
                                                        <table class="table-full" width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                            <!--title-->

                                                            <tr align="left">
                                                                <td data-link-style="text-decoration:none; color:#91c444;" data-link-color="Title Link" data-color="Title" data-size="Title" style="font-family: 'Open Sans', Arial, sans-serif; font-size:18px;color:#3b3b3b;font-weight: bold;">
                                                                    <singleline>
                                                                        Résumer
                                                                    </singleline>
                                                                </td>
                                                            </tr>
                                                            <!--end title-->
                                                            <tr>
                                                                <td align="left">
                                                                    <table align="left" width="30" border="0" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td data-border-bottom-color="Dash" style="border-bottom:2px solid #91c444;" height="15">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="10">
                                                                </td>
                                                            </tr>
                                                            @php $index = 1; @endphp
                                                            @foreach(Cart::content() as $product)
                                                                <tr align="left">
                                                                    <td data-link-style="text-decoration:none; color:#91c444;" data-link-color="Main Text" data-color="Main Text" style="font-family: 'Open Sans', Arial, sans-serif; font-size:13px;color:#7f8c8d;line-height: 28px; font-weight:normal;">
                                                                        <multiline>
                                                                            <span style="color:#3b3b3b;font-weight: bold;">{{$index}}</span><br />
                                                                            <span style="color:#3b3b3b;font-weight: bold;">Nom :</span>  {{$product->name}} <br />
                                                                            <span style="color:#3b3b3b;font-weight: bold;">Quantité :</span>  {{$product->qty}} <br />
                                                                            <span style="color:#3b3b3b;font-weight: bold;">Prix :</span>  {{$product->price}} CFA <br />
                                                                            <span style="color:#3b3b3b;font-weight: bold;">Total :</span>  {{Cart::total()}} CFA
                                                                            <hr>
                                                                        </multiline>
                                                                    </td>
                                                                </tr>
                                                            @php $index++; @endphp
                                                            @endforeach
                                                            <tr>
                                                                <td align="center">
                                                                    <table data-bgcolor="Button 2" data-border-bottom-color="Button Shadow 2" style="border-radius:5px;border-bottom:2px solid #E2E2E2;" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                                                        <tr>
                                                                            <td data-link-style="text-decoration:none; color:#3597D4;" data-link-color="CTA Button Link" data-color="CTA Button" data-size="CTA Button" height="45" align="center" style="font-family:Century Gothic, Arial, Sans-serif; font-size:16px; color:#4a4a4a;padding-left:25px;padding-right:25px;">
                                                                                <a href="#" style="text-decoration:none;color:#3597D4;" data-color="CTA Button Link">Voir</a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--end left-->
                                        </div> <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                        <![endif]-->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </body>
</html>