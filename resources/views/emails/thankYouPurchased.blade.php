<html>
    <head></head>
    <body>
    <table data-module="orderlist_header" data-bgcolor="Header BG" data-bg="Header BG" data-border-bottom-color="header border" bgcolor="#3b3b3b" style="background-size: cover; background-position: center center; border-bottom: 3px solid #3e4494; border-collapse: collapse; background-image: url('{{asset('images/frontEnd/thankYouPurshased.png')}}');" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="currentTable">
        <tr>
            <td height="55" align="center" style="background-color:rgba(0,0,0,0.2)">
                <table border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                    <tr>
                        <td width="600" align="center">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td align="center" style="text-align:center;vertical-align:top;font-size:0;">
                                        <div style="display:inline-block;vertical-align:top;">
                                            <!--spacing-->

                                            <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="250" height="15">
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--end spacing-->

                                        </div> <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                        <td align="center" style="text-align:center;vertical-align:top;font-size:0;">
                                        <![endif]-->
                                        <div style="display:inline-block;vertical-align:top;">
                                            <!--right-->

                                            <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td height="15">
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--end right-->
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table align="center" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" width="600">
                            <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td height="55">
                                    </td>
                                </tr>

                                <tr align="center">
                                    <td data-link-style="text-decoration:none; color:#91c444;" data-link-color="Header Link" data-color="Header Text" data-size="Header Text" style="font-family: 'Open Sans', Arial, sans-serif; font-size:38px; color:#ffffff;letter-spacing: 8px;font-weight: bold;text-transform: uppercase;" class="editable">
                                        <singleline>
                                            {{trans('app.thankYou')}}<br>
                                        </singleline>
                                    </td>
                                </tr>

                                <tr align="center">
                                    <td>
                                        <table align="center" width="30" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td data-border-bottom-color="Dash" style="border-bottom:2px solid #ffffff;" height="10">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="15">
                                    </td>
                                </tr>

                                <tr align="center">
                                    <td data-link-style="text-decoration:none; color:#91c444;" data-link-color="Header Link" data-color="Header Text" data-size="Header Text" style="font-family: 'Open Sans', Arial, sans-serif; font-size:12px; color:#ffffff;letter-spacing: 2px;text-transform: capitalize; line-height: 20px;">
                                        <singleline>
                                            {{trans('app.thankYouPurchased')}}
                                        </singleline>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="55">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="line-height: 0px;">
            </td>
        </tr>
    </table>
    </body>
</html>