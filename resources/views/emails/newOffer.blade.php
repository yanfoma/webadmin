<html>
    <head></head>
    <body>
        <table data-module="main-6_cta" data-bgcolor="Main BG" align="center" width="100%" bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" class="currentTable">
            <tr>
                <td height="20">
                </td>
            </tr>
            <tr>
                <td data-bgcolor="CTA BG" data-bg="CTA BG" align="center" bgcolor="#3597D4" background="url('{{asset('images/frontEnd/jobOffer.png')}}')" style="background-size: cover; background-position: center bottom; background-image: url('{{asset('images/frontEnd/jobOffer.png')}}');">
                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" width="600">
                                <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="45">
                                        </td>
                                    </tr>
                                    <!--headline-->

                                    <tr>
                                        <td data-link-style="text-decoration:none; color:#3597D4;" data-link-color="CTA Title Link" data-color="CTA Title" data-size="CTA Title" align="center" style="font-family:Century gothic, Arial, Sans-serif; font-size:18px; color:#ffffff;font-weight: bold;">
                                            <singleline>
                                                You Have a new Demande !!!
                                            </singleline>
                                        </td>
                                    </tr>
                                    <!--end headline-->

                                    <tr>
                                        <td height="15">
                                        </td>
                                    </tr>
                                    <!--content-->

                                    <tr>
                                        <td data-link-style="text-decoration:none; color:#3597D4;" data-link-color="CTA content Link" data-color="CTA content" data-size="CTA content" align="center" style="font-family:Open sans, Arial, Sans-serif; font-size:14px; color:#ffffff;line-height:28px;">
                                            <multiline>
                                                {{$name}} {{trans('app.appliedMessage')}} {{$code}}.<br>
                                                <b><u>{{trans('app.nom')}}</u></b> {{$name}}
                                                <b><u>Email:</u></b> <span style="color: white"> {{$email}} </span></b></u>
                                                <b><u>{{trans('app.phone')}}:</u></b>{{$phone}}<br>
                                                <b><u>{{trans('app.message')}}:</u></b>{{$content}}

                                            </multiline>
                                        </td>
                                    </tr>
                                    <!--end content-->

                                    <tr>
                                        <td height="25">
                                        </td>
                                    </tr>
                                    <!--button-->

                                    <tr>
                                        <td align="center">
                                            <table data-bgcolor="Button 2" data-border-bottom-color="Button Shadow 2" style="border-radius:5px;border-bottom:2px solid #E2E2E2;" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                                <tr>
                                                    <td data-link-style="text-decoration:none; color:#3597D4;" data-link-color="CTA Button Link" data-color="CTA Button" data-size="CTA Button" height="45" align="center" style="font-family:Century Gothic, Arial, Sans-serif; font-size:16px; color:#4a4a4a;padding-left:25px;padding-right:25px;">
                                                        <a href="{{route('singleNotification',['id' => $id])}}" style="text-decoration:none;color:#3597D4;" data-color="CTA Button Link">{{trans('app.view')}}</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!--end button-->

                                    <tr>
                                        <td height="45">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="20">
                </td>
            </tr>
        </table>
    </body>
</html>