<html>
<head></head>
<body>
    Bonjour Mr/Me {{$name}}
    <p>Votre Commande no:{{$purchasedId}} Montant: {{$total}}.

        <br>Etape:  {{$step}}
        <br>Date:   {{ $date }}

        @if($trackingCode)
            <br> Votre code est : {{$trackingCode}}
        @endif

        @if($note)
            <p> Note: {!! $note !!}</p>
        @endif


        <br>Vous pouvez dès à présent tracker votre commande en vous rendant sur votre profile (en cliquant sur "Mon profile") puis "Mes commandes" et vous pourrez suivre votre commande à tout moment.
        Ou cliquer sur <a href="https://www.yanfoma.com/fr/customer/mes-commandes" class="theme-btn btn-style-one center">Suivre mes Commandes</a>
        <br> <br>Merci de Votre Confiance.
        <br>Equipe YanfoShop!!!
    </p>
</body>
</html>
