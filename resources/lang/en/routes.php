<?php

return [

    'home'              => 'home',
    'welcome'           => '/en',
    'whoAreWe'          => 'about-us/who-are-we',
    'community'         => 'about-us/our-communities',
    'portfolio'         => 'about-us/yanfoma-portfolio',
    'testimonial'       => 'about-us/clients-feedback',
    'partnership'       => 'service/partnership',
    'web'               => 'service/web',
    'medias'            => 'service/medias',
    'analytics'         => 'service/analytics',
    'referral'          => 'service/referral',
    'yanfobot'          => 'service/yanfobot',
    'packages'          => 'career/yanfoma-packages',
    'offers'            => 'career/freelance',
    'news'              => 'news',
    'shop'              => 'shop',
    'contact'           => 'contact',
    'notreHistoire'     => 'our-history',
    'notreEquipe'       => 'our-team',
    'howWeWork'         => 'how-we-work',
    'ghanaHub'          => 'ghana-hub',
    'areYouInterrested' => 'are-you-interrested',
    'ourNews'           => 'our-news',
    'blog'              => 'our-blog',
    'contactUs'         => 'contact-us',
    'ressources'        => 'our-ressources',
    'yanfoshop'             => 'yanfoshop',
    'cosmic'             => 'comsic',
    'index'             => '/en',
    'post.index'        => 'all-posts',
    'post.create'       => 'new-post',
    'post.trashed'      => 'trash',
    'category.create'   => 'new-categorie',
    'category.index'    => 'all-categories',
    'tags.create'       => 'new-tag',
    'tags.index'        => 'all--tags',
    'users.index'       => 'all-users',
    'users.create'      => 'create-user',
    'settings.index'    => 'settings',
    'settings.update'   => 'edit-les-paramètres',
    'users.my.profile'  => 'mon-profile',
    'logout'            => 'logout',
    'slideshow.index'   => 'Slideshow',
    'slideshow.update'  => 'Slideshow-update',
    'slideshow.delete'  => 'Slideshow-delete',
    'team.index'        => 'team',
    'team.update'       => 'team-update',
    'team.delete'       => 'team-delete',
    'team.create'       => 'create-member',
    'results'           => 'search-results',



];