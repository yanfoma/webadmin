<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
    |--------------------------------------------------------------------------\
    |                                       Store Routes                       \
    |--------------------------------------------------------------------------\
    */


Route::get('/', ['as' => 'welcome', 'uses' => 'PagesController@welcome']);

Route::group(['middleware' => ['auth'],'prefix' => 'webadmin-panel'], function ()
{
    Route::get('/', ['as' => 'welcome', 'uses' => 'PagesController@welcome']);

    Route::get('/store/all-stores', [
        'uses' => 'PagesController@welcome',
        'as'   => 'store.index'
    ]);

    Route::get('/dashboard/{slug}', [
        'uses' => 'HomeController@index',
        'as'   => 'store.view'
    ]);
    Route::get('/store/create', [
        'uses' => 'StoreController@create',
        'as'   => 'store.create'
    ]);

    Route::post('/store/store', [
        'uses' => 'StoreController@store',
        'as'   => 'store.store'
    ]);

    Route::get('/store/edit/{id}', [
        'uses' => 'StoreController@edit',
        'as'   => 'store.edit'
    ]);

    Route::get('/store/delete/{id}', [
        'uses' => 'StoreController@destroy',
        'as'   => 'store.delete'
    ]);

    Route::post('/store/update/{id}', [
        'uses' => 'StoreController@update',
        'as'   => 'store.update'
    ]);


    /*
    |--------------------------------------------------------------------------\
    |                                       StoreCategories Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get('stores-categories', [
        'uses' => 'StoreCategoriesController@index',
        'as'   => 'storeCategories.index'
    ]);

    Route::get('storeCategories/create', [
        'uses' => 'StoreCategoriesController@create',
        'as'   => 'storeCategories.create'
    ]);

    Route::post('/storeCategories/store', [
        'uses' => 'StoreCategoriesController@store',
        'as'   => 'storeCategories.store'
    ]);

    Route::get('/storeCategories/edit/{id}', [
        'uses' => 'StoreCategoriesController@edit',
        'as'   => 'storeCategories.edit'
    ]);

    Route::post('/storeCategories/update/{id}', [
        'uses' => 'StoreCategoriesController@update',
        'as'   => 'storeCategories.update'
    ]);

    Route::get('/storeCategories/delete/{id}', [
        'uses' => 'StoreCategoriesController@destroy',
        'as'   => 'storeCategories.delete'
    ]);

});

Route::resource('adminHome','AdminController', [['except' => 'show']]);

Route::get('mails-center' , [
    'as' => 'mails-center' ,
    'uses' => 'AdminController@mailCenter'
]);

Route::post('/customer/mails-center', [
    'as' => 'yanfoma-customers-mails',
    'uses' => 'AdminController@sendMailToCustomers'
]);
/*Route::get('login' , function() {
   return view('auth.login');
});*/

Route::get(trans('routes.notifications'), [
    'as'    => 'notifications',
    'uses'  => 'AdminController@notifications'
]);

Route::get(trans('routes.shopNotifications'), [
    'as'    => 'shopNotifications',
    'uses'  => 'AdminController@shopNotifications'
]);

Route::get(trans('single/notification/{id}'), [
    'as'    => 'singleNotification',
    'uses'  => 'AdminController@singleNotification'
]);

Route::get(trans('single/Shop-Notification/{id}'), [
    'as'    => 'singleShopNotification',
    'uses'  => 'AdminController@singleShopNotification'
]);

Route::post('delete/bulk-notifications', [
    'as'    => 'deleteBulkNotifications',
    'uses'  => 'AdminController@deleteBulkNotifications'
]);

Route::post('delete/bulk-shop-notifications', [
    'as'    => 'deleteShopBulkNotifications',
    'uses'  => 'AdminController@deleteShopBulkNotifications'
]);

Route::get('lang/{language}', ['as' => 'lang.switch', 'uses'    => 'LanguageController@switchLang']);

Route::get('deconnection',[
    'uses'=> 'UsersController@logout',
    'as'  => 'logout'
]);

Auth::routes();


Route::group(['middleware' => ['auth'],'prefix' => 'webadmin-panel'], function () {

    Route::get('/yanfoshop', [
        'uses' =>  'HomeController@index',
        'as'   => 'yanfoshop'
    ]);

    Route::get(trans('routes.cosmic'), [
        'uses' =>  'HomeController@index1',
        'as'   => 'cosmic'
    ]);


    /*
    |--------------------------------------------------------------------------\
    |                                 Grandes Categories Routes                \
    |--------------------------------------------------------------------------\
    */
    Route::get('/toutes-les-grandes-categories', [

        'uses' => 'ParentCategoryController@index',
        'as'   => 'parentCategory.index'
    ]);


    Route::get('/grandes-categories/create', [

        'uses' => 'ParentCategoryController@create',
        'as'   => 'parentCategory.create'
    ]);

     Route::get('/grandes-categories/{id}', [

        'uses' => 'ParentCategoryController@show',
        'as'   => 'parentCategory.show'
    ]);


    Route::post('/grandes-categories/store', [

        'uses' => 'ParentCategoryController@store',
        'as'   => 'parentCategory.store'
    ]);


    Route::get('/grandes-categories/edit/{id}', [

        'uses' => 'ParentCategoryController@edit',
        'as'   => 'parentCategory.edit'
    ]);

    Route::post('/grandes-categories/update/{id}', [

        'uses' => 'ParentCategoryController@update',
        'as'   => 'parentCategory.update'
    ]);

    Route::get('/grandes-categories/delete/{id}', [

        'uses' => 'ParentCategoryController@delete',
        'as'   => 'parentCategory.delete'
    ]);
    /*
    |--------------------------------------------------------------------------\
    |                                 SubCategories Routes                     \
    |--------------------------------------------------------------------------\
    */

    Route::get('/create-subcategories/{id}', [

        'uses' => 'SubcategoryController@create',
        'as'   => 'subCategory.create'
    ]);

    Route::post('/store-subcategories', [

        'uses' => 'SubcategoryController@Store',
        'as'   => 'subcategories.store'
    ]);

    Route::get('/sub-categories/addCategories/{subcat_id}/{parent_id}', [

        'uses' => 'SubcategoryController@addCategories',
        'as'   => 'subcategories.addCategories'
    ]);

    Route::post('/sub-categories/storeCategories', [

        'uses' => 'SubcategoryController@storeCategories',
        'as'   => 'subcategories.storeCategories'
    ]);

    Route::get('/sub-categories/edit/{id}/{parent_id}', [

        'uses' => 'SubcategoryController@edit',
        'as'   => 'subCategory.edit'
    ]);

    Route::post('/sub-categories/update', [

        'uses' => 'SubcategoryController@update',
        'as'   => 'subCategory.update'
    ]);



    Route::get('/sub-categories/delete/{id}/{parent_id}', [

        'uses' => 'SubcategoryController@delete',
        'as'   => 'subCategory.delete'
    ]);

    Route::get('/sub-categories/delCategories/{id}/{parent_id}', [

        'uses' => 'SubcategoryController@delCategories',
        'as'   => 'subcategories.delCategories'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                      Categories Routes                   \
    |--------------------------------------------------------------------------\
    */
    Route::get(trans('routes.category.create'), [

        'uses' => 'CategoriesController@create',
        'as'   => 'category.create'
    ]);

    Route::post('/category/store', [

        'uses' => 'CategoriesController@store',
        'as'   => 'category.store'
    ]);

    Route::get(trans('routes.category.index'), [

        'uses' => 'CategoriesController@index',
        'as'   => 'category.index'
    ]);

    Route::get('/category/edit/{id}', [

        'uses' => 'CategoriesController@edit',
        'as'   => 'category.edit'
    ]);


    Route::get('/category/delete/{id}', [

        'uses' => 'CategoriesController@destroy',
        'as'   => 'category.delete'
    ]);

    Route::post('/category/update/{id}', [

        'uses' => 'CategoriesController@update',
        'as'   => 'category.update'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Coupons Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get(trans('/coupons/create'), [

        'uses' => 'CouponController@create',
        'as'   => 'coupons.create'
    ]);

    Route::post('/coupons/store', [

        'uses' => 'CouponController@store',
        'as'   => 'coupons.store'
    ]);

    Route::get(trans('/all-coupons'), [

        'uses' => 'CouponController@index',
        'as'   => 'coupons.index'
    ]);

    Route::get('/coupons/edit/{id}', [

        'uses' => 'CouponController@edit',
        'as'   => 'coupons.edit'
    ]);

    Route::get('/coupons/delete/{id}', [

        'uses' => 'CouponController@destroy',
        'as'   => 'coupons.delete'
    ]);

    Route::post('/coupons/update/{id}', [

        'uses' => 'CouponController@update',
        'as'   => 'coupons.update'
    ]);
    /*
    |--------------------------------------------------------------------------\
    |                                       Sales Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get(trans('/sales/create'), [

        'uses' => 'SalesController@create',
        'as'   => 'sales.create'
    ]);

    Route::post('/sales/store', [

        'uses' => 'SalesController@store',
        'as'   => 'sales.store'
    ]);

    Route::post('/sales/storePercentage', [

        'uses' => 'SalesController@storePercentage',
        'as'   => 'sales.storePercentage'
    ]);

    Route::get(trans('/all-sales'), [

        'uses' => 'SalesController@index',
        'as'   => 'sales.index'
    ]);

    Route::get('/sales/edit/{id}', [

        'uses' => 'SalesController@edit',
        'as'   => 'sales.edit'
    ]);

    Route::post('/sales/update/{id}', [

        'uses' => 'SalesController@update',
        'as'   => 'sales.update'
    ]);

    Route::get('/sales/delete/{id}', [

        'uses' => 'SalesController@destroy',
        'as'   => 'sales.delete'
    ]);

    Route::get('/sales/delete-all', [
        'uses' => 'SalesController@deleteAll',
        'as'   => 'sales.deleteAll'
    ]);



    /*
    |--------------------------------------------------------------------------\
    |                                       Tracking Routes                       \
    |--------------------------------------------------------------------------\
    */

    Route::get('/products/purchased/tracking', [
        'uses' => 'TrackingController@index',
        'as'   => 'tracking.index'
    ]);

    Route::get('/products/purchased/tracking/create', [
        'uses' => 'TrackingController@create',
        'as'   => 'tracking.create'
    ]);

    Route::post('/products/purchased/tracking/store', [
        'uses' => 'TrackingController@store',
        'as'   => 'tracking.store'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Users Routes                       \
    |--------------------------------------------------------------------------\
    */

    Route::get(trans('routes.users.index'), [

        'uses' => 'UsersController@index',
        'as'   => 'users.index'
    ]);

    Route::get('/products/purchased-items', [
        'uses' => 'PagesController@purchased',
        'as'   => 'purchased.index'
    ]);

    Route::get('/products/comfirm-ecobank-transaction', [
        'uses' => 'PagesController@ecobankConfirm',
        'as'   => 'purchased.ecobankConfirm'
    ]);

    Route::get('/products/{id}/comfirmed-transaction', [
        'uses' => 'PagesController@ecobankConfirmed',
        'as'   => 'purchased.ecobankConfirmed'
    ]);

    Route::get('/products/liked-items', [

        'uses' => 'PagesController@liked',
        'as'   => 'liked.index'
    ]);
    Route::get('/products/favorited-items', [

        'uses' => 'PagesController@favorited',
        'as'   => 'favorited.index'
    ]);

    Route::get(trans('routes.users.create'), [

        'uses' => 'UsersController@create',
        'as'   => 'users.create'
    ]);

    Route::get('/user/edit/{id}', [

        'uses' => 'UsersController@edit',
        'as'   => 'users.edit'
    ]);

    Route::get('/user/delete/{id}', [

        'uses' => 'UsersController@destroy',
        'as'   => 'users.delete'
    ]);

    Route::post('/user/update/{id}', [

        'uses' => 'UsersController@update',
        'as'   => 'users.update'
    ]);

    Route::post('/user/store', [

        'uses' => 'UsersController@store',
        'as'   => 'users.store'
    ]);

    Route::get('/user/yanfoshop_admin/{id}', [

        'uses' => 'UsersController@admin',
        'as'   => 'users.admin'
    ]);

    Route::get('/user/not-admin/{id}', [

        'uses' => 'UsersController@not_admin',
        'as'   => 'users.not.admin'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Team Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get('/team',[
        'uses' => 'TeamController@index',
        'as'   => 'team.index'
    ]);

    Route::get('/team/create',[
        'uses' => 'TeamController@create',
        'as'   => 'team.create'
    ]);

    Route::post('/team/store',[
        'uses' => 'TeamController@store',
        'as'   => 'team.store'
    ]);

    Route::get('/team/delete/{id}', [

        'uses' => 'TeamController@delete',
        'as'   => 'team.delete'
    ]);

    Route::post(trans('routes.settings.update'), [

        'uses' => 'SettingsController@update',
        'as'   => 'settings.update'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       SlideShow Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get(trans('routes.slideshow.index'),[

        'uses' => 'SlideShowController@index',
        'as'   => 'slideshow.index'

    ]);
    Route::post(trans('routes.slideshow.update'),[

        'uses' => 'SlideShowController@upload',
        'as'   => 'slideshow.update'

    ]);
    Route::post('sortSlideShow',[

        'uses' => 'SlideShowController@updateOrder',
        'as'   => 'slideshow.updateOrder'

    ]);

    Route::delete('slideshow/delete/{id}',[

        'uses' => 'SlideShowController@destroy',
        'as'   => 'slideshow.delete'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Interests Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get('/interests',[

        'uses' => 'InterestController@index',
        'as'   => 'interest.index'
    ]);
    Route::get(trans('/interests/create'),[

        'uses' => 'InterestController@create',
        'as'   => 'interest.create'
    ]);
    Route::post(trans('/interests/store'),[

        'uses' => 'InterestController@store',
        'as'   => 'interest.store'
    ]);

    Route::get('interests/{id}/edit',[

        'uses' => 'InterestController@edit',
        'as'   => 'interest.edit'
    ]);

    Route::post('interests/{id}/update',[

        'uses' => 'InterestController@update',
        'as'   => 'interest.update'
    ]);

    Route::get('interests/{id}/delete',[

        'uses' => 'InterestController@destroy',
        'as'   => 'interest.destroy'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Faq Routes                       \
    |--------------------------------------------------------------------------\
    */

    Route::get('/faq',[

        'uses' => 'FaqController@index',
        'as'   => 'faq.index'
    ]);
    Route::get(trans('/faq/create'),[

        'uses' => 'FaqController@create',
        'as'   => 'faq.create'
    ]);
    Route::post(trans('/faq/store'),[

        'uses' => 'FaqController@store',
        'as'   => 'faq.store'
    ]);

    Route::get('faq/{id}/edit',[

        'uses' => 'FaqController@edit',
        'as'   => 'faq.edit'
    ]);

    Route::post('faq/{id}/update',[

        'uses' => 'FaqController@update',
        'as'   => 'faq.update'
    ]);

    Route::get('faq/{id}/delete',[

        'uses' => 'FaqController@destroy',
        'as'   => 'faq.destroy'
    ]);

    Route::resource('products', 'ProductsController',['except'=>['edit','update','destroy']]);

    Route::get('products/{id}/edit',[

        'uses' => 'ProductsController@edit',
        'as'   => 'products.edit'
    ]);

    Route::post('products/{id}/update',[

        'uses' => 'ProductsController@update',
        'as'   => 'products.update'

    ]);

    Route::get('products/delete/{id}',[

        'uses' => 'ProductsController@delete',
        'as'   => 'products.delete'

    ]);


});


Route::group(['middleware' => 'auth','prefix' => 'webadmin-panel'], function () {

    Route::get('/tableau-de-bord', [
        'uses' =>  'HomeController@index',
        'as'   => 'dashboard'
    ]);

    Route::get(trans('routes.cosmic'), [
        'uses' =>  'HomeController@index1',
        'as'   => 'cosmic'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Category Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get(trans('routes.category.create'), [

        'uses' => 'CategoriesController@create',
        'as'   => 'category.create'
    ]);

    Route::post('/category/store', [

        'uses' => 'CategoriesController@store',
        'as'   => 'category.store'
    ]);

    Route::get(trans('routes.category.index'), [

        'uses' => 'CategoriesController@index',
        'as'   => 'category.index'
    ]);

    Route::get('/category/edit/{id}', [

        'uses' => 'CategoriesController@edit',
        'as'   => 'category.edit'
    ]);

    Route::get('/category/delete/{id}', [

        'uses' => 'CategoriesController@destroy',
        'as'   => 'category.delete'
    ]);

    Route::post('/category/update/{id}', [

        'uses' => 'CategoriesController@update',
        'as'   => 'category.update'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Coupons Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get(trans('/coupons/create'), [

        'uses' => 'CouponController@create',
        'as'   => 'coupons.create'
    ]);

    Route::post('/coupons/store', [

        'uses' => 'CouponController@store',
        'as'   => 'coupons.store'
    ]);

    Route::get(trans('/all-coupons'), [

        'uses' => 'CouponController@index',
        'as'   => 'coupons.index'
    ]);

    Route::get('/coupons/edit/{id}', [

        'uses' => 'CouponController@edit',
        'as'   => 'coupons.edit'
    ]);

    Route::get('/coupons/delete/{id}', [

        'uses' => 'CouponController@destroy',
        'as'   => 'coupons.delete'
    ]);

    Route::post('/coupons/update/{id}', [

        'uses' => 'CouponController@update',
        'as'   => 'coupons.update'
    ]);
    /*
    |--------------------------------------------------------------------------\
    |                                       Sales Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get(trans('/sales/create'), [

        'uses' => 'SalesController@create',
        'as'   => 'sales.create'
    ]);

    Route::post('/sales/store', [

        'uses' => 'SalesController@store',
        'as'   => 'sales.store'
    ]);

    Route::post('/sales/storePercentage', [

        'uses' => 'SalesController@storePercentage',
        'as'   => 'sales.storePercentage'
    ]);

    Route::get(trans('/all-sales'), [

        'uses' => 'SalesController@index',
        'as'   => 'sales.index'
    ]);

    Route::get('/sales/edit/{id}', [

        'uses' => 'SalesController@edit',
        'as'   => 'sales.edit'
    ]);

    Route::post('/sales/update/{id}', [

        'uses' => 'SalesController@update',
        'as'   => 'sales.update'
    ]);

    Route::get('/sales/delete/{id}', [

        'uses' => 'SalesController@destroy',
        'as'   => 'sales.delete'
    ]);

    Route::get('/sales/delete-all', [
        'uses' => 'SalesController@deleteAll',
        'as'   => 'sales.deleteAll'
    ]);



    /*
    |--------------------------------------------------------------------------\
    |                                       Tracking Routes                       \
    |--------------------------------------------------------------------------\
    */

    Route::get('/products/purchased/tracking', [
        'uses' => 'TrackingController@index',
        'as'   => 'tracking.index'
    ]);

    Route::get('/products/purchased/tracking/create', [
        'uses' => 'TrackingController@create',
        'as'   => 'tracking.create'
    ]);

    Route::post('/products/purchased/tracking/store', [
        'uses' => 'TrackingController@store',
        'as'   => 'tracking.store'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Users Routes                       \
    |--------------------------------------------------------------------------\
    */

    Route::get(trans('routes.users.index'), [

        'uses' => 'UsersController@index',
        'as'   => 'users.index'
    ]);

    Route::get('/products/purchased-items', [
        'uses' => 'PagesController@purchased',
        'as'   => 'purchased.index'
    ]);

    Route::get('/products/comfirm-ecobank-transaction', [
        'uses' => 'PagesController@ecobankConfirm',
        'as'   => 'purchased.ecobankConfirm'
    ]);

    Route::get('/products/{id}/comfirmed-transaction', [
        'uses' => 'PagesController@ecobankConfirmed',
        'as'   => 'purchased.ecobankConfirmed'
    ]);

    Route::get('/products/liked-items', [

        'uses' => 'PagesController@liked',
        'as'   => 'liked.index'
    ]);
    Route::get('/products/favorited-items', [

        'uses' => 'PagesController@favorited',
        'as'   => 'favorited.index'
    ]);

    Route::get(trans('routes.users.create'), [

        'uses' => 'UsersController@create',
        'as'   => 'users.create'
    ]);

    Route::get('/user/edit/{id}', [

        'uses' => 'UsersController@edit',
        'as'   => 'users.edit'
    ]);

    Route::get('/user/delete/{id}', [

        'uses' => 'UsersController@destroy',
        'as'   => 'users.delete'
    ]);

    Route::post('/user/update/{id}', [

        'uses' => 'UsersController@update',
        'as'   => 'users.update'
    ]);

    Route::post('/user/store', [

        'uses' => 'UsersController@store',
        'as'   => 'users.store'
    ]);

    Route::get('/user/yanfoshop_admin/{id}', [

        'uses' => 'UsersController@admin',
        'as'   => 'users.admin'
    ]);

    Route::get('/user/not-admin/{id}', [

        'uses' => 'UsersController@not_admin',
        'as'   => 'users.not.admin'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Team Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get('/team',[
        'uses' => 'TeamController@index',
        'as'   => 'team.index'
    ]);

    Route::get('/team/create',[
        'uses' => 'TeamController@create',
        'as'   => 'team.create'
    ]);

    Route::post('/team/store',[
        'uses' => 'TeamController@store',
        'as'   => 'team.store'
    ]);

    Route::post('/team/update',[
        'uses' => 'TeamController@update',
        'as'   => 'team.update'
    ]);

    Route::get('/team/delete/{id}', [

        'uses' => 'TeamController@delete',
        'as'   => 'team.delete'
    ]);

    Route::post(trans('routes.settings.update'), [

        'uses' => 'SettingsController@update',
        'as'   => 'settings.update'
    ]);

    Route::get('/profile', [

        'uses' => 'TeamController@profile',
        'as'   => 'profile'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       SlideShow Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get(trans('routes.slideshow.index'),[

        'uses' => 'SlideShowController@index',
        'as'   => 'slideshow.index'

    ]);
    Route::post(trans('routes.slideshow.update'),[

        'uses' => 'SlideShowController@upload',
        'as'   => 'slideshow.update'

    ]);
    Route::post('sortSlideShow',[

        'uses' => 'SlideShowController@updateOrder',
        'as'   => 'slideshow.updateOrder'

    ]);

    Route::delete('slideshow/delete/{id}',[

        'uses' => 'SlideShowController@destroy',
        'as'   => 'slideshow.delete'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Interests Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::get('/interests',[

        'uses' => 'InterestController@index',
        'as'   => 'interest.index'
    ]);
    Route::get(trans('/interests/create'),[

        'uses' => 'InterestController@create',
        'as'   => 'interest.create'
    ]);
    Route::post(trans('/interests/store'),[

        'uses' => 'InterestController@store',
        'as'   => 'interest.store'
    ]);

    Route::get('interests/{id}/edit',[

        'uses' => 'InterestController@edit',
        'as'   => 'interest.edit'
    ]);

    Route::post('interests/{id}/update',[

        'uses' => 'InterestController@update',
        'as'   => 'interest.update'
    ]);

    Route::get('interests/{id}/delete',[

        'uses' => 'InterestController@destroy',
        'as'   => 'interest.destroy'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Faq Routes                       \
    |--------------------------------------------------------------------------\
    */

    Route::get('/faq',[

        'uses' => 'FaqController@index',
        'as'   => 'faq.index'
    ]);
    Route::get(trans('/faq/create'),[

        'uses' => 'FaqController@create',
        'as'   => 'faq.create'
    ]);
    Route::post(trans('/faq/store'),[

        'uses' => 'FaqController@store',
        'as'   => 'faq.store'
    ]);

    Route::get('faq/{id}/edit',[

        'uses' => 'FaqController@edit',
        'as'   => 'faq.edit'
    ]);

    Route::post('faq/{id}/update',[

        'uses' => 'FaqController@update',
        'as'   => 'faq.update'
    ]);

    Route::get('faq/{id}/delete',[

        'uses' => 'FaqController@destroy',
        'as'   => 'faq.destroy'
    ]);

    /*
    |--------------------------------------------------------------------------\
    |                                       Products Routes                       \
    |--------------------------------------------------------------------------\
    */
    Route::resource('products', 'ProductsController',['except'=>['edit','update','destroy']]);

    Route::get('products/{id}/edit',[

        'uses' => 'ProductsController@edit',
        'as'   => 'products.edit'
    ]);

    Route::post('products/{id}/update',[

        'uses' => 'ProductsController@update',
        'as'   => 'products.update'

    ]);

    Route::get('products/delete/{id}',[

        'uses' => 'ProductsController@delete',
        'as'   => 'products.delete'

    ]);


});

