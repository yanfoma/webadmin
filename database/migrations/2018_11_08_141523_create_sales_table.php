<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    public function up()
    {
        if (! Schema::hasTable('sales'))
        {
            Schema::create('sales', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('product_id');
                $table->timestamps();
            });
        }
    }
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
