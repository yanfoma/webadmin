<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToSlideshowTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('slideshow', 'store_id') )
        {
            Schema::table('slideshow', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('slideshow', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
