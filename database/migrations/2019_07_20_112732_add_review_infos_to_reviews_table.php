<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewInfosToReviewsTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('reviews', 'product_id') )
        {
            Schema::table('reviews', function (Blueprint $table)
            {
                $table->integer('product_id');
            });
        }

        if ( ! Schema::hasColumn('reviews', 'user_id') )
        {
            Schema::table('reviews', function (Blueprint $table)
            {
                $table->integer('user_id');
            });
        }

        if ( ! Schema::hasColumn('reviews', 'rating') )
        {
            Schema::table('reviews', function (Blueprint $table)
            {
                $table->integer('rating');
            });
        }

        if ( ! Schema::hasColumn('reviews', 'rating_desc') )
        {
            Schema::table('reviews', function (Blueprint $table)
            {
                $table->text('rating_desc');
            });
        }
        if ( ! Schema::hasColumn('reviews', 'valid') )
        {
            Schema::table('reviews', function (Blueprint $table)
            {
                $table->integer('valid');
            });
        }
    }

    public function down()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->dropColumn('product_id');
            $table->dropColumn('user_id');
            $table->dropColumn('rating');
            $table->dropColumn('rating_desc');
            $table->dropColumn('valid');
        });
    }
}
