<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalePercentageToTheProductTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('products', 'salePercentage') )
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->integer('salePercentage')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('salePercentage');
        });
    }
}
