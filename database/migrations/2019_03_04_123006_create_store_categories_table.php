<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreCategoriesTable extends Migration
{
    public function up()
    {
        if (! Schema::hasTable('store_categories'))
        {
            Schema::create('store_categories', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('store_categories');
    }
}
