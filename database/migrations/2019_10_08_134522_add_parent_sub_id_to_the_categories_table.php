<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentSubIdToTheCategoriesTable extends Migration
{
    public function up()
    {

        if ( ! Schema::hasColumn('categories', 'parentSub_id') )
        {
            Schema::table('categories', function (Blueprint $table)
            {
                $table->integer('parentSub_id');
            });
        }
    }


    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('parentSub_id');
        });
    }
}
