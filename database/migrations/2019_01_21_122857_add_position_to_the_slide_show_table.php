<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionToTheSlideShowTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('slideshow', 'salePercentage') )
        {
            Schema::table('slideshow', function (Blueprint $table)
            {
                $table->integer('order')->unsigned()->default(0);
            });
        }
    }

    public function down()
    {
        Schema::table('slideshow', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
