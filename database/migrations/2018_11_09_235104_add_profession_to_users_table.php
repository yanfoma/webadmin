<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfessionToUsersTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('users', 'profession') )
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->string('profession')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('profession');
        });//
    }
}
