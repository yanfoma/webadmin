<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToCategoriesTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('categories', 'store_id') )
        {
            Schema::table('categories', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
