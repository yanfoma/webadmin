<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoteToThePurchasedTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('purchased', 'note') )
        {
            Schema::table('purchased', function (Blueprint $table)
            {
                $table->text('note')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('purchased', function (Blueprint $table) {
            $table->dropColumn('note');
        });
    }
}
