<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchasedPayedAndPayeddateToThePurchasedTable extends Migration
{

    public function up()
    {
        if (!Schema::hasColumn('purchased', 'payed') )
        {
            Schema::table('purchased', function (Blueprint $table)
            {
                $table->integer('payed')->default(0);
            });
        }
        if (!Schema::hasColumn('purchased', 'payedDate') )
        {
            Schema::table('purchased', function (Blueprint $table)
            {
                $table->string('payedDate')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('purchased', function (Blueprint $table) {
            $table->dropColumn('payed');
        });

        Schema::table('purchased', function (Blueprint $table) {
            $table->dropColumn('payedDate');
        });
    }
}
