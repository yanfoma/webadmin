<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToLikesTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('likes', 'store_id') )
        {
            Schema::table('likes', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('likes', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
