<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('checkouts')) {
            Schema::create('checkouts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('purchased_id');
                $table->string('purchased_name');
                $table->string('name');
                $table->string('image');
                $table->string('qty');
                $table->string('prix');
                $table->string('total');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkouts');
    }
}
