<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreInfoToProductsTable extends Migration
{

    public function up()
    {
        if ( ! Schema::hasColumn('products', 'more_info') )
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->text('more_info')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('more_info');
        });
    }
}
