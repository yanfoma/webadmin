<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToCouponsTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('coupons', 'store_id') )
        {
            Schema::table('coupons', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
