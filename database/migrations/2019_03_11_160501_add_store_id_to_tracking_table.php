<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToTrackingTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('tracking', 'store_id') )
        {
            Schema::table('tracking', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('tracking', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
