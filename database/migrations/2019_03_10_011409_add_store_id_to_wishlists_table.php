<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToWishlistsTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('wishlists', 'store_id') )
        {
            Schema::table('wishlists', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('wishlists', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
