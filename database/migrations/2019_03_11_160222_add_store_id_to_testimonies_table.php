<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToTestimoniesTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('testimonies', 'store_id') )
        {
            Schema::table('testimonies', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('testimonies', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
