<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToSalesTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('sales', 'store_id') )
        {
            Schema::table('sales', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
