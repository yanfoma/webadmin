<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToUsersTable extends Migration
{

    public function up()
    {
        if ( ! Schema::hasColumn('users', 'store_id') )
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
