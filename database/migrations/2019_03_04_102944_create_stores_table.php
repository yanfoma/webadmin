<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{

    public function up()
    {
        if (! Schema::hasTable('stores'))
        {
            Schema::create('stores', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('store_id');
                $table->integer('category_id');
                $table->string('name');
                $table->string('slug')->unique();
                $table->text('logo');
                $table->text('image');
                $table->text('about')->nullable();
                $table->text('email');
                $table->text('phone');
                $table->text('addresse')->nullable();
                $table->text('facebook')->nullable();
                $table->text('messenger')->nullable();
                $table->text('whatsapp')->nullable();
                $table->text('linkedIn')->nullable();
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
