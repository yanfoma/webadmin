<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerifiedColumnToUsersTable extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('users','verified'))
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->string('verified');
            });
        }
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('verified');
        });
    }
}
