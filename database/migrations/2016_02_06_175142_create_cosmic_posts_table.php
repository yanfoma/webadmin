<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCosmicPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('cosmic_posts'))
        {
            Schema::create('cosmic_posts', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('title');
                $table->text('body');
                $table->string('slug')->unique();
                $table->string('image')->nullable();
                $table->integer('category_id')->nullable()->unsigned();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cosmic_posts');
    }
}
