<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingTable extends Migration
{
    public function up()
    {
        if (! Schema::hasTable('tracking'))
        {
            Schema::create('tracking', function (Blueprint $table)
            {
                $table->increments('id');
                $table->integer('purchasedId');
                $table->string('tackingCode')->nullable();
                $table->string('step')->nullable();
                $table->string('date')->nullable();
                $table->text('note')->nullable();
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('tracking');
    }
}
