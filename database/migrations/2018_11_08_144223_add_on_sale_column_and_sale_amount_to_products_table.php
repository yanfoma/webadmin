<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnSaleColumnAndSaleAmountToProductsTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('products', 'onSale') )
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->boolean('onSale')->default(false);
            });
        }
        if ( ! Schema::hasColumn('products', 'sale_amount') )
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->integer('sale_amount')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('onSale');
            $table->dropColumn('sale_amount');
        });//
    }
}
