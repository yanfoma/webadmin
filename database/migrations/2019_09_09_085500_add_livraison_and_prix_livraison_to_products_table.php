<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLivraisonAndPrixLivraisonToProductsTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('products', 'prixLivraison') )
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->integer('prixLivraison');
            });
        }

        if ( ! Schema::hasColumn('products', 'livraison') )
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->string('livraison');
            });
        }
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('prixLivraison');
            $table->dropColumn('livraison');
        });
    }
}
