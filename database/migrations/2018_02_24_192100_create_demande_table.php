<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('demande'))
        {
            Schema::create('demande', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('name');
                $table->integer('code');
                $table->string('email');
                $table->string('phone');
                $table->text('resume');
                $table->text('message');
                $table->string('view');
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demande');
    }
}
