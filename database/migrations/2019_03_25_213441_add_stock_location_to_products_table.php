<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStockLocationToProductsTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('products', 'stock_location') )
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->string('stock_location')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('stock_location');
        });
    }
}
