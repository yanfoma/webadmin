<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchasedIdToThePurchasedTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('purchased', 'purchasedId') )
        {
            Schema::table('purchased', function (Blueprint $table)
            {
                $table->integer('purchasedId')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('purchased', function (Blueprint $table) {
            $table->dropColumn('purchasedId');
        });
    }
}
