<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokenToUserTables extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('users', 'token') )
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->text('token')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('token');
        });
    }
}
