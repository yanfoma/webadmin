<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToPurchasedTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('purchased', 'store_id') )
        {
            Schema::table('purchased', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('purchased', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
