<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductImagesTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasTable('productImages') )
        {
            Schema::create('productImages', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id');
                $table->text('image_url');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('productImages');
    }
}
