<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentToPurchasedTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('purchased', 'payment') )
        {
            Schema::table('purchased', function (Blueprint $table)
            {
                $table->string('payment');
            });
        }
    }

    public function down()
    {
        Schema::table('purchased', function (Blueprint $table) {
            $table->dropColumn('payment');
        });
    }
}
