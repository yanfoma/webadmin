<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdToProductsTale extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('products','category_id'))
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->string('category_id');
            });
        }
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
}
