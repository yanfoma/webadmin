<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasTable('subcategories') )
        {

            Schema::create('subcategories', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('store_id');
                $table->integer('parent_id');
                $table->string('name');
                $table->text('image')->nullable();
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('subcategories');
    }
}
