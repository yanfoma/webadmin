<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSaleTypeToTheProductTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('products', 'saleType') )
        {
            Schema::table('products', function (Blueprint $table)
            {
                $table->integer('saleType')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('saleType');
        });
    }
}
