<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageUrlToCategoriesTale extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('categories','image_url'))
        {
            Schema::table('categories', function (Blueprint $table)
            {
                $table->string('image_url');
            });
        }
    }

    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('image_url');
        });
    }
}
