<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentToCheckoutsTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('checkouts', 'payment') )
        {
            Schema::table('checkouts', function (Blueprint $table)
            {
                $table->string('payment');
            });
        }
    }

    public function down()
    {
        Schema::table('checkouts', function (Blueprint $table) {
            $table->dropColumn('payment');
        });
    }
}
