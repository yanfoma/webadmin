<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimoniesTable extends Migration
{

    public function up()
    {
        if (! Schema::hasTable('testimonies'))
        {
            Schema::create('testimonies', function (Blueprint $table)
            {
                $table->increments('id');
                $table->string('name');
                $table->string('phone')->nullable();
                $table->string('email')->nullable();
                $table->string('country')->nullable();
                $table->text('message');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('testimonies');
    }
}
