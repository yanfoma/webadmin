<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('purchased')) {
            Schema::create('purchased', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('pays');
                $table->string('ville');
                $table->string('email');
                $table->string('phone');
                $table->text('addresse');
                $table->text('nbrProduits');
                $table->text('total');
                $table->string('view');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchased');
    }
}
