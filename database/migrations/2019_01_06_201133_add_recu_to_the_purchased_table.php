<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecuToThePurchasedTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('purchased', 'recu') )
        {
            Schema::table('purchased', function (Blueprint $table)
            {
                $table->string('recu')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('purchased', function (Blueprint $table) {
            $table->dropColumn('recu');
        });
    }
}
