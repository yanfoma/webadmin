<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEducationToUsersTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('users', 'education') )
        {
            Schema::table('users', function (Blueprint $table)
            {
                $table->string('education')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('education');
        });//
    }
}
