<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreIdToCheckoutsTable extends Migration
{
    public function up()
    {
        if ( ! Schema::hasColumn('checkouts', 'store_id') )
        {
            Schema::table('checkouts', function (Blueprint $table)
            {
                $table->integer('store_id')->unsigned();
            });
        }
    }


    public function down()
    {
        Schema::table('checkouts', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
