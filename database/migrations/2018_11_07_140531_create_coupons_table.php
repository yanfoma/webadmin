<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    public function up()
    {
        if (! Schema::hasTable('coupons'))
        {
            Schema::create('coupons', function (Blueprint $table) {
                $table->increments('id');
                // The coupon code
                $table->string('code')->unique();
                // The human readable coupon code name
                $table->string('name');
                // The number of uses currently
                $table->string('number_of_use')->nullable();
                // The max uses this coupon has
                $table->integer('max_uses')->unsigned()->nullable();
                // How many times a user can use this voucher.
                $table->integer('max_uses_user')->unsigned()->nullable( );
                // The type can be: coupon, discount, sale.
                $table->tinyInteger('type')->unsigned();
                // The amount to discount by.
                $table->integer('discount_amount')->nullable();
                // When the coupon begins
                $table->timestamp('starts_at');
                // When the coupon ends
                $table->timestamp('expires_at');
                // Whether or not the coupon is a percentage or a fixed price.
                $table->boolean('is_fixed')->default(true);
                $table->string('percent_off')->nullable();
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
