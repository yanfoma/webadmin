<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdToCategoriesTable extends Migration
{

    public function up()
    {
        if ( ! Schema::hasColumn('categories', 'parent_id') )
        {
            Schema::table('categories', function (Blueprint $table)
            {
                $table->integer('parent_id');
            });
        }
    }


    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('parent_id');
        });
    }
}
